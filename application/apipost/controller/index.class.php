<?php
/**
 * YzmCMS通用内容远程发布接口
 * @author           袁志蒙  
 * @license          http://www.yzmcms.com
 * @lastmodify       2023-02-01
 */
 
class index{

	private $auth = 'caiji'; //远程发布需密码验证，可自由修改！
	private $userid = '1'; //发布管理员ID，默认为1，POST参数中可动态修改
	private $siteid = '0'; //站点ID

	//接口返回形式 1:json格式, 2:et返回格式, 3文字格式
	private $type = 3;

	//数据库句柄，请勿修改！
	private $db = null;


	/**
	 * 构造方法，验证密码
	 */	
	public function __construct() {
		$auth = input('get.auth', '', 'trim');
		if($auth != $this->auth) $this->_return(array('status' => 0, 'message'=>'验证密码不正确！'));
		$this->siteid = input('siteid', 0, 'intval');
	}


	/**
	 * INIT
	 */
	public function init(){
		echo '欢迎使用YzmCMS内容发布模块！';
	}

	
	
	/**
	 * 远程发布接口
	 */
	public function add_content(){	
		if(empty($_POST))  $this->_return(array('status' => 0, 'message'=>'POST数据不能为空！'));
		if(!isset($_POST['title']))  $this->_return(array('status' => 0, 'message'=>'标题不能为空！'));
		if(!isset($_POST['catid']))  $this->_return(array('status' => 0, 'message'=>'栏目ID不能为空！'));
		if(!isset($_POST['content']))  $this->_return(array('status' => 0, 'message'=>'内容不能为空！'));

		$catid = intval($_POST['catid']);
		if(!$catid) $this->_return(array('status' => 0, 'message'=>'栏目ID不能为空！'));

		$userid = isset($_POST['userid']) ? intval($_POST['userid']) : $this->userid;
		$is_system = isset($_POST['system']) ? intval($_POST['system']) : 1;

		if($is_system){
			$data = D('admin')->field('adminname AS username,nickname')->where(array('adminid' => $userid))->find();
			if(!$data) $this->_return(array('status' => 0, 'message'=>'后台管理员不存在！'));
		}else{
			$data = D('member')->field('username')->where(array('userid' => $userid))->find();
			if(!$data) $this->_return(array('status' => 0, 'message'=>'会员账号不存在！'));
			$data['nickname'] = $data['username'];
		}
		
		//设置模型
		$this->set_db($catid);

		//检查标题是否重复
		$id = $this->db->field('id')->where(array('title' => trim($_POST['title'])))->one();
        if($id) $this->_return(array('status' => 0, 'message' => '文章标题重复！'));

		//自动提取缩略图
		if(isset($_POST['auto_thum']) && $_POST['thumb'] == '') {
			$img = match_img($_POST['content']);
			$_POST['thumb'] = $img ? thumb($img, get_config('thumb_width'), get_config('thumb_height')) : '';
		}		

        $_POST['siteid'] = $this->siteid;
        $_POST['nickname'] = $data['nickname'] ? $data['nickname'] : '管理员';
        $_POST['userid'] = $userid;
        $_POST['username'] = $data['username'];
        $_POST['catid'] = $catid;
        $_POST['inputtime'] = isset($_POST['inputtime']) ? intval($_POST['inputtime']) : SYS_TIME;
        $_POST['updatetime'] = isset($_POST['updatetime']) ? intval($_POST['updatetime']) : SYS_TIME;
        $_POST['copyfrom'] = isset($_POST['copyfrom']) ? trim($_POST['copyfrom']) : '原创';
        $_POST['seo_title'] = isset($_POST['seo_title']) ? $_POST['seo_title'] : $_POST['title'].'_'.get_config('site_name');
        $_POST['status'] = isset($_POST['status']) ? intval($_POST['status']) : 1;
        $_POST['issystem'] = $is_system;
        $_POST['listorder'] = 10;
        $_POST['description'] = isset($_POST['description'])&&$_POST['description'] ? trim($_POST['description']) : str_cut(strip_tags($_POST['content']),200);
        $_POST = $this->_content_dispose($_POST);
        $id = $this->db->insert($_POST);
		
		//TAG标签处理
		if(isset($_POST['tag'])){
			$this->_tag_dispose($this->modelid, $catid, explode(',', $_POST['tag']), $id);
		}
		
		//保存附件
		if(isset($_POST['attachment'])){
			$array = explode(',', $_POST['attachment']);
			foreach ($array as $value) {
				if(!is_file(YZMPHP_PATH.$value)) continue;
				$size = filesize(YZMPHP_PATH.$value);
				$pathinfo = pathinfo($value);
				$arr['siteid'] = $this->siteid;
				$arr['originname'] = $pathinfo['basename'];
				$arr['filename'] = $pathinfo['basename'];
				$arr['filepath'] = $pathinfo['dirname'].'/';
				$arr['filesize'] = $size;
				$arr['fileext'] = $pathinfo['extension'];
				$arr['module'] = ROUTE_M;
				$arr['isimage'] = in_array($arr['fileext'], array('gif', 'jpg', 'png', 'jpeg')) ? 1 : 0;
				$arr['userid'] = 1;
				$arr['username'] = 'yzmcms';
				$arr['uploadtime'] = SYS_TIME;
				$arr['uploadip'] = getip();
				D('attachment')->insert($arr);
			}			
		}		
		
        if($id){
        	$url = get_content_url($catid, $id);
        	$this->db->update(array('url' => $url), array('id' => $id));

        	// 插入所有内容表
        	$_POST['siteid'] = $this->siteid;
        	$_POST['modelid'] = $this->modelid;
        	$_POST['id'] = $id;
        	$_POST['url'] = $url;
        	D('all_content')->insert($_POST);

        	$this->_return(array('status' => 1, 'message' => '发布成功'));
        }
        $this->_return(array('status' => 0, 'message' => '发布失败'));
	}


	/**
	 * 获取栏目信息接口 
	 */
	public function get_category(){
		if($this->type == 1){
			$data = D('category')->field('catid,catname')->where(array('siteid'=>$this->siteid))->order('listorder ASC,catid ASC')->select();
	        $this->_return(array('status' => 1, 'data'=>$data, 'message' => '操作成功'));
		}else{
			$data = D('category')->field('catid,catname,arrchildid,parentid')->where('siteid='.$this->siteid.' and type=0')->order('listorder ASC,catid ASC')->select(); 

			foreach($data as $val){
				$val['disabled'] = '';
				if($val['catid'] != $val['arrchildid']) $val['disabled'] = ' disabled="disabled"';
				$categorys[$val['catid']] = $val;
			}

			$tree = yzm_base::load_sys_class('tree');
		    $str = "<option\$disabled value=\\\"\$catid\\\">\$catname</option>";
		    $tree->init($categorys);
		    $categorys = $tree->get_tree(0, $str);
		    echo "<select>$categorys</select>";
		}
		
	}


	/**
	 * 返回请求接口结果
	 * @param array
	 * @param string|array
	 */
	private function _return($array){
		if($this->type == 1){
			return_json($array);
		}elseif($this->type == 2){
			if($array['status']){
				echo '[ok]';
			}else{
				echo '[err]'.$array['message'].'[/err]';
			}
			exit();
		}else{
			echo $array['message'];
			exit();
		}
	}	



	/**
	 * TAG标签处理
	 * @param $modelid
	 * @param $catid 
	 * @param $tags 
	 * @param $aid 
	 */	
	private function _tag_dispose($modelid, $catid, $tags, $aid) {
		$siteid = $this->siteid;
		$tag = D('tag');
		$tag_content = D('tag_content');      
        $tag_content->delete(array('modelid' => $modelid, 'aid' => $aid));
        $tags = array_unique($tags); 
		foreach($tags as $val){
			$val = trim($val);
			if(!$val) continue;
			$tagid = $tag->field('id')->where(array('siteid'=>$siteid, 'tag'=>$val))->one();
			if($tagid){
				$total = $tag_content->where(array('siteid'=>$siteid, 'tagid'=>$tagid))->total();
				$tag->update(array('total'=>$total+1), array('id'=>$tagid));
			}else{
				$tagid = $tag->insert(array('siteid'=>$siteid, 'catid'=>$catid, 'tag'=>$val, 'total'=>1, 'inputtime'=>SYS_TIME));
			}
			
			$tag_content->insert(array('siteid'=>$siteid, 'modelid'=>$modelid, 'catid'=>$catid, 'tagid'=>$tagid, 'aid'=>$aid), false, false);
		}
	}


	/**
	 * 内容处理
	 * @param $content 
	 */	
	private function _content_dispose($content) {
		$data = array();
		foreach($content as $key=>$val){
			if(strpos($val, '{many}')){
				$arr = explode('{many}', $val);
				foreach ($arr as $k => $v) {
					$r[] = array(
						'url' => $v,
						'alt' => '',
					);
				}
				$data[$key] = array2string($r);
			}else{
				$data[$key] = $val;
			}
		}	
		return $data;
	}


	/**
	 * 设置模型
	 */	
	private function set_db($catid){
		$modelid = get_category($catid, 'modelid');
		$model = get_model($modelid);
		if(!$model)  $this->_return(array('status' => 0, 'message'=>'模型错误！'));
		$this->modelid = $modelid;
		$this->db = D($model);
	}
}