<?php
/**
 * 站点浏览量统计
 *
 * @author           袁志蒙  
 * @license          http://www.yzmcms.com
 * @lastmodify       2019-06-21 
 */

class view{
	
	public function init(){
	}

	/**
	 * 获取并记录访问量
	 * 调用方式： <script src="{U('vcount/view/count')}" type='text/javascript' language="javascript"></script>
	 * @return string 
	 */
	public function count(){

		$res = D('view_count')->field('count,today,updatetime')->where(array('id'=>1))->find();
		$data = array(
			'count' => $res['count']+1,
			'today' => $this->_view_log($res),
			'updatetime' => SYS_TIME
		);
		D('view_count')->update($data, array('id'=>1));
		$str =  '总访问量：'.$data['count'].'，今日访问：'.$data['today'];
		echo 'document.write("'.$str.'");';
		exit;
	}


	/**
	 * 记录浏览信息
	 * @param   $res 
	 * @return  int 今日访问量
	 */
	private function _view_log($res){
		$data = array(
			'inputtime' => SYS_TIME,
			'ip' => getip(),
			'referer' => HTTP_REFERER
		);
		D('view_log')->insert($data);
		return date('Ymd', $res['updatetime']) == date('Ymd', SYS_TIME) ? $res['today']+1 : 1;
	}
}