<?php
/**
 * 后台站点浏览量折线统计图
 * www.yzmcms.com
 */

defined('IN_YZMPHP') or exit('Access Denied'); 
yzm_base::load_controller('common', 'admin', 0);

class vcount extends common {
	
	
	/**
	 * init
	 */
	public function init(){	

		//删除一个月前的浏览记录
		D('view_log')->delete(array('inputtime<' => strtotime('-1 month')));

		//统计开始时间
		$starttime = strtotime(date('Y-m-d'))-10*24*3600;

		//统计结束时间
		$endtime = strtotime(date('Y-m-d'));

		$where = "inputtime > $starttime";  //无需加结束条件，否则统计不到今日数据
		$data = D('view_log')->field("COUNT(*) AS num,FROM_UNIXTIME(inputtime, '%Y-%m-%d') AS gap")->where($where)->group('gap')->select();
		$arr = array();
		foreach ($data as $val){
			$arr[$val['gap']] = intval($val['num']);
		}

		for($i=$starttime; $i<=$endtime; $i = $i+24*3600){
			$num = isset($arr[date('Y-m-d',$i)]) ? $arr[date('Y-m-d',$i)] : 0;				
			$result['day'][] = date('Y-m-d',$i);
			$result['num'][] = $num;
		}
		$result = json_encode($result);
		include $this->admin_tpl('view_count');
	}

}