DROP TABLE IF EXISTS `yzmcms_view_count`;
CREATE TABLE `yzmcms_view_count` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '总访问',
  `today` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '今日访问',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='站点浏览统计';

INSERT INTO `yzmcms_view_count` VALUES ('1', '0', '0', '1560988800');

DROP TABLE IF EXISTS `yzmcms_view_log`;
CREATE TABLE `yzmcms_view_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `inputtime` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` char(15) NOT NULL DEFAULT '',
  `referer` varchar(80) NOT NULL DEFAULT '' COMMENT '来源',
  PRIMARY KEY (`id`),
  KEY `ip` (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='站点浏览记录';
