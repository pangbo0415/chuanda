<?php
/**
 * YzmCMS扫描、删除重复内容
 * 要求YzmCMS V5.8及以上版本
 */
defined('IN_YZMPHP') or exit('Access Denied'); 
yzm_base::load_controller('common', 'admin', 0);
yzm_base::load_sys_class('page','',0);

class check_repeat extends common {

	
	public function init(){
		include $this->admin_tpl('content_list');
	}


	/**
	 * 重复内容检测
	 */
	public function check(){
		$data = D('all_content')->field('allid,title,count(title) AS num')->group('title')->having('COUNT(*)>1')->order('num DESC')->limit(100)->select();
		if(!$data) return_json(array('status'=>0, 'message'=>'目前没有重复的内容！'));
		foreach($data as $key => $val){
			$val['url'] = U('show', array('allid'=>$val['allid']));
			$data[$key] = $val;
		}
		return_json(array('status'=>1, 'message'=>'操作成功', 'data'=>$data, 'total'=>count($data)));
	}


	/**
	 * 一键删除
	 */
	public function delete(){
		$titles = isset($_POST['titles']) ? $_POST['titles'] : array();
		$i = 0;
		$all_content = D('all_content');
		yzm_base::load_model('content_model', 'admin', 0);
		foreach ($titles as $title) {
			$data = $all_content->field('allid,modelid,id')->where(array('title'=>$title))->order('allid ASC')->select();
			if(!$data) continue;
			// 保留一个
			unset($data[0]);
			foreach ($data as $val) {
				$_POST['modelid'] = $val['modelid'];
				$content_model = new content_model();
				$res = $content_model->content_delete($val['id']);
				if($res) $i ++;
			}
		}
		return_json(array('status'=>1, 'message'=>'共删除'.$i.'篇重复内容！'));
	}


	/**
	 * 内容重复详情
	 */
	public function show(){
		$allid = isset($_GET['allid']) ? intval($_GET['allid']) : showmsg(L('lose_parameters'), 'stop');
		$title = D('all_content')->field('title')->where(array('allid'=>$allid))->one();

		$all_content = D('all_content');
		$total = $all_content->where(array('title'=>$title))->total();
		$page = new page($total, 5);
		$data = $all_content->where(array('title'=>$title))->order('allid ASC')->limit($page->limit())->select();
		include $this->admin_tpl('content_show');
	}


	/**
	 * 内容重复详情删除
	 */
	public function del(){
		$ids = isset($_POST['ids']) ? $_POST['ids'] : array();
		$i = 0;
		$all_content = D('all_content');
		yzm_base::load_model('content_model', 'admin', 0);
		foreach($ids as $val){
			$res = $all_content->field('modelid,id')->where(array('allid' => $val))->find(); 
			if(!$res) continue;
			$_POST['modelid'] = $res['modelid'];
			$content_model = new content_model();
			$res = $content_model->content_delete($res['id']);
			if($res) $i ++;
		}
		return_json(array('status'=>1, 'message'=>'共删除'.$i.'篇重复内容！'));
	}

}