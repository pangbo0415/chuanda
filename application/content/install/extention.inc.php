<?php
defined('IN_YZMPHP') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$parentid = $menu->insert(array('name'=>'重复内容检测', 'parentid'=>3, 'm'=>'content', 'c'=>'check_repeat', 'a'=>'init', 'data'=>'', 'listorder'=>5, 'display'=>1));
$menu->insert(array('name'=>'重复内容检测', 'parentid'=>$parentid, 'm'=>'content', 'c'=>'check_repeat', 'a'=>'public_check', 'data'=>'', 'listorder'=>0, 'display'=>0));
$menu->insert(array('name'=>'一键删除', 'parentid'=>$parentid, 'm'=>'content', 'c'=>'check_repeat', 'a'=>'delete', 'data'=>'', 'listorder'=>0, 'display'=>0));
$menu->insert(array('name'=>'内容重复详情', 'parentid'=>$parentid, 'm'=>'content', 'c'=>'check_repeat', 'a'=>'show', 'data'=>'', 'listorder'=>0, 'display'=>0));
$menu->insert(array('name'=>'内容重复详情删除', 'parentid'=>$parentid, 'm'=>'content', 'c'=>'check_repeat', 'a'=>'del', 'data'=>'', 'listorder'=>0, 'display'=>0));
