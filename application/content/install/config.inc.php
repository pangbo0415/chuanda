<?php
/**
 * 安装模块须知：
 * 本文件必须存在，作用是记录本模块的信息。
 */
return array(
	'module' => 'content',		    //模块目录
	'name' => '重复内容检测',		//模块名称
	'version' => '1.0',	        //模块版本
	'author' => '袁志蒙',       //模块作者
	'authorsite' => 'http://www.yzmcms.com/',    //作者主页
	'authoremail' => '214243830@qq.com',         //作者邮箱
	'description' => '提供YzmCMS重复内容检测、删除等功能',             //模块简介
	'setting' => '',	                //模块设置	
	'listorder' => '0',	                //模块排序
	'disabled' => '0',	                //是否禁用模块，1为禁用，0为启用
	'updatedate' => '2022-08-30',		//模块更新日期
);