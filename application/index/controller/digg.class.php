<?php
/**
 *  YzmCMS 文章顶踩插件
 *	www.yzmcms.com
 *  作者：袁志蒙
 *  @lastmodify		2018-11-16
 */

class digg{
	
	public function init(){
		$modelid = input('modelid', 1, 'intval');
		$id = input('id', 0, 'intval');
		$digg = input('digg', 1, 'intval');

		$tablename = get_model($modelid);
		if(!$tablename) return_json(array('status' => 0, 'message' => '模型错误'));
		if(!$id) return_json(array('status' => 0, 'message' => '缺少参数ID'));
		
		$fields = D($tablename)->get_fields();
		if(!in_array('up', $fields) || !in_array('down', $fields)) return_json(array('status' => 0, 'message' => '字段不存在'));
		$data = D($tablename)->field('up,down')->where(array('id' => $id))->find();

		if ($digg){
			D($tablename)->update('`up` = `up`+1', array('id' => $id));
			return_json(array('status' => 1, 'message' => $data['up']+1));
		} else {
			D($tablename)->update('`down` = `down`+1', array('id' => $id));
			return_json(array('status' => 1, 'message' => $data['down']+1));
		}
	}
}