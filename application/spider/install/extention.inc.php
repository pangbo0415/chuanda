<?php
defined('IN_YZMPHP') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$parentid = $menu->insert(array('name'=>'蜘蛛来访记录', 'parentid'=>3, 'm'=>'spider', 'c'=>'spider', 'a'=>'init', 'data'=>'', 'listorder'=>1, 'display'=>'1'));
$parentid = $menu->insert(array('name'=>'删除记录', 'parentid'=>$parentid, 'm'=>'spider', 'c'=>'spider', 'a'=>'del', 'data'=>'', 'listorder'=>1, 'display'=>'0'));
$parentid = $menu->insert(array('name'=>'删除全部记录', 'parentid'=>$parentid, 'm'=>'spider', 'c'=>'spider', 'a'=>'all_del', 'data'=>'', 'listorder'=>1, 'display'=>'0'));
$parentid = $menu->insert(array('name'=>'统计报表', 'parentid'=>$parentid, 'm'=>'spider', 'c'=>'spider', 'a'=>'statistics', 'data'=>'', 'listorder'=>1, 'display'=>'0'));