DROP TABLE IF EXISTS `yzmcms_spider_log`;
CREATE TABLE `yzmcms_spider_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `spider` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '蜘蛛',
  `ip` varchar(15) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `referer` varchar(255) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `spider` (`spider`),
  KEY `url` (`url`),
  KEY `addtime` (`addtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='蜘蛛访问日志';