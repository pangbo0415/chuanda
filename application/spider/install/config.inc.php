<?php
/**
 * 安装模块须知：
 * 本文件必须存在，作用是记录本模块的信息。
 */
return array(
	'module' => 'spider',		    //模块目录
	'name' => '蜘蛛访问记录模块',		//模块名称
	'version' => '1.0',	        //模块版本
	'author' => '袁志蒙',       //模块作者
	'authorsite' => 'http://www.yzmcms.com/',    //作者主页
	'authoremail' => '214243830@qq.com',         //作者邮箱
	'description' => '蜘蛛访问记录模块',             //模块简介
	'setting' => '',	                //模块设置	
	'listorder' => '0',	                //模块排序
	'disabled' => '0',	                //是否禁用模块，1为禁用，0为启用
	'updatedate' => '2021-05-21',		//模块更新日期
);