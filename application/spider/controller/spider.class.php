<?php
/**
 * 蜘蛛来访记录管理
 * www.yzmcms.com
 * 2021-05-21
 * 袁志蒙
 */

defined('IN_YZMPHP') or exit('Access Denied'); 
yzm_base::load_controller('common', 'admin', 0);
yzm_base::load_sys_class('page','',0);

class spider extends common {

	public $spider_type = array(    	 				 	
		    1 => '百度',    		  				
		    2 => '谷歌',    		  			 
		    3 => '360',    	 	 			 
		    4 => '神马',        	 	 
		    5 => '必应',      	  	  
		    6 => '雅虎',        		  
		    7 => '搜搜',    	 	 		  
		    8 => '搜狗',    		 	 	 	
		    9 => '有道',      			 	 
		    10 => 'Alexa',     	   	  
		    11 => '头条搜索'    		   			
		); 
	
	
	/**
	 * init
	 */
	public function init(){	

		$where = '1=1';
		if(isset($_GET['dosubmit'])){
			$spider = isset($_GET["spider"]) ? intval($_GET["spider"]) : 0;
			$searinfo = isset($_GET['searinfo']) ? safe_replace(trim($_GET['searinfo'])) : '';
			$type = isset($_GET["type"]) ? intval($_GET["type"]) : 1;
			
			if($spider) {
				$where .= ' AND spider = '.$spider;
			}

			if(isset($_GET['start']) && isset($_GET['end']) && $_GET['start']) {
				$where .= " AND `addtime` >= '".strtotime($_GET['start'])."' AND `addtime` <= '".strtotime($_GET['end'])."' ";
			}

			if($searinfo != ''){
				if($type == '1')
					$where .= ' AND url LIKE \'%'.$searinfo.'%\'';
				elseif($type == '2')
					$where .= ' AND referer LIKE \'%'.$searinfo.'%\'';
				else
					$where .= ' AND ip = \''.$searinfo.'\'';
			}
		}
		$of = input('get.of');
		$or = input('get.or');
		$of = in_array($of, array('id','spider','ip','addtime')) ? $of : 'id';
		$or = in_array($or, array('ASC','DESC')) ? $or : 'DESC';
		$spider_log = D('spider_log');
		$total = $spider_log->where($where)->total();
		$page = new page($total, 15);
		$data = $spider_log->where($where)->order("$of $or")->limit($page->limit())->select();	
		$_GET = array_map('htmlspecialchars', $_GET);
		$spider_type = $this->spider_type;
		include $this->admin_tpl('spider_log');
	}


	/**
	 * 删除记录
	 */	
	public function del(){ 
		if($_POST && is_array($_POST['id'])){
			$spider_log = D('spider_log');
			foreach($_POST['id'] as $val){
				$spider_log->delete(array('id'=>$val));
			}
			showmsg(L('operation_success'),'',1);
		}
	}


	/**
	 * 删除全部记录
	 */	
	public function all_del(){ 
		D('spider_log')->delete(array(1=>1));
		showmsg(L('operation_success'),'',1);
	}


	/**
	 * 统计报表
	 */	
	public function statistics(){
		$starttime = strtotime(date('Y-m-d'))-9*24*3600;
		$endtime = strtotime(date('Y-m-d'));
		$data = $result = array();
		$data['baidu'] = D('spider_log')->field("COUNT(*) AS num,FROM_UNIXTIME(addtime, '%Y-%m-%d') AS date")->where(array('spider'=>1, 'addtime>'=>$starttime))->group('date')->select();
		$data['google'] = D('spider_log')->field("COUNT(*) AS num,FROM_UNIXTIME(addtime, '%Y-%m-%d') AS date")->where(array('spider'=>2, 'addtime>'=>$starttime))->group('date')->select();
		$data['360'] = D('spider_log')->field("COUNT(*) AS num,FROM_UNIXTIME(addtime, '%Y-%m-%d') AS date")->where(array('spider'=>3, 'addtime>'=>$starttime))->group('date')->select();
		$data['shenma'] = D('spider_log')->field("COUNT(*) AS num,FROM_UNIXTIME(addtime, '%Y-%m-%d') AS date")->where(array('spider'=>4, 'addtime>'=>$starttime))->group('date')->select();
		$data['sougou'] = D('spider_log')->field("COUNT(*) AS num,FROM_UNIXTIME(addtime, '%Y-%m-%d') AS date")->where(array('spider'=>8, 'addtime>'=>$starttime))->group('date')->select();
		foreach ($data as $key => $value) {
			$arr = $date = array();
			foreach ($value as $val){
				$arr[$val['date']] = intval($val['num']);
			}

			for($i=$starttime; $i<=$endtime; $i=$i+24*3600){
				$num = isset($arr[date('Y-m-d',$i)]) ? $arr[date('Y-m-d',$i)] : 0;				
				$result[$key][] = $num;
				$date[] = date('Y-m-d',$i);
			}
		}

		include $this->admin_tpl('spider_statistics');
	}

}