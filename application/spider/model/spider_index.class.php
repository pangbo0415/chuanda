<?php

/**
 * 记录蜘蛛来访日志
 * www.yzmcms.com
 * 袁志蒙
 */
class spider_index{
	
	public function record(){
		$useragent = isset($_SERVER['HTTP_USER_AGENT']) ? addslashes(strtolower($_SERVER['HTTP_USER_AGENT'])): false;
		$r = $this->is_spider($useragent);
		if($r){
			D('spider_log')->insert(array('spider' => $r,'ip' => getip(),'url' => get_url(),'referer' => HTTP_REFERER,'addtime' => SYS_TIME), true);
		}
	}


	private function is_spider($useragent){
		if(!$useragent) return false;
		
		if (strpos($useragent, 'baiduspider') !== false) {    	  					
		    return 1;     	 				 
		} 

		if (strpos($useragent, 'googlebot') !== false) {    	    			
		    return 2;     		 		  
		}

		if (strpos($useragent, '360spider') !== false) {      	   	 
		    return 3;      		    
		} 

		if (strpos($useragent, 'yisouspider') !== false) {      			   
		    return 4;       	 	  
		}

		if (strpos($useragent, 'bingbot') !== false) {      	    	
		    return 5;    	  	 	  
		}

		if (strpos($useragent, 'yahoo') !== false) {    		 	  	 
		    return 6;    				   	
		}

		if (strpos($useragent, 'sosospider') !== false) {    	  	 		 
		    return 7;    		 		   
		} 

		if (strpos($useragent, 'sogou') !== false) {     					 	
		    return 8;     		   		
		} 

		if (strpos($useragent, 'yodaobot') !== false) {    	       
		    return 9;    		 	  		
		}

		if (strpos($useragent, 'ia_archiver') !== false) {    		   	  
		    return 10;    	 			   
		}

		if(strpos($useragent,'bytespider') !== false){      		 	  
		    return 11;     		 	   
		}

		return false;
	}
}