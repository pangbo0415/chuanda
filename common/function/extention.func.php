<?php
/**
 * extention.func.php   用户自定义函数库
 *
 * @author           袁志蒙  
 * @license          http://www.yzmcms.com
 * @lastmodify       2018-03-18
 */

 function get_content_img($value, $num = 0) {
    return qh_get_content_url($value, 'src', 'gif|jpg|jpeg|png|webp', $num);
}
/**
 * 获取内容中的指定标签URL地址
 * @param $value 内容值
 * @param $attr 标签值，例如src
 * @param $ext 指定扩展名，例如jpg|gif
 * @param $num 指定获取数量
 * @return 在变量中提取img标签的图片路径到数组
 */
function qh_get_content_url($value, $attr, $ext, $num = 0) {

    $rt = [];
    if (!$value) {
        return $rt;
    }

    $ext = str_replace(',', '|', $ext);
    $value = preg_replace('/\.('.$ext.')@(.*)(\'|")/iU', '.$1$3', $value);
    if (preg_match_all("/(".$attr.")=([\"|']?)([^ \"'>]+\.(".$ext."))\\2/i", $value, $imgs)) {
        $imgs[3] = array_unique($imgs[3]);
        foreach ($imgs[3] as $i => $img) {
            if ($num && $i+1 > $num) {
                break;
            }
            $rt[] = trim($img, '"');
        }
    }

    return $rt;
}
