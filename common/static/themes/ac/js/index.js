function digg(url, mid, cid, d) {
    var saveid = GetCookie('diggid');
    if (saveid == cid) {
        // alert("您已经评价过！");
        layer.msg('亲，过多的赞美容易让人骄傲哟～');
    } else {
        $.ajax({
            type: 'POST',
            url: url,
            data: 'modelid=' + mid + '&id=' + cid + '&digg=' + d,
            dataType: 'json',
            success: function (msg) {
                if (msg.status == 1) {
                    var id = d ? 'up' : 'down';
                    $('#' + id).html(msg.message);
                } else {
                    layer.msg(msg.message);
                }
            },
        });

        SetCookie('diggid', cid, 1);
        return true;
    }
}

function GetCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + '=');
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(';', c_start);
            if (c_end == -1) {
                c_end = document.cookie.length;
            }
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return null;
}

function SetCookie(c_name, value, expiredays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + expiredays);
    document.cookie = c_name + '=' + escape(value) + (expiredays == null ? '' : ';expires=' + exdate.toGMTString());
}
function check_comm(obj) {
    const content = obj.content.value.trim();

    const code = obj.code.value.trim();

    if (content === '') {
        layer.msg('哎呀，你好像忘记写评论了？');
        return false;
    }

    if (code === '') {
        layer.msg('请输入验证码！');
        return false;
    }
}
$('#toggle-search').click(function () {
    $('.header').toggleClass('open');
    const has = $('.header').hasClass('open');
    $('#search-form').toggleClass('open');
    if (has) {
        $(this).children('i.mico-search').removeClass('icon-sousuo').addClass('icon-close');
    } else {
        $(this).children('i.mico-search').removeClass('icon-close').addClass('icon-sousuo');
    }
});
$('#min-man-btn').click(function () {
    $('.bgDiv').show();
    $('.mobile-sidebar').addClass('active');
});
$('.mobile-sidebar .Navleft').click(function () {
    $('.mobile-sidebar').removeClass('active');
    $('.bgDiv').hide();
});
$('.bgDiv').click(function () {
    $('.mobile-sidebar').removeClass('active');
    $('.bgDiv').hide();
});
$('.mobile-sidebar .navbar-item .icon-down').click(function () {
    const parent = $(this).parent().parent();
    parent.children('.sub-nav').slideToggle();
    parent.siblings().children('.sub-nav').slideUp();
});

$('.post-content')
    .find('img')
    .each(function () {
        $(this).attr('data-src', $(this).attr('src'));
        $(this).attr('src', '/common/static/images/lazy.png');
        $(this).addClass('lazy');
        const element = document.createElement('a');
        $(element).attr('data-fancybox', 'gallery');
        $(element).attr('href', $(this).attr('data-src'));
        $(this).wrap(element);
    });
$('img.lazy').lazyload({
    placeholder: '/common/static/images/lazy.png',
    skip_invisible: true,
    data_attribute: 'src', //要加载的图片是哪种data-后缀
});
$('#to-top').click(function () {
    $('html,body').animate({ scrollTop: 0 });
});
function scrollTops() {
    //当滚动条滚动距离大于等于100时，显示回到顶部按钮
    if ($(window).scrollTop() >= 100) {
        $('#to-top').fadeIn();
    } else {
        //否则隐藏按钮
        $('#to-top').fadeOut();
    }
}
scrollTops();
var position = $(window).scrollTop();
$(window).scroll(function () {
    scrollTops();
    var scroll = $(window).scrollTop();
    if (scroll > position) {
        console.log('向下滚动');
        $('#header').addClass('slideUp');
    } else {
        console.log('向上滚动');
        $('#header').removeClass('slideUp');
    }
    position = scroll;
});
