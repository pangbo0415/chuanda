/*
 *
 *  name: detailMotheds
 *  author: tonney
 *  date: 2019/04/17 14:15
 *
 */
$(function () {
    indexBannerInit();

    /*
    //加载页面时执行一次
    resourceChange();
    //监听浏览器宽度的改变
    window.onresize = function(){
        //console.log('监听浏览器宽度的改变');
        resourceChange();
    };
    */
});
const scrollTop = function () {
    const offset = 300,
        offset_opacity = 1200,
        scroll_top_duration = 700,
        $back_to_top = $('.backTop');
    $(window).scroll(function () {
        $(this).scrollTop() > offset ? $back_to_top.addClass('backTopVisible') : $back_to_top.removeClass('backTopVisible cd-fade-out');
        if ($(this).scrollTop() > offset_opacity) {
            $back_to_top.addClass('cd-fade-out');
        }
    });
    $back_to_top.on('click', function (event) {
        event.preventDefault();
        $('body,html').animate(
            {
                scrollTop: 0,
            },
            scroll_top_duration
        );
    });
    $('.boxfixed').theiaStickySidebar({
        // Settings
        additionalMarginTop: 30,
    });
};

$(function () {
    scrollTop();
    $('#layoutMenu').click(function () {
        $('#layoutNavGrid .navGrid, .layoutGroup').toggleClass('layout');
        $('body').toggleClass('layoutLocked');
    });
    $('#closeMenuBtn').click(function () {
        $('#layoutNavGrid .navGrid, .layoutGroup').removeClass('layout');
        $('body').removeClass('layoutLocked');
    });
    window.onresize = function () {
        documentChange();
    };
});

// 监听浏览器
const documentChange = function () {
    const docWidth = document.body.clientWidth;
    if (docWidth > 768) {
        $('#layoutNavGrid .navGrid, .layoutGroup').removeClass('layout');
        $('body').removeClass('layoutLocked');
        $('#articleNavGrid').addClass('whiteNav');
    } else {
        $('#articleNavGrid').removeClass('whiteNav');
    }
};
$(function () {
    const name = '';
    $('.articleDetail  .con_text')
        .find('img')
        .each(function () {
            if (!$(this).attr('data-original')) {
                $(this).addClass('lazy');
                $(this).attr('data-original', $(this).attr('src'));
                $(this).attr('src', '/common/static/images/lazy.png');
                // $(this).attr('src', "'<?php  echo  STATIC_URL;?>/images/lazy.png'");
            }
            //将src赋值到data-original，src赋值为预先准备的loding.gif
        });
    $('img.lazy').lazyload();
});
// 移动端菜单 - open
var touchMenuOpen = function () {
    $('#touchMenuOpen').on('click', function () {
        $('#menuLayout').addClass('on');
    });
};
// 移动端菜单 - close
var touchMenuClose = function () {
    $('#touchMenuClose').on('click', function () {
        $('#menuLayout').removeClass('on');
    });
};

var indexBannerInit = function () {
    // indexFocusSwiper
    var indexFocusSwiper = new Swiper('#indexFocus', {
        autoplay: { disableOnInteraction: false, delay: 5000 },
        pagination: { el: '#indexFocus .swiper-pagination' },
        navigation: { nextEl: '#indexFocus .swiper-button-next', prevEl: '#indexFocus .swiper-button-prev' },
        effect: 'fade',
        loop: true,
    });
};

var resourcePcInit = function () {
    indexBannerInit();
    // resourceGrid
    var resourceSwiper = new Swiper('#resourceGrid', {
        autoplay: { disableOnInteraction: false, delay: 3600 },
        navigation: { nextEl: '#resourceGrid .swiper-button-next', prevEl: '#resourceGrid .swiper-button-prev' },
        slidesPerView: 3,
        //spaceBetween: 20,
        //loop: true,
    });
};
var resourceWapInit = function () {
    indexBannerInit();
    // resourceGrid
    var resourceSwiper = new Swiper('#resourceGrid', {
        autoplay: { disableOnInteraction: false, delay: 3600 },
        navigation: { nextEl: '#resourceGrid .swiper-button-next', prevEl: '#resourceGrid .swiper-button-prev' },
        slidesPerView: 2,
        //spaceBetween: 20,
        //loop: true,
    });
};

// 监听浏览器 - 适配 资源 swiper
var resourceChange = function () {
    var docWidth = document.body.clientWidth;
    var indexSwiperStatus = $('#indexTopFocus').length > 0;
    //console.log('indexSwiperStatus:',indexSwiperStatus);
    if (docWidth <= 768) {
        // 移动端
        if (indexSwiperStatus) {
            resourceWapInit();
        }
        touchMenuOpen();
        touchMenuClose();
    } else {
        // pc端
        if (indexSwiperStatus) {
            resourcePcInit();
        }
    }
};
