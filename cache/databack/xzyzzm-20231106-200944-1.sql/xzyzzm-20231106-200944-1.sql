-- -----------------------------
-- YzmCMS MySQL Data Transfer 
-- 
-- Host     : 127.0.0.1
-- Port     : 3306
-- Database : share
-- 
-- Part : #1
-- Date : 2023-11-06 20:09:44
-- -----------------------------

SET FOREIGN_KEY_CHECKS = 0;


-- -----------------------------
-- Table structure for `yzm_admin`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_admin`;
CREATE TABLE `yzm_admin` (
  `adminid` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `adminname` varchar(30) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `roleid` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `rolename` varchar(30) NOT NULL DEFAULT '',
  `realname` varchar(30) NOT NULL DEFAULT '',
  `nickname` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(30) NOT NULL DEFAULT '',
  `logintime` int(10) unsigned NOT NULL DEFAULT '0',
  `loginip` varchar(15) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `errnum` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `addpeople` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`adminid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `yzm_admin`
-- -----------------------------
INSERT INTO `yzm_admin` VALUES ('1','admin','ffe317ecad2405070e6f5ffefb3a38a5','1','超级管理员','','','','1699272521','127.0.0.1','1696991980','0','创始人');

-- -----------------------------
-- Table structure for `yzm_admin_log`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_admin_log`;
CREATE TABLE `yzm_admin_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module` varchar(15) NOT NULL DEFAULT '',
  `controller` varchar(20) NOT NULL DEFAULT '',
  `querystring` varchar(255) NOT NULL DEFAULT '',
  `adminid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `adminname` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(15) NOT NULL DEFAULT '',
  `logtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `logtime` (`logtime`),
  KEY `adminid` (`adminid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_admin_login_log`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_admin_login_log`;
CREATE TABLE `yzm_admin_login_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `adminname` varchar(30) NOT NULL DEFAULT '',
  `logintime` int(10) unsigned NOT NULL DEFAULT '0',
  `loginip` varchar(15) NOT NULL DEFAULT '',
  `address` varchar(30) NOT NULL DEFAULT '',
  `password` varchar(30) NOT NULL DEFAULT '',
  `loginresult` tinyint(1) NOT NULL DEFAULT '0' COMMENT '登录结果1为登录成功0为登录失败',
  `cause` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `admin_index` (`adminname`,`loginresult`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `yzm_admin_login_log`
-- -----------------------------
INSERT INTO `yzm_admin_login_log` VALUES ('1','admin','1696992041','127.0.0.1','','','1','登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES ('2','admin','1696993941','127.0.0.1','','','1','登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES ('3','admin','1697020549','127.0.0.1','','','1','登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES ('4','admin','1697072790','127.0.0.1','','','1','登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES ('5','admin','1697115509','127.0.0.1','','','1','登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES ('6','admin','1697166126','127.0.0.1','','','1','登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES ('7','admin','1697271107','127.0.0.1','','','1','登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES ('8','admin','1697296002','127.0.0.1','','','1','登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES ('9','admin','1697418970','127.0.0.1','','','1','登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES ('10','admin','1697444249','127.0.0.1','','','1','登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES ('11','admin','1697591412','127.0.0.1','','','1','登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES ('12','admin','1697685856','127.0.0.1','','','1','登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES ('13','admin','1697699860','127.0.0.1','','','1','登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES ('14','admin','1697732040','127.0.0.1','','','1','登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES ('15','pangbo','1698988561','127.0.0.1','','a11523518','0','该用户不存在！');
INSERT INTO `yzm_admin_login_log` VALUES ('16','pangbo','1698988570','127.0.0.1','','a11523518','0','该用户不存在！');
INSERT INTO `yzm_admin_login_log` VALUES ('17','admin','1699184477','127.0.0.1','','a11523518','0','密码错误！');
INSERT INTO `yzm_admin_login_log` VALUES ('18','admin','1699184483','127.0.0.1','','','1','登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES ('19','admin','1699252860','127.0.0.1','','','1','登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES ('20','admin','1699269645','127.0.0.1','','','1','登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES ('21','admin','1699272521','127.0.0.1','','','1','登录成功！');

-- -----------------------------
-- Table structure for `yzm_admin_role`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_admin_role`;
CREATE TABLE `yzm_admin_role` (
  `roleid` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `rolename` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `system` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`roleid`),
  KEY `disabled` (`disabled`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `yzm_admin_role`
-- -----------------------------
INSERT INTO `yzm_admin_role` VALUES ('1','超级管理员','超级管理员','1','0');
INSERT INTO `yzm_admin_role` VALUES ('2','总编','总编','1','0');
INSERT INTO `yzm_admin_role` VALUES ('3','发布人员','发布人员','1','0');

-- -----------------------------
-- Table structure for `yzm_admin_role_priv`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_admin_role_priv`;
CREATE TABLE `yzm_admin_role_priv` (
  `roleid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `m` char(20) NOT NULL DEFAULT '',
  `c` char(20) NOT NULL DEFAULT '',
  `a` char(30) NOT NULL DEFAULT '',
  `data` char(100) NOT NULL DEFAULT '',
  KEY `roleid` (`roleid`,`m`,`c`,`a`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_adver`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_adver`;
CREATE TABLE `yzm_adver` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '1文字2代码3图片',
  `title` varchar(100) NOT NULL DEFAULT '',
  `url` varchar(200) NOT NULL DEFAULT '',
  `text` varchar(200) NOT NULL DEFAULT '',
  `img` varchar(200) NOT NULL DEFAULT '',
  `code` text NOT NULL,
  `describe` varchar(250) NOT NULL DEFAULT '',
  `inputtime` int(10) unsigned NOT NULL DEFAULT '0',
  `start_time` int(10) unsigned NOT NULL DEFAULT '0',
  `end_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `yzm_adver`
-- -----------------------------
INSERT INTO `yzm_adver` VALUES ('2','3','云小站特惠','https://www.aliyun.com/minisite/goods?userCode=0x08kl9y','','/uploads/202310/11/231011014534882.png','<a href=\"https://www.aliyun.com/minisite/goods?userCode=0x08kl9y\" target=\"_blank\" title=\"云小站特惠\"><img src=\"/uploads/202310/11/231011014534882.png\"></a>','云小站特惠价','1697002825','0','0');
INSERT INTO `yzm_adver` VALUES ('3','3','免费试用','https://free.aliyun.com?userCode=0x08kl9y','','/uploads/202310/11/231011015023239.png','<a href=\"https://free.aliyun.com?userCode=0x08kl9y\" target=\"_blank\" title=\"免费试用\"><img src=\"/uploads/202310/11/231011015023239.png\"></a>','','1697003441','0','0');
INSERT INTO `yzm_adver` VALUES ('4','3','云服务器省钱攻略','https://www.aliyun.com/daily-act/ecs/ecs_trial_benefits?userCode=0x08kl9y','','/uploads/202310/11/231011015139548.png','<a href=\"https://www.aliyun.com/daily-act/ecs/ecs_trial_benefits?userCode=0x08kl9y\" target=\"_blank\" title=\"云服务器省钱攻略\"><img src=\"/uploads/202310/11/231011015139548.png\"></a>','','1697003504','0','0');
INSERT INTO `yzm_adver` VALUES ('5','3','首页右侧','http://localhost:94/yule/17.html','','/uploads/ueditor/image/20231012/1697095434851942.jpg','<a href=\"http://localhost:94/yule/17.html\" target=\"_blank\" title=\"首页右侧\"><img src=\"/uploads/ueditor/image/20231012/1697095434851942.jpg\"></a>','','1697699911','0','0');

-- -----------------------------
-- Table structure for `yzm_all_content`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_all_content`;
CREATE TABLE `yzm_all_content` (
  `allid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `siteid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `modelid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` char(30) NOT NULL DEFAULT '',
  `title` varchar(150) NOT NULL DEFAULT '',
  `inputtime` int(10) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0',
  `url` varchar(100) NOT NULL DEFAULT '',
  `thumb` varchar(150) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `issystem` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`allid`),
  KEY `userid_index` (`userid`,`issystem`,`status`),
  KEY `modelid_index` (`modelid`,`id`),
  KEY `status` (`siteid`,`status`),
  KEY `issystem` (`siteid`,`issystem`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `yzm_all_content`
-- -----------------------------
INSERT INTO `yzm_all_content` VALUES ('15','0','1','15','15','1','admin','近十年拿不到房本，问进度没个谱，问题卡在了哪儿？','1697094604','1697095041','http://localhost:94/kaidian/15.html','/uploads/202310/12/231012031047415.png','1','1');
INSERT INTO `yzm_all_content` VALUES ('4','0','1','16','4','1','admin','晚上不到10点就困了，总是凌晨3、4点就醒来，是衰老的征兆吗？','1696993595','1697088615','http://localhost:94/yunying/4.html','/uploads/202310/11/231011110656650.png','1','1');
INSERT INTO `yzm_all_content` VALUES ('6','0','1','16','6','1','admin','周杰伦上海演唱会11万张票484万人想看，开唱前一天黄牛集体退票','1697003251','1697087831','http://localhost:94/yunying/6.html','/uploads/202310/12/231012011655423.png','1','1');
INSERT INTO `yzm_all_content` VALUES ('24','0','1','15','24','1','admin','成都一初中生在校身亡，家长两次被学校藏手机？当地称正调查 ','1697099436','1697099878','http://localhost:94/kaidian/24.html','/uploads/ueditor/image/20231012/thumb_500_300_1697099456308642.jpeg','1','1');
INSERT INTO `yzm_all_content` VALUES ('23','0','1','15','23','1','admin','婚内出轨包养情妇？郑强回应 ','1697099133','1697099185','http://localhost:94/kaidian/23.html','','1','1');
INSERT INTO `yzm_all_content` VALUES ('8','0','1','16','8','1','admin',' 抖音10天涨粉百万的于文亮是谁','1697073517','1697073592','http://localhost:94/yunying/8.html','/uploads/202310/12/231012091914893.png','1','1');
INSERT INTO `yzm_all_content` VALUES ('9','0','1','16','9','1','admin','北京政法职业学院3霸凌女生疑亮相学校宣传片，校方：将核实了解情况','1697087159','1697087267','http://localhost:94/yunying/9.html','/uploads/202310/12/231012010709218.png','1','1');
INSERT INTO `yzm_all_content` VALUES ('11','0','1','16','11','1','admin','程序员工作只能做到 35 岁吗？之后的路是怎么走的呢?','1697088218','1697088371','http://localhost:94/yunying/11.html','/uploads/202310/12/231012012121766.png','1','1');
INSERT INTO `yzm_all_content` VALUES ('12','0','1','16','12','1','admin','2023年应届生平均招聘月薪10342元','1697088442','1697088530','http://localhost:94/yunying/12.html','/uploads/202310/12/231012012820879.jpeg','1','1');
INSERT INTO `yzm_all_content` VALUES ('13','0','1','16','13','1','admin','租客怀孕房东要求退租','1697088702','1697088858','http://localhost:94/yunying/13.html','/uploads/202310/12/231012013301221.png','1','1');
INSERT INTO `yzm_all_content` VALUES ('14','0','1','15','14','1','admin','媒体：“指鼠为鸭”才过去4个月，“指鼠为牛”接棒','1697088945','1697089047','http://localhost:94/kaidian/14.html','/uploads/ueditor/image/20231012/1697089038599153.png','1','1');
INSERT INTO `yzm_all_content` VALUES ('16','0','1','15','16','1','admin','最新公报发布：我国去年出生人口956万，二孩占比近4成','1697095054','1697095104','http://localhost:94/kaidian/16.html','','1','1');
INSERT INTO `yzm_all_content` VALUES ('17','0','1','15','17','1','admin','华为5.5G手机要来了，与5G相比有哪些提升与革新？我有必要为了5.5G换手机吗？','1697095205','1697095437','http://localhost:94/kaidian/17.html','/uploads/202310/12/231012032312351.jpg','1','1');
INSERT INTO `yzm_all_content` VALUES ('18','0','1','15','18','1','admin','江西女子在明月山徒步失联11天，事发前半小时还与女儿通电话','1697095583','1697095649','http://localhost:94/kaidian/18.html','','1','1');
INSERT INTO `yzm_all_content` VALUES ('21','0','1','15','21','1','admin','重庆一 33 层楼房楼底被私挖 4 室 1 厅，这可能会带来什么后果？如何看待此事？','1697098219','1697098245','http://localhost:94/kaidian/21.html','/uploads/ueditor/image/20231012/thumb_500_300_1697098240180962.jpg','1','1');
INSERT INTO `yzm_all_content` VALUES ('22','0','1','15','22','1','admin','曝高洪波将离开中国足协 记者:主动退赃员工还能受重用吗？ ','1697098299','1697098319','http://localhost:94/kaidian/22.html','/uploads/ueditor/image/20231012/thumb_500_300_1697098317992435.jpeg','1','1');
INSERT INTO `yzm_all_content` VALUES ('25','0','1','15','25','1','admin','云南一烟草复烤厂厂长被举报婚内出轨并生下私生女，纪委回应','1697100208','1697100255','http://localhost:94/kaidian/25.html','/uploads/ueditor/image/20231012/thumb_500_300_1697100247111191.jpg','1','1');
INSERT INTO `yzm_all_content` VALUES ('26','0','1','15','26','1','admin','2名电诈集团头目被通缉：在缅甸掸邦第二特区分别任建设部长和县长','1697100571','1697100597','http://localhost:94/kaidian/26.html','/uploads/ueditor/image/20231012/thumb_500_300_1697100593868873.jpg','1','1');
INSERT INTO `yzm_all_content` VALUES ('27','0','1','15','27','1','admin','中学运动会表演刺杀安倍惹争议 教育局回应：允许学生犯错','1697100663','1697100883','http://localhost:94/kaidian/27.html','/uploads/ueditor/image/20231012/thumb_500_300_1697100879108770.png','1','1');
INSERT INTO `yzm_all_content` VALUES ('28','0','1','15','28','1','admin','有哪些一看就莫名感到伤感的图片？','1697101860','1697101978','http://localhost:94/kaidian/28.html','/uploads/ueditor/image/20231012/thumb_500_300_1697101936817335.png','1','1');
INSERT INTO `yzm_all_content` VALUES ('29','0','1','15','29','1','admin','北大“韦神”32岁无车无房被嘲讽？工资曝光，网友们不讲话了','1697166149','1697271119','http://localhost:94/kaidian/29.html','http://localhost:94/uploads/202310/13/20231013110258646.webp','1','1');

-- -----------------------------
-- Table structure for `yzm_article`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_article`;
CREATE TABLE `yzm_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `nickname` varchar(30) NOT NULL DEFAULT '',
  `title` varchar(180) NOT NULL DEFAULT '',
  `color` char(9) NOT NULL DEFAULT '',
  `inputtime` int(10) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0',
  `keywords` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `click` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  `copyfrom` varchar(50) NOT NULL DEFAULT '',
  `thumb` varchar(150) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL DEFAULT '',
  `flag` varchar(12) NOT NULL DEFAULT '' COMMENT '1置顶,2头条,3特荐,4推荐,5热点,6幻灯,7跳转',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `issystem` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `listorder` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `groupids_view` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '阅读权限',
  `readpoint` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '阅读收费',
  `paytype` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '收费类型',
  `is_push` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否百度推送',
  `up` int(10) unsigned NOT NULL DEFAULT '0',
  `down` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `status` (`status`,`listorder`),
  KEY `catid` (`status`,`catid`),
  KEY `userid` (`status`,`userid`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `yzm_article`
-- -----------------------------
INSERT INTO `yzm_article` VALUES ('15','15','1','admin','管理员','近十年拿不到房本，问进度没个谱，问题卡在了哪儿？','','1697094604','1697095041','十年,不到,房本,进度,没个,问题','苦恼近十年了没房本西北四环外、西郊线茶棚站旁数百米距离，是2010年前后陆续修建起来的北坞嘉园居住区。该居住区分为北里、南里和西里三部分，各有居民楼30栋楼左右。这里的...','153','<div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\"><span class=\"qnt-strong\" style=\"overflow-wrap: break-word; word-break: break-all; font-weight: 700;\">苦恼</span></div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\"><span class=\"qnt-strong\" style=\"overflow-wrap: break-word; word-break: break-all; font-weight: 700;\">近十年了没房本</span></div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">西北四环外、西郊线茶棚站旁数百米距离，是2010年前后陆续修建起来的北坞嘉园居住区。该居住区分为北里、南里和西里三部分，各有居民楼30栋楼左右。这里的住户，几乎都是当初从周边北坞、中邬、茶棚等数个村回迁而来。然而居民反映，<span class=\"qnt-strong\" style=\"overflow-wrap: break-word; word-break: break-all; font-weight: 700;\">近十年时间过去，南里和西里的居民仍迟迟没见到“大红本”，也不知道“卡”在了哪里。</span></div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">“大概2015年过来的，原来我是北坞村的，现在户口还是用原来村上的地址没变。”南里8号楼下，在小花园中忙活的刘大爷说。因为无法办理房产证，居民也就无法把户口迁移到现在的房子里，“北里几年前差不多陆续解决了，咱们解不解决，也没听着个声儿。”</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">有同样困惑的，还有北坞嘉园西里的居民。在北坞嘉园西里，记者采访多位居民，均回应自己的房屋没有房产证。“你也别问了。南里和西里的人，都没有房产证。”一位居民说，自己原本是中邬村村民，2014年底入住，“当时大家伙儿都是抢着时间先住进来的，手里只有一份安置协议。”记者从其出示的数页协议内容看，并未发现有专门环节清晰约定拿到房本的时间。多位居民说，<span class=\"qnt-strong\" style=\"overflow-wrap: break-word; word-break: break-all; font-weight: 700;\">有了房产证心里才踏实，迁移户口、租售房屋等也才能更方便、放心。</span></div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\"><div class=\"qnt-img\" style=\"max-width: 100%; width: 680px; display: inline-block; vertical-align: top; overflow-wrap: break-word; word-break: break-all; margin-bottom: 0px;\"><div style=\"border-radius: 6px; overflow: hidden; width: 680px;\"><div class=\"lazyload-wrapper \" style=\"width: 680px; display: flex; justify-content: center; position: relative;\"><img class=\"qnt-img-img\" src=\"https://inews.gtimg.com/om_bt/OmofHqwkJZ8FtTOhO1oG5Q6w3YKZRadjuOVfYVibt3K38AA/641\" alt=\"图片\" style=\"border: 0px none; vertical-align: middle; width: 680px; height: 422.25px;\"/></div></div></div></div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\"><span class=\"qnt-strong\" style=\"overflow-wrap: break-word; word-break: break-all; font-weight: 700;\">尴尬</span></div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\"><span class=\"qnt-strong\" style=\"overflow-wrap: break-word; word-break: break-all; font-weight: 700;\">问进度也没个谱</span></div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">北坞嘉园南里和西里没有房产证一事，近几年陆续有居民反映。但多位居民表示，此事到底进展到了哪一步、什么时候能解决，并不清楚。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">记者在人民网“领导留言板”上看到，仅从去年至今，就有十多位小区居民，反映北坞嘉园南里和西里房产证“难产”一事，多位居民询问“问题出在哪里”。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">最新一条发布于9月14日的居民留言为北坞嘉园西里已经交付10年，“到底因为什么迟迟不给办（房本），将来给不给办……如果是在办理中，必须告知办理到什么程度，哪个部门在负责。”也有居民去年留言称，“请领导核查是卡在什么地方了？问村委会和物业一问三不知。没有房本孩子户口也迁不回来……”</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">记者注意到，<span class=\"qnt-strong\" style=\"overflow-wrap: break-word; word-break: break-all; font-weight: 700;\">从去年3月至今年9月，针对居民的留言反映，相关方面的回复大都有一句——“目前，玉泉村协调国土、规划部门推进北坞嘉园西里房产证办理中，请您耐心等候。”</span></div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\"><div class=\"qnt-img\" style=\"max-width: 100%; width: 680px; display: inline-block; vertical-align: top; overflow-wrap: break-word; word-break: break-all; margin-bottom: 0px;\"><div style=\"border-radius: 6px; overflow: hidden; width: 680px;\"><div class=\"lazyload-wrapper \" style=\"width: 680px; display: flex; justify-content: center; position: relative;\"><p style=\"text-align:center\"><img class=\"qnt-img-img\" src=\"https://inews.gtimg.com/om_bt/OjK5u_SLyjN7lZqmTUt3KVIHU78p5wFjd2ixZ1-NCbcScAA/1000\" alt=\"图片\" style=\"border: 0px none; vertical-align: middle; width: 680px; height: 515.594px;\"/></p></div></div></div></div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\"><span class=\"qnt-strong\" style=\"overflow-wrap: break-word; word-break: break-all; font-weight: 700;\">回应</span></div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\"><span class=\"qnt-strong\" style=\"overflow-wrap: break-word; word-break: break-all; font-weight: 700;\">房本办理材料尚未提交</span></div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">房产证“难产”究竟难在何处、进度如何？玉泉社区居委会工作人员回应，社区不负责房产证的事，建议咨询物业。小区物业公司工作人员称，西里和南里居民的确均没有办理房产证，也不知道房本办理的进度。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">海淀区不动产登记事务中心工作人员答复，目前确实无法办理房产证，“问题出在了你们镇里，建议去咨询镇里。”四季青镇城建科工作人员回应，工作流程大致是，开发商或者村委会有一批要办房本的名单交到该科。科室核实后盖章，而后对方再拿着手续去不动产登记中心办理房本。“目前我们还没收到南里和西里交过来的名单。只要名单交过来，就相当于走程序了，后续就比较快了，建议问开发商。”</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">记者注意到，在人民网“领导留言板”，市规自委海淀分局9月15日回复居民诉求时表示，<span class=\"qnt-strong\" style=\"overflow-wrap: break-word; word-break: break-all; font-weight: 700;\">“经了解，该项目开发商并未向海淀区不动产登记中心申请首次登记，因此无法启动个人产权登记手续。”而当记者询问开发商公司工作人员时，其表示“进度我们也不清楚，手续没下来，所以办不了。具体到哪个环节，缺什么手续，我也不知道。”</span></div><p><br/></p>','网络','/uploads/202310/12/231012031047415.png','http://localhost:94/kaidian/15.html','4','1','1','10','0','0','1','0','1','0');
INSERT INTO `yzm_article` VALUES ('4','16','1','admin','管理员','晚上不到10点就困了，总是凌晨3、4点就醒来，是衰老的征兆吗？','','1696993595','1697088615','晚上,不到,10点,总是,凌晨,4点','“最近总是不到十点就开始困，所以我一般十点就开始睡，睡到早上六点起床，刚好可以睡满8小时。但奇怪的是，经常在凌晨三四点左右醒来，然后就睡不着了，不是老人才会出现这种...','160','<p data-track=\"1\">“最近总是不到十点就开始困，所以我一般十点就开始睡，睡到早上六点起床，刚好可以睡满8小时。</p><p data-track=\"2\">但奇怪的是，经常在凌晨三四点左右醒来，然后就睡不着了，不是老人才会出现这种情况吗？难道我这是衰老的征兆吗？”</p><p data-track=\"3\">这是一则粉丝发来的求助私信，那经常凌晨两三点醒来，到底是怎么回事？</p><h1>一、总在凌晨3、4点就醒来，是衰老征兆？</h1><p data-track=\"5\">不少人有这样的经历，明明距离闹钟还有几个小时，却在凌晨3、4点醒来后，难以再入睡。那么几点算早醒？早醒是身体衰老的表现吗？</p><p data-track=\"6\">“早醒”是中老年人常见的失眠症状，表现为上床睡着一段时间后，突然醒来，然后再难以入睡。一般在临床医学上，常将凌晨2点至4点醒来，醒后再也睡不着的情况称为早醒。</p><div><img src=\"https://p3-sign.toutiaoimg.com/tos-cn-i-qvj2lq49k0/0e810a2f4ac544b48bf69dc1f98a394d~tplv-tt-origin-asy2:5aS05p2hQDM55YGl5bq3572R.image?_iz=58558&from=article.pc_detail&x-expires=1697693355&x-signature=lgS7k5aBTkxRXdo23jvUfX23t%2FE%3D\" img_width=\"750\" img_height=\"475\" image_type=\"1\" mime_type=\"image/png\" web_uri=\"tos-cn-i-qvj2lq49k0/0e810a2f4ac544b48bf69dc1f98a394d\" class=\"syl-page-img\" style=\"box-sizing: border-box; border: 0px; margin: 0px auto 8px; padding: 0px; cursor: zoom-in; max-width: 100%; display: block; border-radius: 4px; height: auto;\"/><p><br/></p></div><p data-track=\"7\">上了年纪以后，分泌的褪黑素和生长激素会逐渐减少，导致睡眠质量降低，从而出现早醒的情况，那年轻人经常早醒暗示了什么？</p><p data-track=\"8\">首先，我们先来了解下是什么在影响睡眠，其实睡眠的长短与人体的激素有关，尤其是以下2种：</p><p data-track=\"9\">一是皮质醇，皮质醇是唤醒激素，在早上8点左右，皮质醇水平最高，达到上午高点后，水平骤然下降，最低点约在凌晨2点。压力大也会导致皮质醇水平增高，当压力越大，人就越容易醒或者难以入睡。</p><p data-track=\"10\">二是褪黑素，褪黑素是入眠激素，白天阳光抑制大脑松果体，减少褪黑素分泌，夜间松果体受到甲肾上腺信号，开始分泌褪黑素帮助我们入眠。</p><p data-track=\"11\">而过早的醒来，是睡眠障碍的典型表现。早醒使晚间积累的睡眠时间变得更短，造成深夜睡眠不足，起床后头晕眼花，在临床上，这类人容易出现焦虑紧张等情绪。</p><p data-track=\"12\">如果频繁早醒，就需要警惕这些情况，包括缺乏维生素D、甲状腺疾病、呼吸问题、睡眠呼吸暂停症、轻度抑郁、胃酸逆流以及夜尿频繁等疾病的可能性。</p><div><img src=\"https://p3-sign.toutiaoimg.com/tos-cn-i-qvj2lq49k0/59b7822bc4d144ed86fbdabe432a7ccf~tplv-tt-origin-asy2:5aS05p2hQDM55YGl5bq3572R.image?_iz=58558&from=article.pc_detail&x-expires=1697693355&x-signature=VyPevevxVZRuC%2BCziyO9lq0%2BTHE%3D\" img_width=\"750\" img_height=\"512\" image_type=\"1\" mime_type=\"image/png\" web_uri=\"tos-cn-i-qvj2lq49k0/59b7822bc4d144ed86fbdabe432a7ccf\" class=\"syl-page-img\" style=\"box-sizing: border-box; border: 0px; margin: 0px auto 8px; padding: 0px; cursor: zoom-in; max-width: 100%; display: block; border-radius: 4px; height: auto;\"/><p><br/></p></div><h1>二、“8小时睡眠论”靠谱吗？</h1><p data-track=\"14\">在《中国睡眠研究报告2023》报告中，2022年中国人每晚平均睡眠时间为7.4小时，虽整体有所改善，但依然有47.55%的人睡不够8小时，更有16.79%的人睡不够7小时，另外还有10.4%的人认为自己的睡眠质量非常差。</p><p data-track=\"15\">可能大家都听说“每天睡满8小时”的睡眠论，这个理论最早其实是和“八小时工作制”一起提出来的。</p><p data-track=\"16\">在《资本论》中，提到“八小时工作、八小时休闲、八小时睡眠”，简称为“888工作制”。就这样，8小时睡眠论由此诞生。</p><p data-track=\"17\">但实际上，这种“一刀切”的理论反而不利于睡眠，因为个体之间存在差异，执着于8小时反而会影响睡眠。</p><p data-track=\"18\">那么，最佳的睡眠时长应该是多长呢？美国国家睡眠基金会在2018年的研究中，给出的建议是：18~64岁的成年人，每天需要的睡眠时间在7~9小时。</p><p data-track=\"19\">哪个时间点入睡最健康？</p><p data-track=\"20\">刊登在European Heart Journal-Digital Health上，收集了英国生物库月10多万名参与者的数据后，最终认为最佳入眠时间应是晚上10点—11点之间，而在凌晨入睡，患病风险最高。</p><div><img src=\"https://p3-sign.toutiaoimg.com/tos-cn-i-qvj2lq49k0/246c5284ac4145108782db6f09d6eb05~tplv-tt-origin-asy2:5aS05p2hQDM55YGl5bq3572R.image?_iz=58558&from=article.pc_detail&x-expires=1697693355&x-signature=k%2BZOvvoyqpOtCALD%2BcV2YV0jhMQ%3D\" img_width=\"750\" img_height=\"500\" image_type=\"1\" mime_type=\"image/jpeg\" web_uri=\"tos-cn-i-qvj2lq49k0/246c5284ac4145108782db6f09d6eb05\" class=\"syl-page-img\" style=\"box-sizing: border-box; border: 0px; margin: 0px auto 8px; padding: 0px; cursor: zoom-in; max-width: 100%; display: block; border-radius: 4px; height: auto;\"/><p><br/></p></div><h1 data-track=\"21\">三、经常早醒会影响健康吗？做好3件事</h1><p data-track=\"22\">随着年龄增长，激素分泌减少，就会影响到睡眠状态，出现早醒现象。不要小看早醒，给身体带来危害一点都不少——由于睡眠不足，次日会感觉到疲惫，注意力不集中，做事效率低，长期如此，还会引起胸闷、心悸等情况。</p><p data-track=\"23\">那有什么方法可以缓解这样的情况呢？不妨做好以下几件事：</p><p data-track=\"24\">1、规律生活</p><p data-track=\"25\">尽量每天同一时刻起床，包括休息日，也不宜睡得过晚，调好“生物钟”。过多的赖床也会导致睡眠片段化或浅睡眠。</p><p data-track=\"26\">2、控制饮食</p><p data-track=\"27\">不要空腹或饱腹上床，避免过多摄入咖啡因饮料，会影响中枢神经，不利于入睡。</p><div><img src=\"https://p3-sign.toutiaoimg.com/tos-cn-i-qvj2lq49k0/07f21b0d2da54e05a2c2385ef2f084cc~tplv-tt-origin-asy2:5aS05p2hQDM55YGl5bq3572R.image?_iz=58558&from=article.pc_detail&x-expires=1697693355&x-signature=%2FbhqWjFCFOmsh9RBUd3oTRhVXUg%3D\" img_width=\"750\" img_height=\"500\" image_type=\"1\" mime_type=\"image/jpeg\" web_uri=\"tos-cn-i-qvj2lq49k0/07f21b0d2da54e05a2c2385ef2f084cc\" class=\"syl-page-img\" style=\"box-sizing: border-box; border: 0px; margin: 0px auto 8px; padding: 0px; cursor: zoom-in; max-width: 100%; display: block; border-radius: 4px; height: auto;\"/><p><br/></p></div><p data-track=\"28\">3、学会放松</p><p data-track=\"29\">睡前可以试试做个深呼吸，如腹式呼吸放松，双眼闭上，双手可放在腹部，缓慢深吸气，暂停1秒～2秒；缓慢呼气，再暂停1秒～2秒。</p><p data-track=\"30\">也可以试试渐渐肌肉放松的方式：先紧紧握拳，收缩前臂，体会肌肉紧张后胀、酸、麻的感觉，再逐渐放松打开，体会放松舒适、血流灌注后温暖的感觉。</p><p data-track=\"31\">4、睡前准备</p><p data-track=\"32\">睡前可以洗个热水澡或泡个脚，穿宽松睡衣，选择舒适的床上用品，营造一个安静、舒适的睡眠环境。</p><div><img src=\"https://p3-sign.toutiaoimg.com/tos-cn-i-qvj2lq49k0/eb0c401bf2d54614b557ee059dcfa9bd~tplv-tt-origin-asy2:5aS05p2hQDM55YGl5bq3572R.image?_iz=58558&from=article.pc_detail&x-expires=1697693355&x-signature=Re7pVM0zGU8387nTH1HFOFXZNgw%3D\" img_width=\"750\" img_height=\"565\" image_type=\"1\" mime_type=\"image/png\" web_uri=\"tos-cn-i-qvj2lq49k0/eb0c401bf2d54614b557ee059dcfa9bd\" class=\"syl-page-img\" style=\"box-sizing: border-box; border: 0px; margin: 0px auto 8px; padding: 0px; cursor: zoom-in; max-width: 100%; display: block; border-radius: 4px; height: auto;\"/><p><br/></p></div><p data-track=\"33\">长期的失眠不利于身体健康，若尝试过以上几种方法，睡眠没有改善，就要注意可能是疾病引起，建议到医院就诊，找到根源，才能从根本上改善睡眠。</p><p><br/></p>','网络','/uploads/202310/11/231011110656650.png','http://localhost:94/yunying/4.html','4','1','1','10','0','0','1','0','0','0');
INSERT INTO `yzm_article` VALUES ('6','16','1','admin','管理员','周杰伦上海演唱会11万张票484万人想看，开唱前一天黄牛集体退票','','1697003251','1697087831','周杰伦,黄牛','        据上观新闻10月11日报道，周杰伦上海站演唱会今天开唱，12日-15日连唱四天，演唱会门票却一票难求，黄牛票炒到上万元。11日，出现有黄牛集体退票的情况，记者询问了一...','195','<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;据上观新闻10月11日报道，周杰伦上海站演唱会今天开唱，12日-15日连唱四天，演唱会门票却一票难求，黄牛票炒到上万元。11日，出现有黄牛集体退票的情况，记者询问了一位黄牛卖家，该卖家表示，主办方原定的给录入信息如今不兑现，因此都在退单。</div><div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;周杰伦2023“嘉年华”世界巡回演唱会上海站将于10月12日至15日举行，9月12日18时开启门票预售。</div><div><div><div><div><p style=\"text-align:center\"><img src=\"/uploads/ueditor/image/20231012/1697087825305988.png\" title=\"周杰伦上海演唱会11万张票484万人想看，开唱前一天黄牛集体退票\" alt=\"周杰伦上海演唱会11万张票484万人想看，开唱前一天黄牛集体退票\"/></p></div></div><div>图片来源：周杰伦歌迷会官方微博截图</div></div></div><div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;根据大麦、票星球、猫眼三个平台公布的演唱会门票数据，本次周杰伦上海演唱会共放出超11万张门票,其中大麦网约放出7万张，猫眼放出约4.1万张，而票星球只放出2400张票。然而单单大麦平台上就有484万人标记了“想看”。</div><div><div><div><div><p style=\"text-align:center\"><img class=\"qnt-img-img\" src=\"https://inews.gtimg.com/om_bt/OL6y0CZEX_jDxT_NT-lhn2pN7XvV9bUqaJHPTjPm3C2dUAA/641\" alt=\"图片\" style=\"border: 0px none; vertical-align: middle; width: 680px; height: 607.75px;\"/></p></div></div></div></div><div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;即使一次性出售11万张门票。不少粉丝仍表示抢票艰难，有粉丝哭诉“到底是谁抢走了我的票？”据悉，本场演出为购票实名制入场，门票可退，但不得转赠及转售，演出开始前48小时停止退票。</div><div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在强实名制下，入场时需要进行“人脸识别”，购票者与观演人必须一致。“强实名制”购票规则下，每张身份证仅可购买每场演出的一张门票， 门票不可转赠，但可在规定时间内退票。</div><div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;此前，天津站未采用“强实名制”，原价2000元内场票被哄抬至十万元。值得注意的是，目前，在强实名制下，“黄牛”的业务已经不是倒买倒卖了，而是“代抢”。他们会提前绑定客户的身份证信息，如果没有抢到票，“代抢”的钱100%退还给客户。正是这种试一试又不吃亏的心态，让不少周杰伦的铁粉选择了“代抢”。</div><div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;近日，记者以消费者身份咨询了一位“黄牛”，对方表示，周杰伦上海场的看台“代抢”价格为1500元一张，而看台的门票价格为580元-1280元不等；内场2080价格的门票“代抢”价格2500元，内场其他价格的门票“代抢”价格2000元。据了解，有些是人工“代抢”，更高级的则会借助特殊软件。</div><div>每日经济新闻综合上观新闻、澎湃新闻、第一财经、看看新闻、新闻坊、网友评论</div><p><br/></p>','网络','/uploads/202310/12/231012011655423.png','http://localhost:94/yunying/6.html','4','1','1','10','0','0','1','0','2','0');
INSERT INTO `yzm_article` VALUES ('26','15','1','admin','管理员','2名电诈集团头目被通缉：在缅甸掸邦第二特区分别任建设部长和县长','','1697100571','1697100597','2名,电诈,集团,头目,通缉,缅甸','据@公安部刑侦局10月12日消息，公安机关公开通缉2名电信网络诈骗犯罪集团头目，对提供有效线索和协助抓捕的有功人员，公安机关将给予10万至50万元人民币奖励。据昆明市公安局...','102','<p>据@公安部刑侦局10月12日消息，公安机关公开通缉2名电信网络诈骗犯罪集团头目，对提供有效线索和协助抓捕的有功人员，公安机关将给予10万至50万元人民币奖励。</p><p>据昆明市公安局和杭州市公安局发布的悬赏通报，这两名电诈集团头目分别为：肖岩块，户籍地在广东省河源市源城区富景西苑，现任缅甸掸邦第二特区勐能县县长；陈岩板，户籍地在云南省普洱市思茅区明礼巷，现任缅甸掸邦第二特区建设部部长。</p><p>@公安部刑侦局消息称，当前，缅北涉我电信网络诈骗犯罪形势严峻，社会危害严重，人民群众反映强烈。经浙江省杭州市公安局、云南省昆明市公安局侦查发现，陈岩板（又名鲍岩板）、肖岩块（又名何春田）系缅北涉我电信网络诈骗犯罪集团重要头目，长期组织开设诈骗窝点，实施针对中国公民的电信网络诈骗犯罪活动，诈骗数额巨大、性质极其恶劣。</p><p>现已查明，陈岩板、肖岩块犯罪事实清楚、证据确凿充分，浙江省杭州市公安局、云南省昆明市公安局决定对其进行公开悬赏通缉。公安机关敦促犯罪嫌疑人陈岩板、肖岩块认清形势，悬崖勒马，主动投案自首，争取宽大处理。同时，希望社会各界和广大人民群众积极举报，协助抓捕犯罪嫌疑人。对提供有效线索和协助抓捕的有功人员，公安机关将给予10万至50万元人民币奖励。</p><p><img alt=\"\" src=\"/uploads/ueditor/image/20231012/1697100593868873.jpg\" style=\"box-sizing: border-box; vertical-align: middle; border-style: none; padding: 0px; display: block; margin: 0px auto; max-width: 100%; color: rgb(51, 51, 51); font-family: MicrosoftYaHei; letter-spacing: 0.5px; text-wrap: wrap; background-color: rgb(248, 249, 249); height: auto !important;\"/><img alt=\"\" src=\"/uploads/ueditor/image/20231012/1697100593166124.jpg\" style=\"box-sizing: border-box; vertical-align: middle; border-style: none; padding: 0px; display: block; margin: 0px auto; max-width: 100%; color: rgb(51, 51, 51); font-family: MicrosoftYaHei; letter-spacing: 0.5px; text-wrap: wrap; background-color: rgb(248, 249, 249); height: auto !important;\"/></p><p><br/></p>','网络','/uploads/ueditor/image/20231012/thumb_500_300_1697100593868873.jpg','http://localhost:94/kaidian/26.html','','1','1','10','0','0','1','0','0','0');
INSERT INTO `yzm_article` VALUES ('27','15','1','admin','管理员','中学运动会表演刺杀安倍惹争议 教育局回应：允许学生犯错','','1697100663','1697100883','提速,10倍,华为,发布,全球,首个','据中国网视频消息，10月10日报道，山东枣庄一中学运动会上表演&ldquo;刺杀安倍&rdquo;，引起大量关注。视频显示，一学生在椅子上发表讲话后，另一学生举起类似&ldquo;手枪&rdquo;物品对准讲话者，...','66','<p>据中国网视频消息，10月10日报道，山东枣庄一中学运动会上表演“刺杀安倍”，引起大量关注。</p><p>视频显示，一学生在椅子上发表讲话后，另一学生举起类似“手枪”物品对准讲话者，并上前“补枪”。后发言者做倒地状，旁人上前追堵“开枪者”。另有学生举起写有“两声枪响尸骨寒，污水排海遗后患”的横幅，围观者欢呼鼓掌。</p><p>记者从事发学校所在的枣庄市教育局了解到，工作人员昨天已知晓此事，“他们（学生）的做法也不一定对，我们允许学生犯错误，他毕竟是个学生。”对方称，枣庄市教育局已经督促该校调查此事并写情况说明。</p><p><img max-width=\"600\" src=\"/uploads/ueditor/image/20231012/1697100879108770.png\" style=\"border: 0px; margin: 10px auto 0px; padding: 0px; display: block; height: auto; max-width: 100%;\"/>视频截图：中学运动会表演“刺杀安倍”</p><p>11日，媒体评论员朱昌俊发表《中学生表演“刺杀安倍”，仇恨教育让人担忧》表示，一种愈发令人担忧的思潮是，一些人把仇恨情绪和爱国情感捆绑在一起，似乎越仇恨某个对象就等于越爱国。这种狭隘和错位的捆绑，增加了仇恨思维的误导性和迷惑性，也让理性思辩的空间变得更逼仄。历史反复证明，这种建立在仇恨基础上的“爱”，也可能是危险的。当这种仇恨思维侵入校园，则更需要社会增强警惕。</p><p>认识到仇恨教育的危害性，并帮助孩子远离，也应该是一种责任。</p><p>到底是希望孩子在一个推崇包容、多元、友爱的环境中长大，还是希望他们从小就被嵌入某种莫名其妙地仇恨思维？是希望他们变成一个被某种单一情绪所左右的狭隘的人，还是成为明辨是非的，拥有良知的文明人？</p><p>红网评论《<a href=\"https://www.sohu.com/a/727671322_120094090?spm=smpc.content.content.2.1697100847880zd5vHop\" target=\"_blank\" data-spm-data=\"2\">警惕种什么样的种子开什么样的花</a>》指出，学校自然要对此事负主要责任，但一部分极端的网络舆论也难逃其咎。需要承认的是，近些年来，一部分人打着“爱国主义”的旗号，高喊“记住历史也要记住仇恨”“要排外不要开放”，不仅与倡导“人类命运共同体”的国家话语相背而行，也在许多三观尚未形成的未成年人心中种下了仇恨与排外的种子。</p><p>“种什么样的种子开什么样的花”，若任由此类网络舆论发展下去，这些仇恨的种子会开出什么样的花呢？还能再开出“心心相融，爱达未来”的花吗？我想我们都不希望看到这样的结果</p><p><br/></p>','网络','/uploads/ueditor/image/20231012/thumb_500_300_1697100879108770.png','http://localhost:94/kaidian/27.html','','1','1','10','0','0','1','0','0','0');
INSERT INTO `yzm_article` VALUES ('28','15','1','admin','管理员','有哪些一看就莫名感到伤感的图片？','','1697101860','1697101978','哪些,一看,莫名,感到,伤感,图片','','114','<p><img src=\"/uploads/ueditor/image/20231012/1697101936817335.png\" title=\"有哪些一看就莫名感到伤感的图片？\" alt=\"有哪些一看就莫名感到伤感的图片？\"/></p><p><img src=\"/uploads/ueditor/image/20231012/1697101974524809.png\" title=\"有哪些一看就莫名感到伤感的图片？\" alt=\"有哪些一看就莫名感到伤感的图片？\"/></p>','网络','/uploads/ueditor/image/20231012/thumb_500_300_1697101936817335.png','http://localhost:94/kaidian/28.html','','1','1','10','0','0','1','0','1','0');
INSERT INTO `yzm_article` VALUES ('29','15','1','admin','管理员','北大“韦神”32岁无车无房被嘲讽？工资曝光，网友们不讲话了','','1697166149','1697271119','北大,韦神,32岁,无车,无房,嘲讽','他，是那位名声在北大广为流传的教授，人们亲切地称之为&quot;韦神&quot;。这个绰号并非空穴来风，而是源自他在数学领域展现出的非凡才华，被誉为数学天才。然而，这位数学天才在其他领...','259','<p data-first-child=\"\" data-pid=\"nnFiGA0w\" style=\"margin-top: 0px; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">他，是那位名声在北大广为流传的教授，人们亲切地称之为&quot;韦神&quot;。这个绰号并非空穴来风，而是源自他在数学领域展现出的非凡才华，被誉为数学天才。</p><p data-pid=\"73rgksjw\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">然而，这位数学天才在其他领域却显得有些黯然失色，尤其是他的外表形象，曝光于视频中的样子似乎毫无修饰，甚至可以用&quot;邋遢&quot;来形容。当记者们追问问题时，他显得有些紧张，简单的问题在他这里变得错综复杂，回答起来毫无条理，令人不禁瞠目结舌。这究竟是传闻中的&quot;北大教授&quot;吗？</p><p class=\"ztext-empty-paragraph\" style=\"margin-top: -0.8em; margin-bottom: -0.8em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><br/></p><p><br/></p><figure data-size=\"normal\" style=\"margin: 1.4em 0px; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><img src=\"/uploads/202310/13/20231013110258646.webp\" data-caption=\"\" data-size=\"normal\" data-rawwidth=\"1200\" data-rawheight=\"812\" class=\"origin_image zh-lightbox-thumb lazy\" width=\"1200\" data-original=\"https://pic4.zhimg.com/v2-11b7622534a9a1ca689df31247b60797_r.jpg\" data-actualsrc=\"/uploads/202310/13/20231013110259101.jpg\" data-original-token=\"v2-115c67cd9373cf927487641d863e71b7\" height=\"812\" data-lazy-status=\"ok\" style=\"display: block; margin: 0px auto; max-width: 100%; height: auto; cursor: zoom-in; background-color: transparent; animation: 0.5s ease-in 0s 1 normal none running animation-1yvu044; width: 690px;\"/></figure><p><br/></p><p class=\"ztext-empty-paragraph\" style=\"margin-top: -0.8em; margin-bottom: -0.8em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><br/></p><p data-pid=\"WLZsCk9a\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">然而，当你真正接触他，深入了解他之后，你将会得到一个全然不同的认识。曾经有网友这样形容他：一开始看上去，他仿佛是个头脑简单的人，然而当你深入了解后，才发现自己实际上是误判了他。</p><p data-pid=\"y9E1jQBz\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">如今，他已经踏入了32岁的门槛，每一天都沉浸在各种数学难题中。尽管他仍然保持着单身的状态，曾经有人戏言他没有车没有房。然而，当他的工资被曝光后，所有的嘲笑声都在一瞬间消失。</p><p data-pid=\"UORKEBwm\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">在山东的一处书香之家，诞生了一个名为韦东奕的数学天才。他的父母皆为大学教授，而他的父亲更是数学领域的重要人物，拥有数学系统的博士学位。在这种知识渗透的环境中，优秀几乎成了韦东奕的天性。</p><p data-pid=\"EMNN-p46\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">在童年时，他便深受父亲的影响，对数学产生了浓烈的兴趣。而在其他孩子仍在尽情玩耍的时候，他早已沉醉于无边的数学海洋，乐此不疲。</p><p data-pid=\"wSLgcaIP\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">那些年，最令他陶醉的，莫过于他父亲书房的一排排数学书籍。他总会找寻每一个闲暇的瞬间，轻轻拿起一本数学书，全神贯注地阅读其中的智慧。</p><p data-pid=\"V1mhDMfK\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">在那些厚重的书页间，他最钟爱的便是《华罗庚数学学校》这本杰作。当他第一次翻开那本书时，仿佛踏入了一个神秘的世界。对他而言，那本书就如同一枚珍贵的宝藏，他愈发沉迷，废寝忘食地深入其中，全心全意地探寻着其中的奥妙。每一个公式，每一段理论，都如美丽的音符，奏响了他内心最深处的激情。</p><p class=\"ztext-empty-paragraph\" style=\"margin-top: -0.8em; margin-bottom: -0.8em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><br/></p><p><br/></p><figure data-size=\"normal\" style=\"margin: 1.4em 0px; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><img src=\"/uploads/202310/13/20231013110259455.webp\" data-caption=\"\" data-size=\"normal\" data-rawwidth=\"1200\" data-rawheight=\"694\" class=\"origin_image zh-lightbox-thumb lazy\" width=\"1200\" data-original=\"https://pic2.zhimg.com/v2-bc8c90bbc71d3a426619c8fc69798495_r.jpg\" data-actualsrc=\"/uploads/202310/13/20231013110259395.jpg\" data-original-token=\"v2-5afba73738780fcebcca205db603e2dd\" height=\"694\" data-lazy-status=\"ok\" style=\"display: block; margin: 0px auto; max-width: 100%; height: auto; cursor: zoom-in; background-color: transparent; animation: 0.5s ease-in 0s 1 normal none running animation-1yvu044; width: 690px;\"/></figure><p><br/></p><p class=\"ztext-empty-paragraph\" style=\"margin-top: -0.8em; margin-bottom: -0.8em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><br/></p><p data-pid=\"aEi-zXZb\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">书中的数学题成了他的挑战目标，他不断地解读、推演。努力终不辜负他的付出，他竟然在一道题上获得了成功，可要知道，当时的他仅是个小小的一年级学生。</p><p data-pid=\"YH-JSHHS\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">他的同班同学们还在学习最基础的加减法，而他已在数学的天空中自由翱翔，尽情享受着战胜难题的快感。他的数学知识已超越了课堂，老师的授课已跟不上他的步伐。</p><p data-pid=\"29cYs2sY\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">放学后，他最开心的时光是与父亲一起，探讨高年级数学课程。父亲惊讶于儿子对数学的洞察力和热爱，指导他的过程得心应手。</p><p data-pid=\"YsLTscN8\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">在与父亲的不断交流中，他的数学理解力飞速提升。初中时，他的数学才华更是得到极大发挥，每次上课，他总会轻轻地闭上眼睛，仿佛在梦中徜徉。</p><p data-pid=\"zF2kn-49\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">然而，你能想象吗？老师最喜欢的，居然是韦东奕那个闭上眼睛的状态。纵然他从不听课，甚至考试时也不抬头，但成绩却总是满分。老师最害怕的，正是韦东奕突然睁开双眼的瞬间，因为那意味着数学题可能出错了，这一切是如此神奇。他在数学的领域里，仿佛有一双隐形的翅膀，将他带向了无限的高度。</p><p data-pid=\"14J0G4wp\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">时光穿越到2001年，一个年轻的数学天才，名叫韦东奕，闪耀在山东师范大学附属中学的校园。以他出色的数学成绩，他成功地免试被录取，成为这所学校的一名学子。然而，这只是他辉煌人生的序曲。</p><p data-pid=\"i_9hSRoD\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">刚踏入学校，他便被选入了数学奥林匹克国家集训队，开始了专业的数学训练。在这个精英汇聚的队伍里，他迅速成为了&quot;学神&quot;。教练们不吝溢美之词，因为他在数学领域展现出非凡的天赋。</p><p class=\"ztext-empty-paragraph\" style=\"margin-top: -0.8em; margin-bottom: -0.8em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><br/></p><p><br/></p><figure data-size=\"normal\" style=\"margin: 1.4em 0px; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><img src=\"/uploads/202310/13/20231013110259571.webp\" data-caption=\"\" data-size=\"normal\" data-rawwidth=\"1200\" data-rawheight=\"766\" class=\"origin_image zh-lightbox-thumb lazy\" width=\"1200\" data-original=\"https://pic4.zhimg.com/v2-9bc0841b091a50be6f500010657d29a3_r.jpg\" data-actualsrc=\"/uploads/202310/13/20231013110259992.jpg\" data-original-token=\"v2-ac3904804be281e50c3ee1bcb3c1860f\" height=\"766\" data-lazy-status=\"ok\" style=\"display: block; margin: 0px auto; max-width: 100%; height: auto; cursor: zoom-in; background-color: transparent; animation: 0.5s ease-in 0s 1 normal none running animation-1yvu044; width: 690px;\"/></figure><p><br/></p><p class=\"ztext-empty-paragraph\" style=\"margin-top: -0.8em; margin-bottom: -0.8em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><br/></p><p data-pid=\"VifNn1np\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">国家集训队的教练回忆道，韦东奕创造了两个传奇，至今仍无人能望其项背。一次，教练们拿出一道极难的奥林匹克题目，让学生们和他们一同解答。这道题的难度可想而知，甚至连教练都花了三个多小时才找到解法。然而，韦东奕却采用了另辟蹊径的方法，在短短两个小时内轻松解答出来。</p><p data-pid=\"2t8fggdW\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">他的名字开始传遍整个国家集训队，他成了&quot;风云人物&quot;。人们或许会认为这只是偶然的幸运，然而接下来的表现却彻底证明了他的非凡才华。</p><p data-pid=\"G3mDVL5V\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">国家集训队的考试，难度极高。一套试卷有24道题，分八次考完，每次三道题，限时四小时。然而，这些题目太过艰深，几乎没有人能在规定时间内完成，更不用说全部正确了。</p><p data-pid=\"wEZBcUeO\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">然而，韦东奕的到来改变了这一局面。每次考试，他只需要一两个小时，便轻松解决了三道题。更不可思议的是，他竟然在整套试卷中，答对了23道半。一时间，全队为之震惊。有人或许会怀疑他是不是在最后一题出了错，然而，经过多次核实，却发现错的并不是他，而是试题本身。</p><p data-pid=\"1X1qDmiG\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">他成为了那个充满传奇的存在，凭借出色的数学天赋和无与伦比的解题能力，他获得了大家的佩服和尊敬。</p><p data-pid=\"1q16J3Sy\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">接下来，他踏上了更高的舞台，参加数学奥林匹克大赛，成绩一次比一次耀眼。两次参赛，两次金牌轻松收入囊中，这让人们不禁惊叹不已。</p><p data-pid=\"NEdX7IRs\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">然而，这只是韦东奕非凡数学之路的开端，他将迎来更加辉煌的篇章。</p><p class=\"ztext-empty-paragraph\" style=\"margin-top: -0.8em; margin-bottom: -0.8em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><br/></p><p><br/></p><figure data-size=\"normal\" style=\"margin: 1.4em 0px; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><img src=\"/uploads/202310/13/20231013110259627.webp\" data-caption=\"\" data-size=\"normal\" data-rawwidth=\"1200\" data-rawheight=\"930\" class=\"origin_image zh-lightbox-thumb lazy\" width=\"1200\" data-original=\"https://pic2.zhimg.com/v2-ac9680a245748d09197883b4f4fa7ed5_r.jpg\" data-actualsrc=\"/uploads/202310/13/20231013110300761.jpg\" data-original-token=\"v2-1ec4b8b8aff874b874d85c2eb26a4375\" height=\"930\" data-lazy-status=\"ok\" style=\"display: block; margin: 0px auto; max-width: 100%; height: auto; cursor: zoom-in; background-color: transparent; animation: 0.5s ease-in 0s 1 normal none running animation-1yvu044; width: 690px;\"/></figure><p><br/></p><p class=\"ztext-empty-paragraph\" style=\"margin-top: -0.8em; margin-bottom: -0.8em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><br/></p><p data-pid=\"tCwJGOtW\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">高二时，韦东奕那颗耀眼的数学之星继续升腾，他在与数学界传奇人物陶哲轩的比赛中展现了无可比拟的实力。数学奥赛组委会设计了一道难度极高的题目，陶哲轩费尽心思，花费了七个小时才最终找到解答。</p><p data-pid=\"xJ2K4WFk\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">然而，韦东奕在这场比拼中，仅仅用了两个小时，就轻松地揭开了谜底。这场比赛让人一睹了他与众不同的数学才华，他的名字彻底成为了数学领域的璀璨明珠。</p><p data-pid=\"sv5MBru_\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">毕业后，他凭借卓越的表现，毫不意外地被保送至北京大学，进入数学科学院深造。在这个顶尖的学术殿堂中，他依旧是耀眼的存在。</p><p data-pid=\"aWMEpXkN\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">北大的校园，成为了他的新舞台。他屡次参加丘成桐大学生数学竞赛，五次夺得金牌，为自己的数学成就再次添彩。</p><p data-pid=\"pww-YqXx\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">然而，远在大洋彼岸的哈佛大学也向他招手，邀请他加入他们的行列。然而，韦东奕却毅然拒绝，他选择坚守自己的母校，对于那诱人的机会毫不动心。</p><p data-pid=\"1E27wRfB\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">他的强大才华在数学领域创造了崭新的高度。他着手研究流体力学方程问题，为我国的航空航天事业解决了诸多难题，极大地提高了飞行器的稳定性和速度，为我国的科技进步做出了杰出贡献。</p><p data-pid=\"z-u3O6Ol\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">然而，即便是这个数学巨擘，也有自己的困惑。作为北大的教授，他开始为学生们授课。然而，他沉浸在自己的数学世界中，传授的知识对其他人来说似乎难以理解。听课的人满满当当，却意外地发现他们竟然难以领会韦东奕在讲述什么。</p><p class=\"ztext-empty-paragraph\" style=\"margin-top: -0.8em; margin-bottom: -0.8em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><br/></p><p><br/></p><figure data-size=\"normal\" style=\"margin: 1.4em 0px; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><img src=\"/uploads/202310/13/20231013110300735.webp\" data-caption=\"\" data-size=\"normal\" data-rawwidth=\"600\" data-rawheight=\"484\" class=\"origin_image zh-lightbox-thumb lazy\" width=\"600\" data-original=\"https://pic3.zhimg.com/v2-d2b9dac9c85a6a04cd694f485dbe66fe_r.jpg\" data-actualsrc=\"/uploads/202310/13/20231013110300576.jpg\" data-original-token=\"v2-89f379a33ae20d6c61b3d23c4d02bc06\" height=\"484\" data-lazy-status=\"ok\" style=\"display: block; margin: 0px auto; max-width: 100%; height: auto; cursor: zoom-in; background-color: transparent; animation: 0.5s ease-in 0s 1 normal none running animation-1yvu044; width: 690px;\"/></figure><p><br/></p><p class=\"ztext-empty-paragraph\" style=\"margin-top: -0.8em; margin-bottom: -0.8em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><br/></p><p data-pid=\"8HqKWErq\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">原来，他的思维已经深入数学的境界，自以为简单的问题在他口中变得复杂难懂，即便是旁听的老师有时也是一头雾水。</p><p data-pid=\"Jne4LKCx\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">在“韦神”的课堂中，他的教学内容显然针对的是精英中的精英。北大的数学教授曾经对同学们说，如果遇到不懂的数学问题，先问我；如果连我都解答不出来，再找韦教授。如果他也回答不了，那就很明显，问题可能出在题目本身。</p><p data-pid=\"-sDnYSU0\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">然而，这种孤独并没有阻挡他的热情。他沉浸在数学的海洋中，独自钻研，寻求更深层次的理解。对于这个数学天才来说，最大的幸福莫过于此。</p><p data-pid=\"ioR_QCL7\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">然而，这位天才的故事却因一段网络视频而变得更加引人关注，远超过他作为&quot;数学天才&quot;的声誉。</p><p data-pid=\"9iflZ74E\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">在那段视频中，他正在学校的食堂用餐，他的形象完全不像一个教授。他随意地穿着，身体扭曲，不修边幅地进食，仿佛一个不经世事的人。</p><p data-pid=\"SKafQChk\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">这段视频还频繁出现他手中拿着一瓶矿泉水，夹着几个馒头。这似乎就是他的一餐。即使在学校食堂，他也只选择最便宜的食物。</p><p data-pid=\"PMJ-qWJi\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">据他自己所说，每个月生活费只需300元，足够了。想想看，这可是在北京，300元够吃什么呢？然而，他对此满足。他并不需要太多的物质享受，只要能填饱肚子就行。他吃着馒头，喝着矿泉水，食速极快，也不影响他钻研数学。</p><p class=\"ztext-empty-paragraph\" style=\"margin-top: -0.8em; margin-bottom: -0.8em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><br/></p><p><br/></p><figure data-size=\"normal\" style=\"margin: 1.4em 0px; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><img src=\"/uploads/202310/13/20231013110300677.webp\" data-caption=\"\" data-size=\"normal\" data-rawwidth=\"1200\" data-rawheight=\"1172\" class=\"origin_image zh-lightbox-thumb lazy\" width=\"1200\" data-original=\"https://pic2.zhimg.com/v2-ab29d43821c43aafb00a65144db371a9_r.jpg\" data-actualsrc=\"/uploads/202310/13/20231013110300230.jpg\" data-original-token=\"v2-c159de72bb25e95ed9bdfd014b487bec\" height=\"1172\" data-lazy-status=\"ok\" style=\"display: block; margin: 0px auto; max-width: 100%; height: auto; cursor: zoom-in; background-color: transparent; animation: 0.5s ease-in 0s 1 normal none running animation-1yvu044; width: 690px;\"/></figure><p><br/></p><p class=\"ztext-empty-paragraph\" style=\"margin-top: -0.8em; margin-bottom: -0.8em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><br/></p><p data-pid=\"1mReEv6-\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">然后，一些网友开始讽刺韦东奕。他已经32岁了，在北京没有房子，没有车，甚至从未恋爱过。他们认为这一定是因为经济拮据所致，即使是天才也无法免俗。</p><p data-pid=\"5jUQsK_I\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">更有一些人对他进行抨击，声称这样的教授能培养出好的学生吗？他们一致要求韦东奕不要再继续授课，以免误导学生。</p><p data-pid=\"etqE7keS\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">然而，随着人们不断揭示韦东奕所创造的奇迹，嘲讽逐渐停息，人们开始转向他的外貌、衣着和生活方式。</p><p data-pid=\"6hh8oIsA\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">人们普遍认为他生活拮据，否则他也不会仅用300元度日。然而，最近北大公开了一份没有房产的教职工名单，让人们瞥见了韦东奕的工资单。</p><p data-pid=\"4cPnMXnF\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">他的年薪在60万左右，这还不包括他参加比赛所获得的奖金。考虑到他在生活上的节俭，相信在北京买房也不是难事。许多人看到他的工资单后，开始改变看法。这位神秘的数学天才不再只是一个&quot;贫困&quot;的标签，而是一个更为丰富多彩的人物。</p><p data-pid=\"tWtuNgv2\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">然而，对于“韦神”来说，他对网友的评论根本毫不在意。他宁愿将宝贵的时间投入到自己钟爱的数学世界中，因为那里才是他真正快乐的源泉。</p><p data-pid=\"x-zKdhQE\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">他不是一个贪图享受的人，对于金钱，他只是将其看作一个数字而已。他从不过分在乎财富，与马云曾说的那句话相比：“我对钱不感兴趣。” 如果&quot;韦神&quot;说出这句话，人们必定会由衷地认同。他真的是一个不追求物质财富的数学奇才，他将全部精力投入到了他热爱的数学事业中，因为只有这个事业能给他真正的满足。</p><p class=\"ztext-empty-paragraph\" style=\"margin-top: -0.8em; margin-bottom: -0.8em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><br/></p><p><br/></p><figure data-size=\"normal\" style=\"margin: 1.4em 0px; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><img src=\"/uploads/202310/13/20231013110300474.webp\" data-caption=\"\" data-size=\"normal\" data-rawwidth=\"1200\" data-rawheight=\"752\" class=\"origin_image zh-lightbox-thumb lazy\" width=\"1200\" data-original=\"https://pic2.zhimg.com/v2-b6c5342c67d8da4f0081ead6759ee595_r.jpg\" data-actualsrc=\"/uploads/202310/13/20231013110301555.jpg\" data-original-token=\"v2-373c6287f5e033ff2392f8f108d2aaa8\" height=\"752\" data-lazy-status=\"ok\" style=\"display: block; margin: 0px auto; max-width: 100%; height: auto; cursor: zoom-in; background-color: transparent; animation: 0.5s ease-in 0s 1 normal none running animation-1yvu044; width: 690px;\"/></figure><p><br/></p><p class=\"ztext-empty-paragraph\" style=\"margin-top: -0.8em; margin-bottom: -0.8em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><br/></p><p data-pid=\"BEEgPi7v\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">毕竟，我们普通人和他真的不在同一层次上。他的境界如同电影中周星驰所说的：“别人笑我太疯癫，我笑他人看不穿。”</p><p data-pid=\"eyHUCP9H\" style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: medium; text-wrap: wrap; background-color: rgb(255, 255, 255);\">这位神秘的数学天才，将自己的一切都献给了他热爱的事业，这是值得我们敬佩和钦佩的。不知道在屏幕前的朋友们是如何看待他的呢？或许，对于&quot;韦神&quot;来说，最重要的不是别人的看法，而是他在数学的世界里所寻找到的无尽快乐。</p><p><br/></p>','网络','http://localhost:94/uploads/202310/13/20231013110258646.webp','http://localhost:94/kaidian/29.html','','1','1','10','0','0','1','0','2','0');
INSERT INTO `yzm_article` VALUES ('24','15','1','admin','管理员','成都一初中生在校身亡，家长两次被学校藏手机？当地称正调查 ','','1697099436','1697099878','成都,初中生,在校,身亡,家长,两次','近日，有网友发帖称，10月10日下午，四川省教科研究院附属实验中学发生一起学生身亡事件，引发关注。家长与校方对话的视频显示，孩子在校告知家长“不愿意参赛”后出事。12日...','89','<p>近日，有网友发帖称，10月10日下午，四川省教科研究院附属实验中学发生一起学生身亡事件，引发关注。家长与校方对话的视频显示，孩子在校告知家长“不愿意参赛”后出事。12日上午，成都市教育局工作人员告诉南都记者，成都市教育局已关注到相关情况，成都市高新区相关部门正在调查处理此事。</p><p><img src=\"/uploads/ueditor/image/20231012/1697099456308642.jpeg\" style=\"border: 0px; margin: 10px auto 0px; padding: 0px; display: block; height: auto; max-width: 100%;\"/></p><p>孩子家长与学校工作人员对话。</p><p>一段事发后家长与学校工作人员对话的视频显示，该家长称自己于10日16时42分接到孩子的电话，孩子表示身体不舒服，能否不参加学校的一个比赛。17时02分，家长最后一次跟孩子沟通，孩子回复“正在上课，下课再说”。18时，老师忽然打来电话称“孩子找不到了”，家长随后赶到校门口见到老师，依然被告知“孩子没有找到”，家长直接上教学楼后发现“孩子已经躺在那里了”。</p><p>据家长描述，事发后，通过学校的监控视频发现，孩子于17时06分“自缢”身亡，期间没有人寻找过孩子。而在她与学校的交涉过程中，手机两次被藏起来。</p><p>12日上午，南都记者就此事致电成都市教育局，相关工作人员回应称，成都市教育局已关注到相关情况，成都市高新区相关部门正在调查处理此事。</p><p>公开资料显示，四川省教育科学研究院附属实验中学创办于2021年，由四川省教育科学研究院与成都高新区合作办学，位于成都高新区崇和三街999号。</p><p><br/></p>','网络','/uploads/ueditor/image/20231012/thumb_500_300_1697099456308642.jpeg','http://localhost:94/kaidian/24.html','4','1','1','10','0','0','1','0','0','0');
INSERT INTO `yzm_article` VALUES ('25','15','1','admin','管理员','云南一烟草复烤厂厂长被举报婚内出轨并生下私生女，纪委回应','','1697100208','1697100255','云南,烟草,复烤厂,厂长,举报,婚内','10月10日，有网友在社交平台发帖举报称，云南烟叶复烤责任有限公司陆良复烤厂厂长张某某在宣威复烤厂任厂长期间，严重违规违纪，婚内出轨已婚女职工陈某，并生下私生女引发关...','96','<p>10月10日，有网友在社交平台发帖举报称，云南烟叶复烤责任有限公司陆良复烤厂厂长张某某在宣威复烤厂任厂长期间，严重违规违纪，婚内出轨已婚女职工陈某，并生下私生女引发关注。</p><p><img alt=\"\" width=\"600\" src=\"/uploads/ueditor/image/20231012/1697100247111191.jpg\" style=\"box-sizing: border-box; vertical-align: middle; border-style: none; padding: 0px; display: block; margin: 0px auto; max-width: 100%; color: rgb(51, 51, 51); font-family: MicrosoftYaHei; letter-spacing: 0.5px; text-wrap: wrap; background-color: rgb(248, 249, 249); height: auto !important;\"/></p><p>网友举报截图</p><p><br/></p><p>10月11日，海报新闻记者电话采访了发帖举报的网友费先生，据介绍，费先生是陈某的前夫，双方已离婚，费先生与陈某婚内育有两个孩子，离婚时费先生只知道陈某出轨，后经陈某单位同事告知，才知道两人的小女儿并非其亲生，而是陈某与时任云南烟叶复烤责任有限公司宣威复烤厂厂长张某某的女儿。</p><p>“知道这件事后，我就带着孩子去做了DNA亲子鉴定，鉴定结果显示孩子不是我的。”费先生告诉记者，此后他拿到了张某某的毛发，并重新给孩子做了亲子鉴定，鉴定结果显示孩子确系张某某的。</p><p>从费先生提供的小孩与张某某的亲子鉴定报告上，记者看到咨询意见一栏写道：依据DNA分析结果，支持SZ22WZ2686-M-R1号检材所属个体与SZ22WZ2212-C号检材所属个体存在生物学亲子关系，而上述两个检材编号所对应的正是张某某与孩子带毛囊的毛发。</p><p><img alt=\"\" width=\"600\" src=\"/uploads/ueditor/image/20231012/1697100248190174.png\" style=\"box-sizing: border-box; vertical-align: middle; border-style: none; padding: 0px; display: block; margin: 0px auto; max-width: 100%; color: rgb(51, 51, 51); font-family: MicrosoftYaHei; letter-spacing: 0.5px; text-wrap: wrap; background-color: rgb(248, 249, 249); width: 618px; height: auto !important;\"/></p><p>鉴定报告截图</p><p><br/></p><p>针对此事，记者电话采访了云南省烟草专卖局纪委部门，工作人员回复记者称，此前已经接到了相关举报，目前此事正按照内部程序办理，办理的进程也跟当事人进行了反馈。“因为受内部办理的权限影响，目前我们还无法深入地进行核实，同时对于举报人反映的问题，暂时还无法下结论。”</p><p><br/></p><p><br/></p>','网络','/uploads/ueditor/image/20231012/thumb_500_300_1697100247111191.jpg','http://localhost:94/kaidian/25.html','','1','1','10','0','0','1','0','0','0');
INSERT INTO `yzm_article` VALUES ('23','15','1','admin','管理员','婚内出轨包养情妇？郑强回应 ','','1697099133','1697099185','婚内,出轨,包养,情妇,郑强,回应','婚内出轨包养情妇？郑强回应近日，一网友举报太原理工大学党委书记郑强与其前女友有染，引发广泛关注。10月11日，举报人对《中国新闻周刊》称，2015年7月的一天凌晨，郑强给其...','98','<p style=\"margin-top: 15px;\">婚内出轨包养情妇？郑强回应</p><p style=\"margin-top: 15px;\">近日，一网友举报太原理工大学党委书记郑强与其前女友有染，引发广泛关注。</p><p style=\"margin-top: 15px;\">10月11日，举报人对《中国新闻周刊》称，2015年7月的一天凌晨，郑强给其女友打电话被他发现，几个月后双方分手。举报人称，今年3月他和郑强见过一次面，对方劝其慎重，今年向有关校方和纪检部门反映过，至今没有结果。</p><p style=\"margin-top: 15px;\">10月12日，“郑强教授”公众号发布《关于近期网络上郑强同志相关不实新闻的严正声明》称：近期在网络上进行发布和传播虚假信息的账号“华趣”，现已证实其发布人实名认证为吕某，系刑满释放犯罪人员，所发布的全部文字与图片内容均为恶意编造、虚构的不实信息。</p><p style=\"margin-top: 15px;\">声明中称：2023年3月，吕某以“粉丝”名义联系郑强，并讲述个人经历及在监狱服刑的体会感想，希望得到相关帮助。2023年6月，吕某再次联系郑强，希望在其社会工作方面获得郑强支持，要求为其介绍资源。</p><p style=\"margin-top: 15px;\">“吕某的实际诉求是让我本人出面做担保，为其借款1000万元，被直接严词拒绝。于是吕某开始不断编造各种不实信息和证据，陆续通过线上和线下各种方式对我的个人名誉进行诽谤，意图以此达到敲诈、勒索财物的目的，并且对我的人身安全造成了威胁。6月下旬，我已主动向省纪委领导报告和说明该事件的相关情况。”</p><p style=\"margin-top: 15px;\">郑强在声明中强调，吕某发布虚假信息，所述内容均为捏造虚构，对其本人进行敲诈和勒索，严重损害其个人名誉，造成极坏影响和极大伤害。“如我本人存在任何违反纪律和法律的言行，我本人愿意接受组织的审查和处理。”</p><p style=\"margin-top: 15px;\">12日，太原理工大学纪委办公室工作人员回应《中国新闻周刊》，已关注到该舆情，昨晚校方几个部门连夜开会。</p><p style=\"margin-top: 15px;\">12日，山西省纪委监委宣传部工作人员对《中国新闻周刊》表示，正在了解相关情况。</p><p style=\"margin-top: 15px;\">截至发稿，《中国新闻周刊》多次联系郑强本人，电话均未接听。</p><p><br/></p>','网络','','http://localhost:94/kaidian/23.html','4','1','1','10','0','0','1','0','0','0');
INSERT INTO `yzm_article` VALUES ('14','15','1','admin','管理员','媒体：“指鼠为鸭”才过去4个月，“指鼠为牛”接棒','','1697088945','1697089047','媒体,指鼠为鸭,过去,4个月,指鼠为牛,接棒','图源：红星新闻10月10日，有网友爆料称华北理工大学的学生在食堂吃出异物，疑似鼠头。网友发布的群聊天截图显示，10月10日11时22分许，有人在群内发布了几张食物图片，其中一...','112','<div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\"><div class=\"qnt-img\" style=\"max-width: 100%; width: 680px; display: inline-block; vertical-align: top; overflow-wrap: break-word; word-break: break-all; margin-bottom: 0px;\"><div style=\"border-radius: 6px; overflow: hidden; width: 680px;\"><p dir=\"ltr\" style=\"text-align: justify; margin-top: 5px;\"><img src=\"/uploads/ueditor/image/20231012/1697089038599153.png\" title=\"媒体：“指鼠为鸭”才过去4个月，“指鼠为牛”接棒\" alt=\"媒体：“指鼠为鸭”才过去4个月，“指鼠为牛”接棒\"/></p></div></div></div><p dir=\"ltr\" style=\"text-align: justify; margin-top: 5px;\">图源：红星新闻</p><p dir=\"ltr\" style=\"text-align: justify; margin-top: 5px;\">10月10日，有网友爆料称华北理工大学的学生在食堂吃出异物，疑似鼠头。网友发布的群聊天截图显示，10月10日11时22分许，有人在群内发布了几张食物图片，其中一块筷子夹住的深色肉上有几根毛，该人称这份食物是其室友在学校食堂买到的，看上去像是老鼠头，但食堂的店家说是牛肉。有群友则质疑肉上的毛是胡须。</p><p dir=\"ltr\" style=\"text-align: justify; margin-top: 5px;\">当晚，华北理工大学后勤服务中心发出公告，称宁波市江徽美食餐饮有限公司于2023年7月27日参与华北理工大学学生食堂委托经营项目招标并中标。2023年10月10日，该公司违反食品卫生相关规定，在经营过程中发生食品卫生问题。经研究决定，解除与其之间的委托经营合同，并将依法依规追究其相关责任。最新情况是，当地市监局也已经介入调查此事。</p><p dir=\"ltr\" style=\"text-align: justify; margin-top: 5px;\">江西高校食堂“指鼠为鸭”事件才过去4个月，又是高校食堂出现疑似老鼠头，区别在于，这次是“指鼠为牛”。</p><p dir=\"ltr\" style=\"text-align: justify; margin-top: 5px;\">对于此事，学校的处理还相对及时，“火速”与相关公司解除合同。但仍有两个关键信息点没有说清楚。</p><p dir=\"ltr\" style=\"text-align: justify; margin-top: 5px;\">一个是学校只明确“存在食品卫生问题”，却没有说清楚这个异物到底是不是鼠头，如果真是老鼠头，那身体又在哪里？另一个是，相关学生的健康和安全情况如何，还有没有其他学生在这个食堂吃出问题，学校在这方面有何具体作为？这些才是舆论的最大疑惑和关切，学校应当第一时间说清楚，而不是模棱两可，或遮遮掩掩。</p><p dir=\"ltr\" style=\"text-align: justify; margin-top: 5px;\">这家涉事公司，通过正常招标进入大学食堂，并且只干了两个多月就出现食品安全问题。我们想知道，到底是哪一个环节失守了，才会让疑似老鼠头的东西出现在学生的饭菜里。项目招标，厨房环境，食材采购、仓储、清洗，后勤监管等一系列情况，都需要好好倒查一番。</p><p dir=\"ltr\" style=\"text-align: justify; margin-top: 5px;\">“指鼠为牛”出现在舆论场，如果在事发后的处理阶段，再不将整个环节过程“公开透明化”，将真相和问题公之于众，接受社会和民众的监督和检验，并采取有力举措真正刮骨疗毒，那“信任危机”才刚刚开始。</p><p dir=\"ltr\" style=\"text-align: justify; margin-top: 5px;\">如果没有记错的话，这已经是自“指鼠为鸭”以来，今年媒体公开报道的第三起“鼠头”事件。卡洛琳里维特有一句名言：“如果你看到一只蟑螂出没，代表有成千上万只蟑螂在埋伏。”此类事情“没完没了”，恰恰说明问题其实一直都存在，所暴露和指向的，既有责任主体方对食品安全的冷漠，也有食品安全管理制度的漏洞，更有社会机制体系具体设计上，在更有力关切和保障群众利益层面的相对忽视和缺失。就拿最后一点来说，社会食品安全的标准是不是还是低了些，违规成本也小了些，惩戒机制和力度也不够有震慑性。</p><p dir=\"ltr\" style=\"text-align: justify; margin-top: 5px;\">如果机制性、体系化存在的相关问题不解决，不朝着更有利于群众利益的方向调整巩固和提升，那么，面对如今的漠视、漏洞与缺失，恐怕不是几次“鼠头”事件的曝光，解除几个合同，追究几个餐饮公司和主管部门的责任，就能弥补的。</p><p dir=\"ltr\"><br/></p>','网络','/uploads/ueditor/image/20231012/1697089038599153.png','http://localhost:94/kaidian/14.html','1','1','1','1','0','0','1','0','0','0');
INSERT INTO `yzm_article` VALUES ('8','16','1','admin','管理员',' 抖音10天涨粉百万的于文亮是谁','','1697073517','1697073592','抖音,于文亮,涨粉','        10月10日，于文亮10天涨粉百万的话题登上抖音热搜榜第一。于文亮是谁，我们或许还不知道，但打开他的抖音主页会发现他仅仅是一个普通人。在这个充斥着网络名人和KOL的...','61','<p data-track=\"2\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10月10日，于文亮10天涨粉百万的话题登上抖音热搜榜第一。于文亮是谁，我们或许还不知道，但打开他的抖音主页会发现他仅仅是一个普通人。在这个充斥着网络名人和KOL的时代，我们似乎习惯了追随那些拥有千万粉丝的大V，他们的每一次动态、每一条微博都能引起一片哗然。但就在这样的背景下，于文亮如一颗耀眼的星星，短短10天时间，粉丝狂涨一百万，他的名字成为了众多网友热议的焦点。</p><p data-track=\"3\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;但他并不是那些拥有千万粉丝的大V，而只是一个真实记录生活的普通人。打开他的投稿，你会看到最真实的日常：他遇到兴奋的事情会大声呐喊，也从来不在意别人的眼光；他分享着自己生活中每一个平凡又普通的瞬间，却成功触动到了我们的内心，引起了无数人的共鸣。</p><p data-track=\"4\">这些平凡的日常，对于大多数人来说，可能只是生命中的一小部分，但对于于文亮来说，这就是他的全部，这就是他想要分享给大家的生活。他的每一篇文章、每一段视频，都充满了真实情感，让人感受到他对生活的热爱和对每一个细节的珍惜。在这个看似光鲜亮丽的网络世界里，很多人都认为只有帅哥、美女、有钱人才能在网络上获得关注。但于文亮用他的行动告诉我们，每个人都是自己生活的主角，每个人都有自己的故事，都值得被关注和分享。他的存在，仿佛是一道清新的风，吹散了网络上的浮躁和虚假。他告诉我们，不需要华丽的外表，不需要奢侈的生活，只要真实地做自己，就能够得到他人的认同和关注。</p><p data-track=\"5\">于文亮的故事，不仅仅是他一个人的故事，更是千千万万普通人的故事。他鼓励我们，不要因为别人的一句批评或嘲笑而怀疑自己，要勇敢地做自己，勇敢地分享自己的生活。</p><p data-track=\"6\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在当今社会，攀比与嫉妒似乎已经成为了一种普遍的社会风气。大家都在相互比较着谁赚的钱多，谁的权力大，谁的名气大……如果比别人过得好，不免洋洋自得；过得差，又会感到羞愧难安，很难做到“一箪食，一瓢饮，在陋巷，人不堪其忧，回也不改其乐”。</p><p data-track=\"7\">无论是在学校，社会上甚至是网络上，普通人都是容易被忽略的群体。但历史不是英雄的个人史，推动历史进步的永远是一个个普通人。于文亮的视频就是黑夜里的灯塔，也让我们明白，每一个人都有资格记录自己平凡的生活，可以呐喊发疯，也可以不在乎世俗的眼光，顺着自己的心意潇洒地活着 。</p><p data-track=\"8\">所以，不要羡慕于文亮，不要嫉妒他。因为在这个世界上，每个人都是独一无二的，我们每个人都有自己的故事，都有自己的经历，都有自己的情感。只要我们勇敢地做自己，勇敢地分享自己的生活，我们就是千千万万个于文亮，是自己生活的主角。</p><p><br/></p>','网络','/uploads/202310/12/231012091914893.png','http://localhost:94/yunying/8.html','','1','1','10','0','0','1','0','1','0');
INSERT INTO `yzm_article` VALUES ('9','16','1','admin','管理员','北京政法职业学院3霸凌女生疑亮相学校宣传片，校方：将核实了解情况','','1697087159','1697087267','北京,政法,职业,学院,霸凌女,生疑','6月10日，北京政法职业学院“男生制止校园霸凌遭三名女同学辱骂围堵”事件在网上引发关注。在事件平息后，10月10日，有网友浏览北京政法职业学院迎新视频时，发现疑似三名涉事...','66','<p>6月10日，北京政法职业学院“男生制止校园霸凌遭三名女同学辱骂围堵”事件在网上引发关注。在事件平息后，10月10日，有网友浏览北京政法职业学院迎新视频时，发现疑似三名涉事女生摆造型迎新的定格画面。有网友表示震惊：“这也能拿来招生？”</p><p style=\"text-align:center\"><img src=\"/uploads/ueditor/image/20231012/1697087259352413.png\" title=\"北京政法职业学院3霸凌女生疑亮相学校宣传片，校方：将核实了解情况\" alt=\"北京政法职业学院3霸凌女生疑亮相学校宣传片，校方：将核实了解情况\"/></p><p>上游新闻（报料邮箱baoliaosy@163.com）记者登录短视频平台北京政法职业学院主页看到，学校在9月7日发布了迎新视频，在视频1分05秒一共有5名女同学站在“您好，应法小伙伴”“开学啦”的拱门下向镜头招手。其中右边3位女同学的相貌被网友截图用来与之前霸凌事件视频截图做对比，并在网上成为热议话题。10月11日，上游新闻记者联系北京政法职业学院党政办公室，求证网络爆料的真实性，以便跟进事件后续动态。对方表示将核实了解一下情况，并根据调查结果进行处理或回应，“如果是谣言的话，我们就做一个辟谣，如果是有具体什么情况，可以进行一个详细的沟通。”</p><p>据媒体此前报道，6月10日，一段题为“男生制止校园霸凌遭同学辱骂”的视频引发各方广泛关注。视频发布者李某曾表示，他因看到两名女生对另一名女生进行言语霸凌，便出口制止，之后有三名女生指着他进行辱骂，随后产生争执。李某曾在评论区称：“这帮人平时就喜欢针对一个女孩子”“我不能眼睁睁再看着她们伤害她。”</p><p>6月11日，北京政法职业学院发布情况通报称，针对网络上出现的有关该校“男生制止校园霸凌遭同学辱骂围堵”的网帖，学校高度重视，第一时间成立工作专班，迅速核查事件经过。经学校初步调查，6月9日上午第四节课后，同班同学许某、李某跟张某等人因值日安排事宜，在教室内发生言语冲突。随后，任课教师、班长当即在现场进行劝解，辅导员第一时间到教室开展调解，双方均表示接受调解，并自行离去。6月10日晚，涉事学生李某将现场录制的视频片段发至网上，张某等人报警。</p><p>7月4日，涉事男生李某在社交平台上发布了“学生退学申请表”图片，同时配文“江湖路远，各自珍重”，疑似从北京政法职业学院办理退学。7月9日晚，北京市公安局大兴分局和北京政法职业学院就相关事件进行了通报：双方在调解下已达成和解。</p><p><br/></p>','网络','/uploads/202310/12/231012010709218.png','http://localhost:94/yunying/9.html','','1','1','10','0','0','1','0','0','0');
INSERT INTO `yzm_article` VALUES ('11','16','1','admin','管理员','程序员工作只能做到 35 岁吗？之后的路是怎么走的呢?','','1697088218','1697088371','程序员,工作,只能,做到,岁吗,后的路','我二十二的时候，他们说程序员只能干到25 。我二十五的时候，他们说程序员只能干到27 。我三十的时候，他们说程序员只能干到 35 。我现在三十七了。我觉得再干三十年毫无...','138','<div><div><p data-first-child=\"\" data-pid=\"dIsboHmN\">我二十二的时候，他们说程序员只能干到25 。</p><p data-pid=\"Clwa1nGI\">我二十五的时候，他们说程序员只能干到27 。</p><p data-pid=\"5h2EctsR\">我三十的时候，他们说程序员只能干到 35 。</p><p data-pid=\"Y0-CncoF\">我现在三十七了。我觉得再干三十年毫无压力。</p><p data-pid=\"7i_fqH8h\">据说图灵奖得主，SQL语言发明人 Jim Gray 七十六岁还在写程序，上帝无法用时间打败他，最终让他消失在茫茫大海。</p><a href=\"https://link.zhihu.com/?target=http%3A//en.wikipedia.org/wiki/Jim_Gray_%28computer_scientist%29\" target=\"_blank\" rel=\"nofollow noreferrer\" data-za-detail-view-id=\"1043\">Jim Gray (computer scientist)</a><br/><p data-pid=\"jnZoy4uJ\">如果你仍有斗志，上天就只能让你失踪于海难，让你出车祸，让你死于滑翔伞事故，让你得阿兹海默氏症，或者诸如此类的方式，才能无耻的战胜你。</p></div><br/><br/></div><p><br/></p>','网络','/uploads/202310/12/231012012121766.png','http://localhost:94/yunying/11.html','1','1','1','1','0','0','1','0','0','0');
INSERT INTO `yzm_article` VALUES ('12','16','1','admin','管理员','2023年应届生平均招聘月薪10342元','','1697088442','1697088530','2023年,应届,平均,招聘,月薪,10342元','据最新报告显示，2023年应届生平均招聘月薪10342元，较2021届上涨1050元，创下近年来新高。北京应届生招聘月薪13283元，全国居首。专家表示，这反映了国内经济的稳步复苏和高...','122','<p>&nbsp;&nbsp;&nbsp;&nbsp;据最新报告显示，2023年应届生平均招聘月薪10342元，较2021届上涨1050元，创下近年来新高。北京应届生招聘月薪13283元，全国居首。专家表示，这反映了国内经济的稳步复苏和高校毕业生的优质供给。</p><p>&nbsp;&nbsp;&nbsp;&nbsp;根据《全国高校毕业生就业趋势与展望2023》，相比2022届，2023届高校毕业生招聘薪资和期望薪资略有下降，但较2021届有明显提升。具体来看，2021届学生岗位平均招聘月薪9292元，2023届为10342元，三年来上涨1050元；2021届高校毕业生期望月薪7417元，2023届为8033元，三年来上涨616元。整体来看，应届生期望薪资均低于招聘薪资，表明应届生求职时对薪资的期望非常理性。</p><p>&nbsp;&nbsp;&nbsp;&nbsp;从地区分布看，北京以13283元的平均招聘月薪位居第一；深圳、上海分别位居第二、第三；杭州、合肥、南京、西安、苏州位居第四至第八，均超过全国平均水平（10342元）。从学历分布看，博士应届生期望薪资遥遥领先。</p><p>&nbsp;&nbsp;&nbsp;&nbsp;从行业分布看，互联网仍然是持续释放就业机会最多的行业，但近三年出现明显下滑态势；医疗健康、机械制造、电子通信、能源化工等近年国家重点发展的行业对应届生需求占比增长明显。从细分行业看，人工智能平均招聘月薪位居第一；区块链、养老服务、航空航天等新兴领域也表现出较高的薪资水平和增长幅度。</p><p><br/></p>','网络','/uploads/202310/12/231012012820879.jpeg','http://localhost:94/yunying/12.html','','1','1','10','0','0','1','0','0','0');
INSERT INTO `yzm_article` VALUES ('13','16','1','admin','管理员','租客怀孕房东要求退租','','1697088702','1697088858','租客,怀孕,房东,要求,退租','俗话说，百里不同风，千里不同俗作为一个多民族国家，中国每个地区都有自己的习俗。外人不能理解这种行为，但也应该尊重它。也许很多人不太明白，认为现在是什么时代？这样的...','145','<div class=\"dpu8C _2kCxD \" style=\"max-width: 100%; overflow-x: visible; font-size: 18px; line-height: 30px; color: rgb(34, 34, 34); font-family: arial; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><p>俗话说，百里不同风，千里不同俗</p></div><div><p>作为一个多民族国家，中国每个地区都有自己的习俗。外人不能理解这种行为，但也应该尊重它。也许很多人不太明白，认为现在是什么时代？</p></div><div><div><img src=\"https://pics5.baidu.com/feed/3bf33a87e950352a6cfebc944f2a16feb3118b01.jpeg@f_auto?token=22ab57823d410736102d9ab3e2377e2f\" width=\"640\" class=\"_1g4Ex _1i_Oe\" style=\"border: 1px solid rgba(0, 0, 0, 0.05); width: 700px; border-radius: 13px;\"/></div></div><div><p>这样的说法显然是有偏见的。有些习俗流传了几千年，任何时候都不会改变，也没有人会试图扭转它。人与人相处，无论双方关系如何，都要注意你的爱和我的愿望。如果两者不能达成共识，只要给出合理的陈述，承担相应的责任，就没有什么好争论的。</p></div><div><p>广东有个女人租房，遇到了一个比较传统的房东。得知她怀孕五个月后，房东让她退房，宁愿支付违约金也不愿让她搬走。这名妇女说，她已经租了房子一段时间了。我们签了一年的合同，住了七个多月。当房东听说我怀孕时，他给我丈夫发了一条信息。</p></div><div><p>这可能意味着房子没有租给我们，让我们尽快找到房子搬走，我丈夫不明白，问房东为什么？我们住进来后，从来没有拖欠过房租，和房东的关系还可以，突然让我们搬走，最后，房东说出了他的担忧。</p></div><div><div><img src=\"https://pics6.baidu.com/feed/c8177f3e6709c93d5d63bc87825415d0d00054a6.jpeg@f_auto?token=590d0ed11ceb019882c72a242dfb845e\" width=\"640\" class=\"_1g4Ex _1i_Oe\" style=\"border: 1px solid rgba(0, 0, 0, 0.05); width: 700px; border-radius: 13px;\"/></div></div><div><p>他说他们这边有句话，借死不借生，我的情况也不错，现在搬家也方便了，他就给我们提出来了。由于合同尚未到期，他支付了一个月的租金。既然他这么说了，我们只能找另一个地方住。虽然我们不明白，但我们只能按照他说的去做。</p></div><div><p>女人把自己的个人经历发到网上，网友们纷纷议论。有人理解，也有人认为房东太迷信了。不懂的人也有情有理，觉得房东不让你住，那么其他房东也不愿意，怀孕的女人不能在外面生孩子吗？</p></div><div><p>事实上，类似于这种情况，不应该都是这样，广东这个地方，很多房子不一定由房东自己管理，如果第二个房东，可能不是这样。</p></div><div><p>此外，上述房东也可以，表明他的困难，宁愿支付违约金，这也是对租户的补偿，他承担了相应的损失。很多人对借死不借生这句话感到困惑，不知道是什么意思。</p></div><div><div><img src=\"https://pics5.baidu.com/feed/503d269759ee3d6d58d4acb15f7f802e4f4ade2b.jpeg@f_auto?token=838d583e824c383f7dcf7137bc7fde74\" width=\"640\" class=\"_1g4Ex _1i_Oe\" style=\"border: 1px solid rgba(0, 0, 0, 0.05); width: 700px; border-radius: 13px;\"/></div></div><div><p>借死不借生是一句俗语，大概就是宁愿把房子借给别人办葬事，也不愿把房子借给生孩子的家庭，借给别人生孩子是不吉利的。从房东的做法来看，他应该非常关心，否则他宁愿支付违约金，也不愿让租户搬走。</p></div><div><p>对于房东来说，他们也喜欢说话比较好的租客，不拖房租，生活圈不乱，管理起来也不难。如果不是真的很难说，谁愿意赶走名声财神？在这种情况下，没有办法租房子。房东还是比较好的。有些房东完全是双重标准。他们什么都不在乎违约。如果租户违约，他们将不退还押金。</p></div><div><p>主动支付违约金的房东也应该是好人，但由于家乡的习俗，他不得不成为这个恶人。</p></div><p><br/></p>','网络','/uploads/202310/12/231012013301221.png','http://localhost:94/yunying/13.html','1','1','1','1','0','0','1','0','0','0');
INSERT INTO `yzm_article` VALUES ('16','15','1','admin','管理员','最新公报发布：我国去年出生人口956万，二孩占比近4成','','1697095054','1697095104','最新,公报,发布,我国,去年,出生','国家卫生健康委今天（10月12日）发布了2022年我国卫生健康事业发展统计公报（以下简称《公报》）。2022年总诊疗人次84.2亿 与上年基本持平《公报》显示，2022年全国医疗卫生...','138','<p style=\"margin-top: 5px;\">国家卫生健康委今天（10月12日）发布了2022年我国卫生健康事业发展统计公报（以下简称《公报》）。</p><p style=\"margin-top: 5px;\">2022年总诊疗人次84.2亿 与上年基本持平</p><p style=\"margin-top: 5px;\">《公报》显示，2022年全国医疗卫生机构总诊疗人次84.2亿，与上年基本持平。居民到医疗卫生机构就诊平均每人每年6次。《公报》显示，我国政府投入不断增加，医疗卫生资源提质扩容，卫生服务体系不断健全，健康中国建设稳步推进。</p><p style=\"margin-top: 5px;\">《公报》显示，我国医疗卫生资源总量继续增加。到2022年末，全国医疗卫生机构达到1032918个，比上年增加1983个。全国共设置13个类别的国家医学中心和儿童类别的国家区域医疗中心。全国医疗卫生机构床位975.0万张，每千人口医疗卫生机构床位数由2021年的6.70张增加到2022年的6.92张。全国卫生人员总数1441.1万人，比上年增加42.5万人（增长3.0%）。每千人口执业（助理）医师3.15人，每千人口注册护士3.71人。</p><p style=\"margin-top: 5px;\">2022年全国卫生总费用初步推算为84846.7亿元，其中政府卫生支出占28.2%；社会卫生支出占44.8%；个人卫生支出22914.5亿元，占27.0%。人均卫生总费用6010.0元，卫生总费用占GDP的比重为7.0%。</p><p style=\"margin-top: 5px;\">2022年，全国医疗卫生机构总诊疗人次84.2亿，与上年基本持平。全国医疗卫生机构入院人次24686万，与上年相比基本持平，居民年住院率为17.5%。</p><p style=\"margin-top: 5px;\">2022年，医院次均门诊费用342.7元，次均住院费用10860.6元，日均住院费用1186.5元。其中，医院次均门诊药费130.3元，占次均门诊费用的38.0%，比上年（37.5%）增加0.5个百分点；医院次均住院药费2640.5元，占次均住院费用的24.3%，比上年（25.1%）下降0.8个百分点。</p><p style=\"margin-top: 5px;\">各级公立医院中，三级医院次均门诊费用上涨3.1%，次均住院费用下降了4.0%。</p><p style=\"margin-top: 5px;\">2022年末全国共有县级医院17555所</p><p style=\"margin-top: 5px;\">在基层卫生服务方面，2022年我国卫生健康事业发展统计公报显示，到2022年末，全国共有县级（含县级市）医院17555所，乡镇卫生院3.4万个，村卫生室58.8万个，全国县级（含县级市）医院诊疗人次13.5亿，乡镇卫生院诊疗人次12.1亿，村卫生室诊疗人次12.8亿。设立社区卫生服务中心（站）36448个，全国社区卫生服务中心诊疗人次6.9亿，社区卫生服务站诊疗人次1.4亿。</p><p style=\"margin-top: 5px;\">在基本公共卫生服务项目方面，基本公共卫生服务项目人均财政补助标准从2021年的79元提高至2022年的84元。2022年内在基层医疗卫生机构接受健康管理的65岁及以上老年人为12708.3万。2022年，全国中医类医疗卫生机构总诊疗人次12.3亿，比上年增加0.2亿人次。</p><p style=\"margin-top: 5px;\">2022年出生人口956万 二孩占比近4成</p><p style=\"margin-top: 5px;\">在人口发展和妇幼健康方面，2022年我国卫生健康事业发展统计公报显示，2022年全国出生人口956万人。</p><p style=\"margin-top: 5px;\">2022年出生人口中二孩占比为38.9%，三孩及以上占比为15.0%，出生人口性别比为111.1。2022年全国托育服务机构总数7.57万家，提供的托位数362.4万个，全国千人口托位数2.57个。农村部分计划生育家庭奖励扶助和计划生育家庭特别扶助“两项制度”共投入资金278.0亿元，比上年增加37.5亿元。</p><p style=\"margin-top: 5px;\">在妇幼健康方面，2022年我国孕产妇产前检查率97.9%，产后访视率96.5%。与上年比较，产前检查率和产后访视率均有提高。2022年全国住院分娩率为99.94%，基本实现全部住院分娩。</p><p style=\"margin-top: 5px;\">据全国妇幼健康监测，2022年，我国5岁以下儿童死亡率6.8‰，婴儿死亡率4.9‰，全国孕产妇死亡率为15.7/10万，均有不同程度下降。</p><p style=\"margin-top: 5px;\">2022年全国共为816.2万名计划怀孕夫妇提供免费检查，目标人群覆盖率平均达91.8%。筛查出的风险人群全部获得针对性的咨询指导和治疗转诊等服务，落实了孕前预防措施，有效降低了出生缺陷的发生风险。</p><p><br/></p>','网络','','http://localhost:94/kaidian/16.html','','1','1','10','0','0','1','0','0','0');
INSERT INTO `yzm_article` VALUES ('17','15','1','admin','管理员','华为5.5G手机要来了，与5G相比有哪些提升与革新？我有必要为了5.5G换手机吗？','','1697095205','1697095437','5.5G,手机,华为,要来,5G,相比','5G的普及进程尚未完成，又来了5.5G，网络通信技术的发展堪称日新月异。什么是5.5G？回答这个问题需要先搞清楚什么是5G。因为所谓5.5G就是5G的增强版，比5G传输速率更快、延时...','148','<div>5G的普及进程尚未完成，又来了5.5G，网络通信技术的发展堪称日新月异。什么是5.5G？回答这个问题需要先搞清楚什么是5G。因为所谓5.5G就是5G的增强版，比5G传输速率更快、延时更低、同时连接设备数量更高。</div><div>回过头来看什么是5G，我觉得直到现在很多人对5G依然存在很大误解。首先需要明确一点，5G是面向物联网的技术，而不是移动互联网。所以如果单纯从智能手机的网络体验来理解5G就会显得非常狭隘。</div><div><div><div><div><img src=\"/uploads/ueditor/image/20231012/1697095434851942.jpg\" title=\"华为5.5G手机要来了，与5G相比有哪些提升与革新？我有必要为了5.5G换手机吗？\" alt=\"华为5.5G手机要来了，与5G相比有哪些提升与革新？我有必要为了5.5G换手机吗？\"/></div></div></div></div><div>另外，普通消费者提到5G总是说它有多快，其实快只是5G的特性之一，而且在实际的体验当中感受并不明显。所谓5G比4G快10倍、20倍，现在又说5.5G比5G快10倍，抛开实际体验谈速度，这些都不过是数字而已。比方说，4G、5G都可以流畅打游戏、看视频，用户也知道5G比4G更快，但他很能直观感受得到，或者说体验差别不大。</div><div>为什么5G套餐那么难普及呢，原因就在这里。如果用户不能直观感受到代际差异就很难为此买单。从5G到5.5G我想也会面临同样的问题。如果单纯为了5.5G而换手机，我觉得意义不是特别大。</div><div>我看华为官方在介绍5.5G时，也是更侧重于它的物联网性能。比如物联网链接数量增加 10 倍、能源效率提高 10 倍、自动驾驶网络（ADN）将从 3 级升级到 4 级等等。这些改变才是最主要的，而相比之下，智能手机的网速更快了，显得不值一提。</div><div><div><div><div><img class=\"qnt-img-img\" src=\"https://inews.gtimg.com/news_bt/OBYZrX41yhEXFwOxHhqmuf78BSJB3ErUWOI3YqSl7tc_4AA/641\" alt=\"图片\" style=\"border: 0px; vertical-align: middle; width: 680px; height: 382.812px;\"/></div></div></div></div><div>5.5G实际上可以视为5G到6G的过度阶段，早在5G商用之初，关于6G的讨论就开始了。6G，最主要的特征就是实现卫星通信和地面通信的全面融合，所谓“星地通信”，这个工程现在可以说完成还不到一半。</div><div>华为之前推出了支持卫星通信的华为Mate 60系列，这是一个里程碑式的产品，但依然存在很多限制，比如说目前只支持电信卡，还要开通专门的卫星通信套餐，而且价格不菲。如果将来运营商的部署成本降下来，套餐的费用也降下来，相信卫星通信终将实现普及，但这将是一个漫长的过程。</div><div>目前中国移动、中国联通也在积极布局卫星网络，相信很快支持卫星通信的运营商就会不只中国电信一家。如果市场竞争足够充分，那么星地通信的实现进程就会更快一些，6G时代也会来得更快一些。我想，到那时回过头来看5.5G，可能就会觉得它只不过是大餐之前的一碟开胃小菜而已。</div><p><br/></p>','网络','/uploads/202310/12/231012032312351.jpg','http://localhost:94/kaidian/17.html','','1','1','10','0','0','1','0','0','0');
INSERT INTO `yzm_article` VALUES ('18','15','1','admin','管理员','江西女子在明月山徒步失联11天，事发前半小时还与女儿通电话','','1697095583','1697095649','江西,女子,明月山,徒步,失联,11天','极目新闻记者 满达10月11日，海拔千余米的江西明月山云雾缭绕。密林之间，家属、民警、消防员和民间搜救队员，仍在苦苦搜寻黄女士的踪迹。这是黄女士失联的第11天。10月1日，...','104','<div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">极目新闻记者 满达</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">10月11日，海拔千余米的江西明月山云雾缭绕。密林之间，家属、民警、消防员和民间搜救队员，仍在苦苦搜寻黄女士的踪迹。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">这是黄女士失联的第11天。10月1日，在江西宜春开超市的她和户外群的群友一起徒步穿行武功山和明月山。当天下午5时51分，她和另外3名群友在明月山的羊角尖下山时，意外失联。之后，当地官方和民间救援力量一直没有放弃搜救。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">10月11日傍晚，刚从明月山上撤离回城的家属告诉极目新闻（报料邮箱：jimu1701@163.com）记者，黄女士失联前半小时，还接到女儿来电，问她怎么还没回来，她告诉女儿，自己快下山了。没想到，半小时后，和她一起徒步的驴友就发现，黄女士不见了。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\"><span class=\"qnt-strong\" style=\"overflow-wrap: break-word; word-break: break-all; font-weight: 700;\">徒步下山蹊跷失联</span></div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">49岁的黄女士和丈夫在宜春市袁州区开了一家批发超市。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">丈夫介绍，黄女士喜欢运动，参加了当地的跑步团，每天清晨都会去跑步。她在跑步团认识了一位户外群的群主，对方将她拉到了户外群。后来，黄女士陆续跟群友去徒步过两次。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">10月1日，她和30多名群友一起徒步穿越武功山和明月山，这是她第一次走这条线路。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\"><div class=\"qnt-img\" style=\"max-width: 100%; width: 680px; display: inline-block; vertical-align: top; overflow-wrap: break-word; word-break: break-all; margin-bottom: 0px;\"><div style=\"border-radius: 6px; overflow: hidden; width: 680px;\"><div class=\"lazyload-wrapper \" style=\"width: 680px; display: flex; justify-content: center; position: relative;\"><img class=\"qnt-img-img\" src=\"https://inews.gtimg.com/om_bt/OsHmD0MxK9x4Rnmf9-Ut7DCWZkts70bugMdMWkyjfkn6AAA/641\" alt=\"图片\" style=\"border: 0px none; vertical-align: middle; width: 680px; height: 1146.08px;\"/></div></div></div></div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">黄女士失联前曾在山上拍摄短视频（视频截图）</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">黄女士喜欢发短视频记录生活。当天下午3点26分，她在山上发了一段视频。视频中的她穿着一件亮眼的黄色T恤，胸前还印有户外群的标识。她戴着围巾和一顶白色的运动帽，身后的山涧弥漫着浓浓的云雾，能见度很低。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">黄女士的姐姐称，下午4点40分左右，黄女士和群友来到了位于羊狮幕的观光小火车站。此时，大部分群友通过乘坐小火车或其他方式中途下山。剩下黄女士和另外3名群友决定继续徒步，去往羊角尖附近，从潭下村的位置下山。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">同行的4人系两男两女，包括户外群的群主。“有两人走在前面，我妹妹第三，群主在最后。”黄女士的姐姐说。5点24分，黄女士接到了女儿的电话，对方问她怎么还没回来。黄女士告诉女儿，自己还有一个半小时就要下山了。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">接完电话后，黄女士将手机放回了随身携带的包里。后来，群主帮黄女士背着包。下山途中穿过一片茶园时，群主停下来摘了些野菜，后来又追赶前面的群友。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">黄女士的姐姐从3名群友处获悉，他们最后看到黄女士，是在下午5点51分左右。他们发现黄女士失联后，又通知山下的群友上山来帮忙寻找。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\"><span class=\"qnt-strong\" style=\"overflow-wrap: break-word; word-break: break-all; font-weight: 700;\">搜寻10日没有结果</span></div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">家属得知黄女士失联时，已是晚上8点多。当晚，就有公安民警、消防员和当地的蓝天救援队在搜救，但一夜过去，并没有黄女士的消息。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">黄女士的姐姐介绍，户外群的成员走的都是野路，并非景区的道路，有的位置会有山崖。但妹妹的体力还算不错。事后她查看了羊狮幕小火车站的监控视频，当时他们刚翻过一座山下来，其他群友有坐下来休息，妹妹都不需要坐下来，期间还甩了甩手中的登山杖。后来，4个人还小跑了一段，那时候妹妹还跑在第二。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">10月2日和3日，当地天晴，山里的能见度较佳。搜救人员在山上到处搜寻，呼唤黄女士的名字，但没有结果。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\"><div class=\"qnt-img\" style=\"max-width: 100%; width: 680px; display: inline-block; vertical-align: top; overflow-wrap: break-word; word-break: break-all; margin-bottom: 0px;\"><div style=\"border-radius: 6px; overflow: hidden; width: 680px;\"><div class=\"lazyload-wrapper \" style=\"width: 680px; display: flex; justify-content: center; position: relative;\"><img class=\"qnt-img-img\" src=\"https://inews.gtimg.com/om_bt/O0c5P0b5-ptCbrxZKWhhXCN9Sc5KguFzOVkmDVSJuI4qUAA/641\" alt=\"图片\" style=\"border: 0px none; vertical-align: middle; width: 680px; height: 1270.75px;\"/></div></div></div></div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">民警在密林内搜救（视频截图）</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">黄女士的丈夫称，目前除了家属，还有森林消防、民警、蓝天救援队以及其他民间救援队参与搜救。多的时候有一两百人，少的时候也有数十人。10月11日傍晚，他刚从明月山撤离准备乘车回城，他告诉极目新闻记者，自己这些天很是担心妻子，还忙着搜寻，感到头痛欲裂。有搜救队员称，看到黄女士的丈夫拖着疲惫的身躯，在山里一边敲锣，一边呼唤妻子的名字，情绪崩溃，大家看着都好心痛。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\"><div class=\"qnt-img\" style=\"max-width: 100%; width: 680px; display: inline-block; vertical-align: top; overflow-wrap: break-word; word-break: break-all; margin-bottom: 0px;\"><div style=\"border-radius: 6px; overflow: hidden; width: 680px;\"><div class=\"lazyload-wrapper \" style=\"width: 680px; display: flex; justify-content: center; position: relative;\"><img class=\"qnt-img-img\" src=\"https://inews.gtimg.com/om_bt/Oz4yykLIorLakDURTJR9KuQ34TF_Pp24P4ZIXbACE7Vd4AA/641\" alt=\"图片\" style=\"border: 0px none; vertical-align: middle; width: 680px; height: 1299.08px;\"/></div></div></div></div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">救援队员在山上的茶园搜救（视频截图）</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">“一开始白天和晚上都在搜救，现在主要是白天。”黄女士的姐姐说，搜救人员在山崖边也看了，都没找到有人滑下去的痕迹。妹妹穿着黄色的上衣、戴着白色的帽子，应该比较显眼，但目前都没发现她本人以及她身上的物品。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">宜春市战狼探险队也参与了黄女士的搜救行动。队长辛观生介绍，10月3日傍晚，他和3名队友背着无人机、热成像仪等设备从潭下村登山，并一路用强光在山间搜寻，呼唤黄女士的名字。因为背着很重的装备，他们花了4小时才爬到海拔1300多米的羊角尖分岔口。当晚搜寻未果后，他们就在分岔口睡了一晚。第二天早上，趁着山上有点阳光，用无人机进行搜寻。不久后，山里起了浓雾，无人机已经无法搜寻了。期间，他们来到一处山崖，顺着山崖往下查看，未发现有人滑下去的痕迹。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\"><div class=\"qnt-img\" style=\"max-width: 100%; width: 680px; display: inline-block; vertical-align: top; overflow-wrap: break-word; word-break: break-all; margin-bottom: 0px;\"><div style=\"border-radius: 6px; overflow: hidden; width: 680px;\"><div class=\"lazyload-wrapper \" style=\"width: 680px; display: flex; justify-content: center; position: relative;\"><img class=\"qnt-img-img\" src=\"https://inews.gtimg.com/om_bt/OzM2srL9z4d3FZ9-Y6wQbVzr8pJM6dWcSTL5QMd8DwNMMAA/641\" alt=\"图片\" style=\"border: 0px none; vertical-align: middle; width: 680px; height: 359.125px;\"/></div></div></div></div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">战狼救援队夜间下山崖搜救（视频截图）</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">5日，辛观生和队友再次乘坐缆车上山。根据分析，不排除黄女士可能因为迷路，往回走了一段路，来到千丈崖附近的分岔口。这个位置比较危险，有滑下山崖的可能。辛观生和队友利用绳索下降200余米，在千丈崖下面搜寻，还来到附近几处瀑布查看，仍然一无所获。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">6日至8日，辛观生的队友们继续在山上搜寻，他们一度在羊角尖的山崖下降了300余米，还是没能找到。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\"><span class=\"qnt-strong\" style=\"overflow-wrap: break-word; word-break: break-all; font-weight: 700;\">事发地位于热门徒步路线</span></div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">辛观生介绍，黄女士失联的位置再往前，左边有一条不太明显的小路，是下山前往潭下村的路，而右边的路较宽，往前走实际上并不能下山，而那边的山崖较多。不排除黄女士走了右边那条路，在某处山崖上滑落。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">极目新闻记者注意到，地处萍乡市的武功山和属于宜春市的明月山相隔不远，徒步穿越武功山和明月山，是颇受驴友欢迎的徒步线路。还有驴友在网上分享攻略，为穿越武功山和明月山设计了各种不同的经典徒步线路。根据短视频平台和户外爱好者论坛的信息，黄女士所加入的户外群是宜春当地较有名的户外群，曾组织过多次徒步武功山或明月山的活动。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">不过，因为徒步所经过的区域很多属于未开发的景区，且山上多雾，还有山崖，往往伴随着危险。据江西当地媒体报道，就在今年5月下旬，一名来自山东烟台的女性驴友在武功山徒步穿行，由于她走的是没开发的路线，加上景区起雾，结果不慎跌落悬崖，腰部受伤，手臂骨折，而手机等物品又掉落在悬崖。事发5天后，躺在溪流边的她才被上山砍木蔸的村民发现。2020年4月12日，一名52岁的湖南籍男游客，独自从一条小路徒步上明月山，最后在深山中迷路被困。直到次日下午，被困山中一天一夜的他才被消防员和蓝天救援队员发现。</div><div class=\"qnt-p\" style=\"margin: 0px 0px 30px; line-height: 30px; font-size: 18px; text-align: justify; color: rgb(51, 51, 51); letter-spacing: 0.2px; overflow-wrap: break-word; word-break: break-all; font-family: &quot;PingFang SC&quot;, &quot;Microsoft Yahei&quot;, Avenir, &quot;Segoe UI&quot;, &quot;Hiragino Sans GB&quot;, STHeiti, &quot;Microsoft Sans Serif&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; text-wrap: wrap;\">黄女士的家属介绍，事后警方也介入了调查，目前家属还在努力寻找黄女士的下落。记者联系了户外群的群主易先生，他称自己只会接受警方的调查询问，不方便接受采访。</div><p><br/></p>','网络','','http://localhost:94/kaidian/18.html','','1','1','10','0','0','1','0','1','0');
INSERT INTO `yzm_article` VALUES ('21','15','1','admin','管理员','重庆一 33 层楼房楼底被私挖 4 室 1 厅，这可能会带来什么后果？如何看待此事？','','1697098219','1697098245','重庆,楼房楼,私挖,可能,带来,什么','据重庆天天630报道，近日，据重庆北碚蔡家岗锦馨苑业主反映，底楼有住户私自挖地下室，面积相当于四室一厅。锦馨苑小区1栋有33层高，共有业主200多户，从接房入住到现在共有七...','60','<p data-first-child=\"\" style=\"margin-top: 0px; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: 15px; text-wrap: wrap; background-color: rgb(255, 255, 255);\">据重庆天天630报道，近日，据重庆北碚蔡家岗锦馨苑业主反映，底楼有住户私自挖地下室，面积相当于四室一厅。</p><p><figure data-size=\"normal\" style=\"margin: 1.4em 0px; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: 15px; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><img src=\"/uploads/ueditor/image/20231012/1697098240180962.jpg\" data-caption=\"\" data-size=\"normal\" data-rawwidth=\"1130\" data-rawheight=\"652\" class=\"origin_image zh-lightbox-thumb\" width=\"1130\" data-original=\"https://pic4.zhimg.com/v2-8f2402e2d6fe8db9627a98a3a483515f_r.jpg\" style=\"display: block; margin: 0px auto; max-width: 100%; height: auto; cursor: zoom-in; width: 674px;\"/></figure></p><p style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: 15px; text-wrap: wrap; background-color: rgb(255, 255, 255);\">锦馨苑小区1栋有33层高，共有业主200多户，从接房入住到现在共有七八年时间。近日，小区居民发现底楼用户开挖了地下室，面积相当于四室一厅。</p><p><figure data-size=\"normal\" style=\"margin: 1.4em 0px; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: 15px; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><img src=\"/uploads/ueditor/image/20231012/1697098240106519.jpg\" data-caption=\"\" data-size=\"normal\" data-rawwidth=\"956\" data-rawheight=\"570\" class=\"origin_image zh-lightbox-thumb\" width=\"956\" data-original=\"https://pic4.zhimg.com/v2-50a37845f30c604fa2bb0f396282e7bb_r.jpg\" style=\"display: block; margin: 0px auto; max-width: 100%; height: auto; cursor: zoom-in; width: 674px;\"/></figure></p><p style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: 15px; text-wrap: wrap; background-color: rgb(255, 255, 255);\">该地下室目前水电俱全，还有家具、电器等生活用品。底楼住户称地下室只是用来堆东西，并非用作出租用途，开挖地下室的详细情况自己已经记不清了。</p><p><figure data-size=\"normal\" style=\"margin: 1.4em 0px; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: 15px; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><img src=\"/uploads/ueditor/image/20231012/1697098241396585.jpg\" data-caption=\"\" data-size=\"normal\" data-rawwidth=\"986\" data-rawheight=\"1346\" class=\"origin_image zh-lightbox-thumb\" width=\"986\" data-original=\"https://pic1.zhimg.com/v2-099df10003889a1c7d310c0e3312f2e4_r.jpg\" style=\"display: block; margin: 0px auto; max-width: 100%; height: auto; cursor: zoom-in; width: 674px;\"/></figure></p><p style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: 15px; text-wrap: wrap; background-color: rgb(255, 255, 255);\">小区业主表示：“我们辛辛苦苦买一套房，如果成了危房，自己不能住，还欠一身债。”</p><p style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: 15px; text-wrap: wrap; background-color: rgb(255, 255, 255);\">对于一楼住户私挖地下室的情况，小区物业经理表示，他们不是前期物业，在他们入驻前，这些私自开挖的地下室就存在。</p><p><figure data-size=\"normal\" style=\"margin: 1.4em 0px; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: 15px; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><img src=\"/uploads/ueditor/image/20231012/1697098241350658.jpg\" data-caption=\"\" data-size=\"normal\" data-rawwidth=\"990\" data-rawheight=\"550\" class=\"origin_image zh-lightbox-thumb\" width=\"990\" data-original=\"https://pic3.zhimg.com/v2-941e59b4b5585b3f8ea0987c7fbd8dae_r.jpg\" style=\"display: block; margin: 0px auto; max-width: 100%; height: auto; cursor: zoom-in; width: 674px;\"/></figure></p><p style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: 15px; text-wrap: wrap; background-color: rgb(255, 255, 255);\">蔡家岗街道规建办工作人员表示，他们已经向北碚区相关部门汇报，要求底楼住户将所有地下室回填，恢复原样。</p><p style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: 15px; text-wrap: wrap; background-color: rgb(255, 255, 255);\">该工作人员表示：“等住建委来了，看下图纸，看对房屋安全有没有影响，没有影响就进行回填，恢复原状。”</p><p style=\"margin-top: 1.4em; margin-bottom: 1.4em; color: rgb(18, 18, 18); font-family: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Microsoft YaHei&quot;, &quot;Source Han Sans SC&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, sans-serif; font-size: 15px; text-wrap: wrap; background-color: rgb(255, 255, 255);\">私挖地下室的1-1业主表示，会按照有关部门的要求进行整改。</p><p><br/></p>','网络','/uploads/ueditor/image/20231012/thumb_500_300_1697098240180962.jpg','http://localhost:94/kaidian/21.html','','1','1','10','0','0','1','0','0','0');
INSERT INTO `yzm_article` VALUES ('22','15','1','admin','管理员','曝高洪波将离开中国足协 记者:主动退赃员工还能受重用吗？ ','','1697098299','1697098319','曝高,洪波,离开,中国,足协,记者','北京时间10月12日消息，按照计划足代会将于本月中旬召开，选举产生新一届中国足协领导班子。此前有报道称，中国足协执委会人选会有不小的变化，本届执委会35人，部分执委来自...','298','<p class=\"ql-align-center\" style=\"border: 0px; margin-top: 0.63em; margin-bottom: 1.8em; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; text-align: center; color: rgb(25, 25, 25); font-family: &quot;PingFang SC&quot;, Arial, 微软雅黑, 宋体, simsun, sans-serif; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><img max-width=\"600\" src=\"/uploads/ueditor/image/20231012/1697098317992435.jpeg\" style=\"border: 0px; margin: 10px auto 0px; padding: 0px; display: block; height: auto; max-width: 100%;\"/></p><p style=\"border: 0px; margin-top: 0.63em; margin-bottom: 1.8em; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; color: rgb(25, 25, 25); font-family: &quot;PingFang SC&quot;, Arial, 微软雅黑, 宋体, simsun, sans-serif; text-wrap: wrap; background-color: rgb(255, 255, 255);\">北京时间10月12日消息，按照计划足代会将于本月中旬召开，选举产生新一届中国足协领导班子。</p><p style=\"border: 0px; margin-top: 0.63em; margin-bottom: 1.8em; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; color: rgb(25, 25, 25); font-family: &quot;PingFang SC&quot;, Arial, 微软雅黑, 宋体, simsun, sans-serif; text-wrap: wrap; background-color: rgb(255, 255, 255);\">此前有报道称，中国足协执委会人选会有不小的变化，本届执委会35人，部分执委来自足球以外的领域，新一届执委会人数计划调整到20名左右，大部分执委人选来自各地方体育局。</p><p style=\"border: 0px; margin-top: 0.63em; margin-bottom: 1.8em; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; color: rgb(25, 25, 25); font-family: &quot;PingFang SC&quot;, Arial, 微软雅黑, 宋体, simsun, sans-serif; text-wrap: wrap; background-color: rgb(255, 255, 255);\">而在此之前，足协“工作组”相关成员实际上已经进入领导班子成员的角色。如从手曲棒垒中心前来的杨旭就已分管职业联赛。在那之前，袁永清已被任命为中国足协党委副书记。他们两人都将进入新一届中国足协领导班子。</p><p style=\"border: 0px; margin-top: 0.63em; margin-bottom: 1.8em; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; color: rgb(25, 25, 25); font-family: &quot;PingFang SC&quot;, Arial, 微软雅黑, 宋体, simsun, sans-serif; text-wrap: wrap; background-color: rgb(255, 255, 255);\">至于现任足协副主席高洪波、孙雯、足协纪委书记闫占河会否进入新一届足协领导班子，目前还不得而知。对此，足球报记者李璇透露：足代会马上要召开了，我想知道的是，那些之前主动交待并退赃的员工，虽然没有被追究法律责任，但未来在足协怎么使用？还能受重用吗？</p><p style=\"border: 0px; margin-top: 0.63em; margin-bottom: 1.8em; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; color: rgb(25, 25, 25); font-family: &quot;PingFang SC&quot;, Arial, 微软雅黑, 宋体, simsun, sans-serif; text-wrap: wrap; background-color: rgb(255, 255, 255);\">可以参考的前车之鉴是，陈永亮就是上次反赌扫黑案的涉案人员，法院宣判书里都有他的名字和具体犯罪事实，但他在足协留任，在杜兆才和陈戌源任上还受到重用，成为国管部部长、中国足协常务副秘书长。最后在这次的中国足协反腐案中再次涉案。</p><p style=\"border: 0px; margin-top: 0.63em; margin-bottom: 1.8em; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; color: rgb(25, 25, 25); font-family: &quot;PingFang SC&quot;, Arial, 微软雅黑, 宋体, simsun, sans-serif; text-wrap: wrap; background-color: rgb(255, 255, 255);\">此外，李璇还表示，高洪波这次足代会后应该是要离开中国足协了，至少他应该不是班子成员。</p><p class=\"ql-align-center\" style=\"border: 0px; margin-top: 0.63em; margin-bottom: 1.8em; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; text-align: center; color: rgb(25, 25, 25); font-family: &quot;PingFang SC&quot;, Arial, 微软雅黑, 宋体, simsun, sans-serif; text-wrap: wrap; background-color: rgb(255, 255, 255);\"><img max-width=\"600\" src=\"/uploads/ueditor/image/20231012/1697098317187334.png\" style=\"border: 0px; margin: 10px auto 0px; padding: 0px; display: block; height: auto; max-width: 100%;\"/></p><p style=\"border: 0px; margin-top: 0.63em; margin-bottom: 1.8em; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; color: rgb(25, 25, 25); font-family: &quot;PingFang SC&quot;, Arial, 微软雅黑, 宋体, simsun, sans-serif; text-wrap: wrap; background-color: rgb(255, 255, 255);\">中国足协目前正处于过渡时期，辽宁省体育局局长宋凯作为“换届工作筹备组”副组长，进入中国足协工作了一段时间，他是新一届中国足协主席的主要人选。</p><p><br/></p>','网络','/uploads/ueditor/image/20231012/thumb_500_300_1697098317992435.jpeg','http://localhost:94/kaidian/22.html','','1','1','10','0','0','1','0','1','0');

-- -----------------------------
-- Table structure for `yzm_attachment`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_attachment`;
CREATE TABLE `yzm_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `siteid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `module` char(15) NOT NULL DEFAULT '',
  `contentid` varchar(20) NOT NULL DEFAULT '',
  `originname` varchar(50) NOT NULL DEFAULT '',
  `filename` varchar(50) NOT NULL DEFAULT '',
  `filepath` char(200) NOT NULL DEFAULT '',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `fileext` char(10) NOT NULL DEFAULT '',
  `isimage` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `downloads` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `uploadtime` int(10) unsigned NOT NULL DEFAULT '0',
  `uploadip` char(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `siteid` (`siteid`),
  KEY `userid_index` (`userid`),
  KEY `contentid` (`contentid`)
) ENGINE=MyISAM AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `yzm_attachment`
-- -----------------------------
INSERT INTO `yzm_attachment` VALUES ('1','0','admin','','221220035018581.png','231011110656650.png','/uploads/202310/11/','241440','png','1','0','1','admin','1696993616','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('2','0','banner','','banner-eric.jpg','231011111311175.jpg','/uploads/202310/11/','156910','jpg','1','0','1','admin','1696993991','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('3','0','banner','','banner-yzmcms.jpg','231011111338932.jpg','/uploads/202310/11/','154547','jpg','1','0','1','admin','1696994018','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('4','0','index','','远程抓取-1630380186131371.jpg','1696994355428229.jpg','/uploads/ueditor/image/20231011/','246028','jpg','1','0','1','admin','1696994350','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('5','0','index','','远程抓取-1630380183156394.jpg','1696994357136634.jpg','/uploads/ueditor/image/20231011/','284647','jpg','1','0','1','admin','1696994350','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('7','0','adver','','QQ截图20231011134513.png','231011014534882.png','/uploads/202310/11/','17968','png','1','0','1','admin','1697003134','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('8','0','adver','','QQ截图20231011134959.png','231011015023239.png','/uploads/202310/11/','21315','png','1','0','1','admin','1697003423','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('9','0','adver','','QQ截图20231011135122.png','231011015139548.png','/uploads/202310/11/','21403','png','1','0','1','admin','1697003499','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('10','0','admin','','dgy-logo.png','231011024651376.png','/uploads/202310/11/','3535','png','1','0','1','admin','1697006811','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('11','0','admin','','190117030507645.jpg','231011042617310.jpg','/uploads/202310/11/','18140','jpg','1','0','1','admin','1697012777','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('14','0','banner','','200410021935974.jpg','231011052121112.jpg','/uploads/202310/11/','148615','jpg','1','0','1','admin','1697016081','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('13','0','banner','','200407031451342.jpg','231011051623279.jpg','/uploads/202310/11/','97150','jpg','1','0','1','admin','1697015783','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('15','0','index','','远程抓取-201903051436106979.jpg','1697016275106912.jpg','/uploads/ueditor/image/20231011/','95759','jpg','1','0','1','admin','1697016273','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('16','0','index','','远程抓取-1552299246465699.png','1697016275809387.png','/uploads/ueditor/image/20231011/','46641','png','1','0','1','admin','1697016273','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('17','0','index','','远程抓取-1552298412576723.jpg','1697016276105786.jpg','/uploads/ueditor/image/20231011/','39982','jpg','1','0','1','admin','1697016273','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('18','0','admin','','190322015922984.jpg','231011052501325.jpg','/uploads/202310/11/','133473','jpg','1','0','1','admin','1697016301','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('19','0','admin','','dc146b8319ae4225bbf29899c2c009...','231012091914893.png','/uploads/202310/12/','473322','png','1','0','1','admin','1697073554','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('20','0','admin','','QQ截图20231012130554.png','231012010709218.png','/uploads/202310/12/','346766','png','1','0','1','admin','1697087229','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('21','0','index','','QQ截图20231012130554.png','1697087259352413.png','/uploads/ueditor/image/20231012/','346766','png','1','0','1','admin','1697087259','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('22','0','admin','','QQ截图20231012131244.png','231012011655423.png','/uploads/202310/12/','367807','png','1','0','1','admin','1697087815','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('23','0','index','','QQ截图20231012131244.png','1697087825305988.png','/uploads/ueditor/image/20231012/','367807','png','1','0','1','admin','1697087825','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('24','0','admin','','QQ截图20231012132115.png','231012012121766.png','/uploads/202310/12/','174467','png','1','0','1','admin','1697088081','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('25','0','admin','','33a72d5e-f1b1-4926-b698-cabf8d...','231012012820879.jpeg','/uploads/202310/12/','33642','jpeg','1','0','1','admin','1697088500','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('26','0','admin','','QQ截图20231012133251.png','231012013301221.png','/uploads/202310/12/','317626','png','1','0','1','admin','1697088781','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('27','0','index','','QQ截图20231012133607.png','1697089038599153.png','/uploads/ueditor/image/20231012/','285380','png','1','0','1','admin','1697089038','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('28','0','admin','','641.png','231012031047415.png','/uploads/202310/12/','881683','png','1','0','1','admin','1697094647','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('29','0','admin','','641.jpg','231012032312351.jpg','/uploads/202310/12/','32644','jpg','1','0','1','admin','1697095392','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('30','0','index','','641.jpg','1697095434851942.jpg','/uploads/ueditor/image/20231012/','32644','jpg','1','0','1','admin','1697095434','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('31','0','index','','远程抓取-1676018917285273.jpg','1697095944480079.jpg','/uploads/ueditor/image/20231012/','276524','jpg','1','0','1','admin','1697095942','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('32','0','index','','远程抓取-v2-8f2402e2d6fe8db962...','1697098240180962.jpg','/uploads/ueditor/image/20231012/','33613','jpg','1','0','1','admin','1697098240','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('33','0','index','','远程抓取-v2-50a37845f30c604fa2...','1697098240106519.jpg','/uploads/ueditor/image/20231012/','39988','jpg','1','0','1','admin','1697098240','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('34','0','index','','远程抓取-v2-099df10003889a1c7d...','1697098241396585.jpg','/uploads/ueditor/image/20231012/','59357','jpg','1','0','1','admin','1697098240','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('35','0','index','','远程抓取-v2-941e59b4b5585b3f8e...','1697098241350658.jpg','/uploads/ueditor/image/20231012/','43037','jpg','1','0','1','admin','1697098240','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('36','0','index','','远程抓取-290eac8eb4814e4286db86c7b1663e0a.jpeg','1697098317992435.jpeg','/uploads/ueditor/image/20231012/','42463','jpeg','1','0','1','admin','1697098317','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('37','0','index','','远程抓取-0e9b4156c1cb45cea995c8b0187c620d.png','1697098317187334.png','/uploads/ueditor/image/20231012/','15937','png','1','0','1','admin','1697098317','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('38','0','index','','远程抓取-b545723099584f8dbaa13a7ae19eb1b6.jpeg','1697099456308642.jpeg','/uploads/ueditor/image/20231012/','41492','jpeg','1','0','1','admin','1697099456','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('39','0','index','','远程抓取-542.jpg','1697100247111191.jpg','/uploads/ueditor/image/20231012/','538344','jpg','1','0','1','admin','1697100246','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('40','0','index','','远程抓取-543.png','1697100248190174.png','/uploads/ueditor/image/20231012/','465140','png','1','0','1','admin','1697100246','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('41','0','index','','远程抓取-338.jpg','1697100593868873.jpg','/uploads/ueditor/image/20231012/','72824','jpg','1','0','1','admin','1697100593','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('42','0','index','','远程抓取-320.jpg','1697100593166124.jpg','/uploads/ueditor/image/20231012/','71780','jpg','1','0','1','admin','1697100593','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('43','0','index','','远程抓取-a5bb05a576a444449625c0e96aff434f.png','1697100879108770.png','/uploads/ueditor/image/20231012/','131693','png','1','0','1','admin','1697100879','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('44','0','index','','QQ截图20231012171153.png','1697101936817335.png','/uploads/ueditor/image/20231012/','90577','png','1','0','1','admin','1697101936','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('45','0','index','','QQ截图20231012171244.png','1697101974524809.png','/uploads/ueditor/image/20231012/','111814','png','1','0','1','admin','1697101974','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('46','0','api','','远程下载-v2-11b7622534a9a1ca689df31247b60797_720w.webp','20231013110258646.webp','/uploads/202310/13/','60982','webp','1','0','1','admin','1697166178','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('47','0','api','','远程下载-v2-11b7622534a9a1ca689df31247b60797_b.jpg','20231013110259101.jpg','/uploads/202310/13/','74976','jpg','1','0','1','admin','1697166178','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('48','0','api','','远程下载-v2-bc8c90bbc71d3a426619c8fc69798495_720w.webp','20231013110259455.webp','/uploads/202310/13/','27116','webp','1','0','1','admin','1697166178','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('49','0','api','','远程下载-v2-bc8c90bbc71d3a426619c8fc69798495_b.jpg','20231013110259395.jpg','/uploads/202310/13/','41608','jpg','1','0','1','admin','1697166178','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('50','0','api','','远程下载-v2-9bc0841b091a50be6f500010657d29a3_720w.webp','20231013110259571.webp','/uploads/202310/13/','39934','webp','1','0','1','admin','1697166178','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('51','0','api','','远程下载-v2-9bc0841b091a50be6f500010657d29a3_b.jpg','20231013110259992.jpg','/uploads/202310/13/','53346','jpg','1','0','1','admin','1697166178','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('52','0','api','','远程下载-v2-ac9680a245748d09197883b4f4fa7ed5_720w.webp','20231013110259627.webp','/uploads/202310/13/','42678','webp','1','0','1','admin','1697166178','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('53','0','api','','远程下载-v2-ac9680a245748d09197883b4f4fa7ed5_b.jpg','20231013110300761.jpg','/uploads/202310/13/','59980','jpg','1','0','1','admin','1697166178','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('54','0','api','','远程下载-v2-d2b9dac9c85a6a04cd694f485dbe66fe_720w.webp','20231013110300735.webp','/uploads/202310/13/','24506','webp','1','0','1','admin','1697166178','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('55','0','api','','远程下载-v2-d2b9dac9c85a6a04cd694f485dbe66fe_b.jpg','20231013110300576.jpg','/uploads/202310/13/','36939','jpg','1','0','1','admin','1697166178','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('56','0','api','','远程下载-v2-ab29d43821c43aafb00a65144db371a9_720w.webp','20231013110300677.webp','/uploads/202310/13/','16568','webp','1','0','1','admin','1697166178','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('57','0','api','','远程下载-v2-ab29d43821c43aafb00a65144db371a9_b.jpg','20231013110300230.jpg','/uploads/202310/13/','35998','jpg','1','0','1','admin','1697166178','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('58','0','api','','远程下载-v2-b6c5342c67d8da4f0081ead6759ee595_720w.webp','20231013110300474.webp','/uploads/202310/13/','32720','webp','1','0','1','admin','1697166178','127.0.0.1');
INSERT INTO `yzm_attachment` VALUES ('59','0','api','','远程下载-v2-b6c5342c67d8da4f0081ead6759ee595_b.jpg','20231013110301555.jpg','/uploads/202310/13/','47994','jpg','1','0','1','admin','1697166178','127.0.0.1');

-- -----------------------------
-- Table structure for `yzm_banner`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_banner`;
CREATE TABLE `yzm_banner` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `image` varchar(150) NOT NULL DEFAULT '',
  `url` varchar(150) NOT NULL DEFAULT '',
  `introduce` varchar(255) NOT NULL DEFAULT '' COMMENT '简介',
  `inputtime` int(10) unsigned NOT NULL DEFAULT '0',
  `listorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `typeid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '1显示0隐藏',
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `typeid` (`typeid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `yzm_banner`
-- -----------------------------
INSERT INTO `yzm_banner` VALUES ('1','首页轮播','/uploads/202310/11/231011111311175.jpg','http://localhost:94','','1696994000','1','1','1');
INSERT INTO `yzm_banner` VALUES ('2','首页轮播2','/uploads/202310/11/231011111338932.jpg','http://localhost:94/','','1696994025','2','1','1');
INSERT INTO `yzm_banner` VALUES ('3','极速建站免费开源','/uploads/202310/11/231011051623279.jpg','http://ahttps://www.yzmcms.com/','','1697015756','1','2','1');
INSERT INTO `yzm_banner` VALUES ('4','小程序开发问题','/uploads/202310/11/231011052121112.jpg','http://https://www.duoguyu.com/smart/35.html','','1697015787','1','2','1');

-- -----------------------------
-- Table structure for `yzm_banner_type`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_banner_type`;
CREATE TABLE `yzm_banner_type` (
  `tid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`tid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `yzm_banner_type`
-- -----------------------------
INSERT INTO `yzm_banner_type` VALUES ('1','首页轮播');
INSERT INTO `yzm_banner_type` VALUES ('2','互联网轮播');

-- -----------------------------
-- Table structure for `yzm_category`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_category`;
CREATE TABLE `yzm_category` (
  `catid` smallint(5) NOT NULL AUTO_INCREMENT COMMENT '栏目ID',
  `siteid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `catname` varchar(60) NOT NULL DEFAULT '' COMMENT '栏目名称',
  `modelid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '模型id',
  `parentid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '父级id',
  `arrparentid` varchar(255) NOT NULL DEFAULT '' COMMENT '父级路径',
  `arrchildid` mediumtext NOT NULL COMMENT '子栏目id集合',
  `catdir` varchar(50) NOT NULL DEFAULT '' COMMENT '栏目目录',
  `catimg` varchar(150) NOT NULL DEFAULT '' COMMENT '栏目图片',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '栏目类型:0普通栏目1单页2外部链接',
  `listorder` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '栏目排序',
  `target` char(10) NOT NULL DEFAULT '' COMMENT '打开方式',
  `member_publish` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否会员投稿',
  `display` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '在导航显示',
  `pclink` varchar(100) NOT NULL DEFAULT '' COMMENT '电脑版地址',
  `domain` varchar(100) NOT NULL DEFAULT '' COMMENT '绑定域名',
  `entitle` varchar(80) NOT NULL DEFAULT '' COMMENT '英文标题',
  `subtitle` varchar(60) NOT NULL DEFAULT '' COMMENT '副标题',
  `mobname` varchar(50) NOT NULL DEFAULT '' COMMENT '手机版名称',
  `category_template` varchar(30) NOT NULL DEFAULT '' COMMENT '频道页模板',
  `list_template` varchar(30) NOT NULL DEFAULT '' COMMENT '列表页模板',
  `show_template` varchar(30) NOT NULL DEFAULT '' COMMENT '内容页模板',
  `seo_title` varchar(100) NOT NULL DEFAULT '' COMMENT 'SEO标题',
  `seo_keywords` varchar(200) NOT NULL DEFAULT '' COMMENT 'SEO关键字',
  `seo_description` varchar(250) NOT NULL DEFAULT '' COMMENT 'SEO描述',
  PRIMARY KEY (`catid`),
  KEY `siteid` (`siteid`),
  KEY `modelid` (`modelid`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `yzm_category`
-- -----------------------------
INSERT INTO `yzm_category` VALUES ('23','0','抖音','1','22','0,22','23','douyin','','0','0','_self','0','1','http://localhost:94/douyin/','','','','抖音','category_article','list_article','show_article','','','');
INSERT INTO `yzm_category` VALUES ('14','0','淘宝开店','1','0','0','14,15,16','taobaokaidian','','0','0','_self','0','1','http://localhost:94/taobaokaidian/','','icon-kaidian','','淘宝开店','category_article','list_article','show_article','','','');
INSERT INTO `yzm_category` VALUES ('19','0','淘宝推广','1','0','0','19,20,21','taobaotuiguang','','0','0','_self','0','1','http://localhost:94/taobaotuiguang/','','icon-tuiguangyaoqing','','淘宝推广','category_article','list_article','show_article','','','');
INSERT INTO `yzm_category` VALUES ('20','0','推广','1','19','0,19','20','tuiguang','','0','0','_self','0','1','http://localhost:94/tuiguang/','','','','推广','category_article','list_article','show_article','','','');
INSERT INTO `yzm_category` VALUES ('21','0','直通车','1','19','0,19','21','zhitongche','','0','0','_self','0','1','http://localhost:94/zhitongche/','','','','直通车','category_article','list_article','show_article','','','');
INSERT INTO `yzm_category` VALUES ('16','0','运营','1','14','0,14','16','yunying','','0','0','_self','0','1','http://localhost:94/yunying/','','','','运营','category_article','list_article','show_article','','','');
INSERT INTO `yzm_category` VALUES ('15','0','开店','1','14','0,14','15','kaidian','','0','0','_self','0','1','http://localhost:94/kaidian/','','','','开店','category_article','list_article','show_article','','','');
INSERT INTO `yzm_category` VALUES ('22','0','直播电商','1','0','0','22,23,24,25','zhibodianshang','','0','0','_self','0','1','http://localhost:94/zhibodianshang/','','icon-format12','','直播电商','category_article','list_article','show_article','','','');
INSERT INTO `yzm_category` VALUES ('24','0','快手','1','22','0,22','24','kuaishou','','0','0','_self','0','1','http://localhost:94/kuaishou/','','','','快手','category_article','list_article','show_article','','','');
INSERT INTO `yzm_category` VALUES ('25','0','微信','1','22','0,22','25','weixin','','0','0','_self','0','1','http://localhost:94/weixin/','','','','微信','category_article','list_article','show_article','','','');
INSERT INTO `yzm_category` VALUES ('26','0','电商杂谈','1','0','0','26','dianshangzatan','','0','0','_self','0','1','http://localhost:94/dianshangzatan/','','icon-qita','','电商杂谈','category_article','list_article','show_article','','','');

-- -----------------------------
-- Table structure for `yzm_collection_content`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_collection_content`;
CREATE TABLE `yzm_collection_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nodeid` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0:未采集,1:已采集,2:已导入',
  `url` char(255) NOT NULL DEFAULT '',
  `title` char(100) NOT NULL DEFAULT '',
  `data` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `nodeid` (`nodeid`),
  KEY `status` (`status`),
  KEY `url` (`url`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_collection_node`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_collection_node`;
CREATE TABLE `yzm_collection_node` (
  `nodeid` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '采集节点ID',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '节点名称',
  `lastdate` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后采集时间',
  `sourcecharset` varchar(8) NOT NULL DEFAULT '' COMMENT '采集点字符集',
  `sourcetype` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '网址类型:1序列网址,2单页',
  `urlpage` text NOT NULL COMMENT '采集地址',
  `pagesize_start` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '页码开始',
  `pagesize_end` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '页码结束',
  `par_num` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '每次增加数',
  `url_contain` char(100) NOT NULL DEFAULT '' COMMENT '网址中必须包含',
  `url_except` char(100) NOT NULL DEFAULT '' COMMENT '网址中不能包含',
  `url_start` char(100) NOT NULL DEFAULT '' COMMENT '网址开始',
  `url_end` char(100) NOT NULL DEFAULT '' COMMENT '网址结束',
  `title_rule` char(100) NOT NULL DEFAULT '' COMMENT '标题采集规则',
  `title_html_rule` text NOT NULL COMMENT '标题过滤规则',
  `time_rule` char(100) NOT NULL DEFAULT '' COMMENT '时间采集规则',
  `time_html_rule` text COMMENT '时间过滤规则',
  `content_rule` char(100) NOT NULL DEFAULT '' COMMENT '内容采集规则',
  `content_html_rule` text COMMENT '内容过滤规则',
  `down_attachment` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否下载图片',
  `watermark` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '图片加水印',
  `coll_order` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '导入顺序',
  PRIMARY KEY (`nodeid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_comment`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_comment`;
CREATE TABLE `yzm_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `siteid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `commentid` char(30) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `userpic` varchar(100) NOT NULL DEFAULT '',
  `inputtime` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` char(15) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '评论状态{0:未审核,1:通过审核}',
  `reply` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '是否为回复',
  PRIMARY KEY (`id`),
  KEY `siteid` (`siteid`),
  KEY `userid` (`userid`),
  KEY `commentid` (`commentid`,`status`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `yzm_comment`
-- -----------------------------
INSERT INTO `yzm_comment` VALUES ('8','0','1_15_22','0','游客','','1699201919','127.0.0.1','测试评论','1','0');
INSERT INTO `yzm_comment` VALUES ('9','0','1_15_22','0','游客','','1699202564','127.0.0.1','dsadasdassdd','1','0');

-- -----------------------------
-- Table structure for `yzm_comment_data`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_comment_data`;
CREATE TABLE `yzm_comment_data` (
  `commentid` char(30) NOT NULL DEFAULT '',
  `siteid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `title` char(255) NOT NULL DEFAULT '',
  `url` varchar(200) NOT NULL DEFAULT '',
  `total` int(8) unsigned NOT NULL DEFAULT '0',
  `catid` smallint(4) unsigned NOT NULL DEFAULT '0',
  `modelid` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`commentid`),
  KEY `siteid` (`siteid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `yzm_comment_data`
-- -----------------------------
INSERT INTO `yzm_comment_data` VALUES ('1_15_22','0','曝高洪波将离开中国足协 记者:主动退赃员工还能受重用吗？','http://localhost:94/kaidian/22.html','2','15','1');

-- -----------------------------
-- Table structure for `yzm_config`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_config`;
CREATE TABLE `yzm_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL DEFAULT '' COMMENT '配置名称',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置类型',
  `title` varchar(60) NOT NULL DEFAULT '' COMMENT '配置说明',
  `value` text NOT NULL COMMENT '配置值',
  `fieldtype` varchar(20) NOT NULL DEFAULT '' COMMENT '字段类型',
  `setting` text COMMENT '字段设置',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `yzm_config`
-- -----------------------------
INSERT INTO `yzm_config` VALUES ('1','site_name','0','站点名称','创淘网','','','1');
INSERT INTO `yzm_config` VALUES ('2','site_url','0','站点根网址','http://localhost:94/','','','1');
INSERT INTO `yzm_config` VALUES ('3','site_keyword','0','站点关键字','淘宝开店,拼多多开店,抖音小店开店,抖音,淘宝,拼多多,电商','','','1');
INSERT INTO `yzm_config` VALUES ('4','site_description','0','站点描述','创淘网 - 专业的电商创业资讯网站，为广大店长提供如何开网店的学习平台。教你如何开网店的方法，步骤以及经验；分享怎么开淘宝店,淘宝开店,拼多多开店以及抖音小店开店运营的信息资讯。','','','1');
INSERT INTO `yzm_config` VALUES ('71','wx','99','微信二维码','/uploads/202310/11/231011042617310.jpg','image','','1');
INSERT INTO `yzm_config` VALUES ('72','qq','99','QQ号码','11523518','textarea','','1');
INSERT INTO `yzm_config` VALUES ('5','site_copyright','0','版权信息','Powered By 趣事网 © 2014-2099','','','1');
INSERT INTO `yzm_config` VALUES ('6','site_filing','0','站点备案号','京ICP备666666号','','','1');
INSERT INTO `yzm_config` VALUES ('7','site_code','0','统计代码','','','','1');
INSERT INTO `yzm_config` VALUES ('8','site_theme','0','站点模板主题','ac','','','1');
INSERT INTO `yzm_config` VALUES ('9','site_logo','0','站点logo','/uploads/202310/11/231011024651376.png','','','1');
INSERT INTO `yzm_config` VALUES ('10','url_mode','0','前台URL模式','1','','','1');
INSERT INTO `yzm_config` VALUES ('11','is_words','0','是否开启前端留言功能','1','','','1');
INSERT INTO `yzm_config` VALUES ('12','upload_maxsize','0','文件上传最大限制','2048','','','1');
INSERT INTO `yzm_config` VALUES ('13','upload_types','0','允许上传附件类型','zip|rar|ppt|doc|xls','','','1');
INSERT INTO `yzm_config` VALUES ('14','upload_image_types','0','允许上传图片类型','png|jpg|jpeg|gif|webp','','','1');
INSERT INTO `yzm_config` VALUES ('15','watermark_enable','0','是否开启图片水印','0','','','1');
INSERT INTO `yzm_config` VALUES ('16','watermark_name','0','水印图片名称','mark.png','','','1');
INSERT INTO `yzm_config` VALUES ('17','watermark_position','0','水印的位置','9','','','1');
INSERT INTO `yzm_config` VALUES ('18','mail_server','1','SMTP服务器','ssl://smtp.qq.com','','','1');
INSERT INTO `yzm_config` VALUES ('19','mail_port','1','SMTP服务器端口','465','','','1');
INSERT INTO `yzm_config` VALUES ('20','mail_from','1','SMTP服务器的用户邮箱','','','','1');
INSERT INTO `yzm_config` VALUES ('21','mail_auth','1','AUTH LOGIN验证','1','','','1');
INSERT INTO `yzm_config` VALUES ('22','mail_user','1','SMTP服务器的用户帐号','','','','1');
INSERT INTO `yzm_config` VALUES ('23','mail_pass','1','SMTP服务器的用户密码','','','','1');
INSERT INTO `yzm_config` VALUES ('24','mail_inbox','1','收件邮箱地址','','','','1');
INSERT INTO `yzm_config` VALUES ('25','admin_log','2','启用后台管理操作日志','0','','','1');
INSERT INTO `yzm_config` VALUES ('26','admin_prohibit_ip','2','禁止登录后台的IP','','','','1');
INSERT INTO `yzm_config` VALUES ('27','prohibit_words','2','屏蔽词','她妈|它妈|他妈|你妈|去死|贱人','','','1');
INSERT INTO `yzm_config` VALUES ('28','comment_check','2','是否开启评论审核','0','','','1');
INSERT INTO `yzm_config` VALUES ('29','comment_tourist','2','是否允许游客评论','1','','','1');
INSERT INTO `yzm_config` VALUES ('30','is_link','2','允许用户申请友情链接','0','','','1');
INSERT INTO `yzm_config` VALUES ('31','member_register','3','是否开启会员注册','0','','','1');
INSERT INTO `yzm_config` VALUES ('32','member_email','3','新会员注册是否需要邮件验证','0','','','1');
INSERT INTO `yzm_config` VALUES ('33','member_check','3','新会员注册是否需要管理员审核','0','','','1');
INSERT INTO `yzm_config` VALUES ('34','member_point','3','新会员默认积分','0','','','1');
INSERT INTO `yzm_config` VALUES ('35','member_yzm','3','是否开启会员登录验证码','1','','','1');
INSERT INTO `yzm_config` VALUES ('36','rmb_point_rate','3','1元人民币购买积分数量','10','','','1');
INSERT INTO `yzm_config` VALUES ('37','login_point','3','每日登录奖励积分','1','','','1');
INSERT INTO `yzm_config` VALUES ('38','comment_point','3','发布评论奖励积分','1','','','1');
INSERT INTO `yzm_config` VALUES ('39','publish_point','3','投稿奖励积分','3','','','1');
INSERT INTO `yzm_config` VALUES ('40','qq_app_id','3','QQ App ID','','','','1');
INSERT INTO `yzm_config` VALUES ('41','qq_app_key','3','QQ App key','','','','1');
INSERT INTO `yzm_config` VALUES ('42','weibo_key','4','微博登录App Key','','','','1');
INSERT INTO `yzm_config` VALUES ('43','weibo_secret','4','微博登录App Secret','','','','1');
INSERT INTO `yzm_config` VALUES ('44','wx_appid','4','微信开发者ID','','','','1');
INSERT INTO `yzm_config` VALUES ('45','wx_secret','4','微信开发者密码','','','','1');
INSERT INTO `yzm_config` VALUES ('46','wx_token','4','微信Token签名','','','','1');
INSERT INTO `yzm_config` VALUES ('47','wx_encodingaeskey','4','微信EncodingAESKey','','','','1');
INSERT INTO `yzm_config` VALUES ('48','wx_relation_model','4','微信关联模型','article','','','1');
INSERT INTO `yzm_config` VALUES ('49','baidu_push_token','0','百度推送token','','','','1');
INSERT INTO `yzm_config` VALUES ('50','thumb_width','2','缩略图默认宽度','500','','','1');
INSERT INTO `yzm_config` VALUES ('51','thumb_height','2','缩略图默认高度','300','','','1');
INSERT INTO `yzm_config` VALUES ('52','site_seo_division','0','站点标题分隔符','_创业网','','','1');
INSERT INTO `yzm_config` VALUES ('53','keyword_link','2','是否启用关键字替换','0','','','1');
INSERT INTO `yzm_config` VALUES ('54','keyword_replacenum','2','关键字替换次数','1','','','1');
INSERT INTO `yzm_config` VALUES ('55','error_log_save','2','是否保存系统错误日志','1','','','1');
INSERT INTO `yzm_config` VALUES ('56','comment_code','2','是否开启评论验证码','1','','','1');
INSERT INTO `yzm_config` VALUES ('57','site_wap_open','0','是否启用手机站点','0','','','1');
INSERT INTO `yzm_config` VALUES ('58','site_wap_theme','0','WAP端模板风格','default','','','1');
INSERT INTO `yzm_config` VALUES ('59','member_theme','3','会员中心模板风格','default','','','1');
INSERT INTO `yzm_config` VALUES ('60','att_relation_content','1','是否开启内容附件关联','1','','','1');
INSERT INTO `yzm_config` VALUES ('61','site_seo_suffix','0','站点SEO后缀','_创业网','','','1');
INSERT INTO `yzm_config` VALUES ('62','site_security_number','0','公安备案号','','',' ','1');
INSERT INTO `yzm_config` VALUES ('63','words_code','3','是否开启留言验证码','1','','','1');
INSERT INTO `yzm_config` VALUES ('64','watermark_minwidth','2','添加水印最小宽度','300','','','1');
INSERT INTO `yzm_config` VALUES ('65','watermark_minheight','2','添加水印最小高度','300','','','1');
INSERT INTO `yzm_config` VALUES ('66','auto_down_imag','2','自动下载远程图片','1','','','1');
INSERT INTO `yzm_config` VALUES ('67','down_ignore_domain','2','下载远程图片忽略的域名','','','','1');
INSERT INTO `yzm_config` VALUES ('68','content_click_random','2','内容默认点击量','0','','','1');
INSERT INTO `yzm_config` VALUES ('69','blacklist_ip','3',' 前端IP黑名单','','','','1');
INSERT INTO `yzm_config` VALUES ('70','email','99','邮箱','a11523518@126.com','textarea','','1');

-- -----------------------------
-- Table structure for `yzm_download`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_download`;
CREATE TABLE `yzm_download` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `nickname` varchar(30) NOT NULL DEFAULT '',
  `title` varchar(180) NOT NULL DEFAULT '',
  `color` char(9) NOT NULL DEFAULT '',
  `inputtime` int(10) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0',
  `keywords` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `click` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  `copyfrom` varchar(50) NOT NULL DEFAULT '',
  `thumb` varchar(150) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL DEFAULT '',
  `flag` varchar(12) NOT NULL DEFAULT '' COMMENT '1置顶,2头条,3特荐,4推荐,5热点,6幻灯,7跳转',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `issystem` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `listorder` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `groupids_view` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '阅读权限',
  `readpoint` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '阅读收费',
  `paytype` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '收费类型',
  `is_push` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否百度推送',
  `down_url` varchar(200) NOT NULL DEFAULT '' COMMENT '下载地址',
  `copytype` varchar(30) NOT NULL DEFAULT '' COMMENT '授权形式',
  `systems` varchar(100) NOT NULL DEFAULT '' COMMENT '平台',
  `language` varchar(30) NOT NULL DEFAULT '' COMMENT '语言',
  `version` varchar(30) NOT NULL DEFAULT '' COMMENT '版本',
  `filesize` varchar(10) NOT NULL DEFAULT '' COMMENT '文件大小',
  `classtype` varchar(30) NOT NULL DEFAULT '' COMMENT '软件类型',
  `stars` varchar(10) NOT NULL DEFAULT '' COMMENT '评分等级',
  PRIMARY KEY (`id`),
  KEY `status` (`status`,`listorder`),
  KEY `catid` (`status`,`catid`),
  KEY `userid` (`status`,`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_favorite`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_favorite`;
CREATE TABLE `yzm_favorite` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `title` char(100) NOT NULL DEFAULT '',
  `url` char(100) NOT NULL DEFAULT '',
  `inputtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_guestbook`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_guestbook`;
CREATE TABLE `yzm_guestbook` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `siteid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL DEFAULT '' COMMENT '主题',
  `booktime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '名字',
  `email` varchar(40) NOT NULL DEFAULT '' COMMENT '留言人电子邮箱',
  `phone` varchar(11) NOT NULL DEFAULT '' COMMENT '留言人电话',
  `qq` varchar(11) NOT NULL DEFAULT '' COMMENT '留言人qq',
  `address` varchar(100) NOT NULL DEFAULT '' COMMENT '留言人地址',
  `bookmsg` text NOT NULL COMMENT '内容',
  `ip` varchar(20) NOT NULL DEFAULT '' COMMENT 'ip地址',
  `ischeck` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否审核',
  `isread` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否读过',
  `ispc` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1电脑,0手机',
  `replyid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '回复的id',
  PRIMARY KEY (`id`),
  KEY `index_booktime` (`booktime`),
  KEY `index_replyid` (`replyid`),
  KEY `index_ischeck` (`siteid`,`ischeck`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_keyword_link`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_keyword_link`;
CREATE TABLE `yzm_keyword_link` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `keyword` varchar(36) NOT NULL DEFAULT '' COMMENT '关键字',
  `url` varchar(100) NOT NULL DEFAULT '' COMMENT '地址',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_link`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_link`;
CREATE TABLE `yzm_link` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `siteid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `typeid` smallint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1首页,2列表页,3内容页',
  `linktype` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0:文字链接,1:logo链接',
  `name` varchar(50) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `logo` varchar(255) NOT NULL DEFAULT '',
  `msg` text NOT NULL,
  `username` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(40) NOT NULL DEFAULT '',
  `listorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0未通过,1正常,2未审核',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `siteid` (`siteid`,`status`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_member`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_member`;
CREATE TABLE `yzm_member` (
  `userid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `password` char(32) NOT NULL DEFAULT '',
  `regdate` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdate` int(10) unsigned NOT NULL DEFAULT '0',
  `regip` char(15) NOT NULL DEFAULT '',
  `lastip` char(15) NOT NULL DEFAULT '',
  `loginnum` smallint(5) unsigned NOT NULL DEFAULT '0',
  `email` char(32) NOT NULL DEFAULT '',
  `groupid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `amount` decimal(8,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '金钱',
  `experience` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '经验',
  `point` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '积分',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0待审核,1正常,2锁定,3拒绝',
  `vip` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `overduedate` int(10) unsigned NOT NULL DEFAULT '0',
  `email_status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `problem` varchar(39) NOT NULL DEFAULT '' COMMENT '安全问题',
  `answer` varchar(30) NOT NULL DEFAULT '' COMMENT '答案',
  PRIMARY KEY (`userid`),
  UNIQUE KEY `username` (`username`),
  KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_member_authorization`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_member_authorization`;
CREATE TABLE `yzm_member_authorization` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `authname` varchar(10) NOT NULL DEFAULT '',
  `token` varchar(60) NOT NULL DEFAULT '',
  `userinfo` varchar(255) NOT NULL DEFAULT '',
  `inputtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `authindex` (`authname`,`token`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_member_detail`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_member_detail`;
CREATE TABLE `yzm_member_detail` (
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `sex` varchar(6) NOT NULL DEFAULT '',
  `realname` varchar(30) NOT NULL DEFAULT '' COMMENT '真实姓名',
  `nickname` varchar(30) NOT NULL DEFAULT '',
  `qq` char(11) NOT NULL DEFAULT '',
  `mobile` char(11) NOT NULL DEFAULT '',
  `phone` char(10) NOT NULL DEFAULT '',
  `userpic` varchar(100) NOT NULL DEFAULT '',
  `birthday` char(10) NOT NULL DEFAULT '' COMMENT '生日',
  `industry` varchar(60) NOT NULL DEFAULT '' COMMENT '行业',
  `area` varchar(60) NOT NULL DEFAULT '',
  `motto` varchar(210) NOT NULL DEFAULT '' COMMENT '个性签名',
  `introduce` text COMMENT '个人简介',
  `guest` int(10) unsigned NOT NULL DEFAULT '0',
  `fans` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '粉丝数',
  UNIQUE KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_member_follow`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_member_follow`;
CREATE TABLE `yzm_member_follow` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `followid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '被关注者id',
  `followname` varchar(30) NOT NULL DEFAULT '' COMMENT '被关注者用户名',
  `inputtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_member_group`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_member_group`;
CREATE TABLE `yzm_member_group` (
  `groupid` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(21) NOT NULL DEFAULT '',
  `experience` smallint(6) unsigned NOT NULL DEFAULT '0',
  `icon` char(30) NOT NULL DEFAULT '' COMMENT '图标',
  `authority` char(12) NOT NULL DEFAULT '' COMMENT '1短消息,2发表评论,3发表内容',
  `max_amount` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '每日最大投稿量',
  `description` char(100) NOT NULL DEFAULT '',
  `is_system` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '系统内置',
  PRIMARY KEY (`groupid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `yzm_member_group`
-- -----------------------------
INSERT INTO `yzm_member_group` VALUES ('1','初来乍到','50','icon1.png','1,2','1','初来乍到组','1');
INSERT INTO `yzm_member_group` VALUES ('2','新手上路','100','icon2.png','1,2','2','新手上路组','1');
INSERT INTO `yzm_member_group` VALUES ('3','中级会员','200','icon3.png','1,2,3','3','中级会员组','1');
INSERT INTO `yzm_member_group` VALUES ('4','高级会员','300','icon4.png','1,2,3','4','高级会员组','1');
INSERT INTO `yzm_member_group` VALUES ('5','金牌会员','500','icon5.png','1,2,3,4','5','金牌会员组','1');

-- -----------------------------
-- Table structure for `yzm_member_guest`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_member_guest`;
CREATE TABLE `yzm_member_guest` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `space_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `guest_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `guest_name` varchar(30) NOT NULL DEFAULT '',
  `guest_pic` varchar(100) NOT NULL DEFAULT '',
  `inputtime` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `space_id` (`space_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_menu`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_menu`;
CREATE TABLE `yzm_menu` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(40) NOT NULL DEFAULT '',
  `parentid` smallint(6) NOT NULL DEFAULT '0',
  `m` char(20) NOT NULL DEFAULT '',
  `c` char(20) NOT NULL DEFAULT '',
  `a` char(30) NOT NULL DEFAULT '',
  `data` char(100) NOT NULL DEFAULT '',
  `listorder` smallint(6) unsigned NOT NULL DEFAULT '0',
  `display` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `listorder` (`listorder`),
  KEY `parentid` (`parentid`),
  KEY `module` (`m`,`c`,`a`)
) ENGINE=MyISAM AUTO_INCREMENT=327 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `yzm_menu`
-- -----------------------------
INSERT INTO `yzm_menu` VALUES ('1','内容管理','0','admin','content','top','yzm-iconneirong','1','1');
INSERT INTO `yzm_menu` VALUES ('2','会员管理','0','member','member','top','yzm-iconyonghu','2','1');
INSERT INTO `yzm_menu` VALUES ('3','模块管理','0','admin','module','top','yzm-icondaohang','3','1');
INSERT INTO `yzm_menu` VALUES ('4','管理员管理','0','admin','admin_manage','top','yzm-iconguanliyuan','4','1');
INSERT INTO `yzm_menu` VALUES ('5','个人信息','0','admin','admin_manage','top','yzm-iconrizhi','5','0');
INSERT INTO `yzm_menu` VALUES ('6','系统管理','0','admin','system_manage','top','yzm-iconshezhi','6','1');
INSERT INTO `yzm_menu` VALUES ('7','数据管理','0','admin','database','top','yzm-iconshujuku','7','1');
INSERT INTO `yzm_menu` VALUES ('8','稿件管理','1','admin','admin_content','init','','13','1');
INSERT INTO `yzm_menu` VALUES ('9','稿件浏览','8','admin','admin_content','public_preview','','0','0');
INSERT INTO `yzm_menu` VALUES ('10','稿件删除','8','admin','admin_content','del','','0','0');
INSERT INTO `yzm_menu` VALUES ('11','通过审核','8','admin','admin_content','adopt','','0','0');
INSERT INTO `yzm_menu` VALUES ('12','退稿','8','admin','admin_content','rejection','','0','0');
INSERT INTO `yzm_menu` VALUES ('13','后台操作日志','6','admin','admin_log','init','','66','1');
INSERT INTO `yzm_menu` VALUES ('14','操作日志删除','13','admin','admin_log','del_log','','0','0');
INSERT INTO `yzm_menu` VALUES ('15','操作日志搜索','13','admin','admin_log','search_log','','0','0');
INSERT INTO `yzm_menu` VALUES ('16','后台登录日志','6','admin','admin_log','admin_login_log_list','','67','1');
INSERT INTO `yzm_menu` VALUES ('17','登录日志删除','16','admin','admin_log','del_login_log','','0','0');
INSERT INTO `yzm_menu` VALUES ('18','管理员管理','4','admin','admin_manage','init','','0','1');
INSERT INTO `yzm_menu` VALUES ('19','删除管理员','18','admin','admin_manage','delete','','0','0');
INSERT INTO `yzm_menu` VALUES ('20','添加管理员','18','admin','admin_manage','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('21','编辑管理员','18','admin','admin_manage','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('22','修改资料','18','admin','admin_manage','public_edit_info','','0','1');
INSERT INTO `yzm_menu` VALUES ('23','修改密码','18','admin','admin_manage','public_edit_pwd','','0','1');
INSERT INTO `yzm_menu` VALUES ('24','栏目管理','1','admin','category','init','','11','1');
INSERT INTO `yzm_menu` VALUES ('25','排序栏目','24','admin','category','order','','0','0');
INSERT INTO `yzm_menu` VALUES ('26','删除栏目','24','admin','category','delete','','0','0');
INSERT INTO `yzm_menu` VALUES ('27','添加栏目','24','admin','category','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('28','编辑栏目','24','admin','category','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('29','编辑单页内容','24','admin','category','page_content','','0','0');
INSERT INTO `yzm_menu` VALUES ('30','内容管理','1','admin','content','init','','10','1');
INSERT INTO `yzm_menu` VALUES ('31','内容搜索','30','admin','content','search','','0','0');
INSERT INTO `yzm_menu` VALUES ('32','添加内容','30','admin','content','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('33','修改内容','30','admin','content','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('34','删除内容','30','admin','content','del','','0','0');
INSERT INTO `yzm_menu` VALUES ('35','数据备份','7','admin','database','init','','70','1');
INSERT INTO `yzm_menu` VALUES ('36','数据还原','7','admin','database','databack_list','','71','1');
INSERT INTO `yzm_menu` VALUES ('37','优化表','35','admin','database','public_optimize','','0','0');
INSERT INTO `yzm_menu` VALUES ('38','修复表','35','admin','database','public_repair','','0','0');
INSERT INTO `yzm_menu` VALUES ('39','备份文件删除','36','admin','database','databack_del','','0','0');
INSERT INTO `yzm_menu` VALUES ('40','备份文件下载','36','admin','database','databack_down','','0','0');
INSERT INTO `yzm_menu` VALUES ('41','数据导入','36','admin','database','import','','0','0');
INSERT INTO `yzm_menu` VALUES ('42','字段管理','54','admin','model_field','init','','0','1');
INSERT INTO `yzm_menu` VALUES ('43','添加字段','42','admin','model_field','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('44','修改字段','42','admin','model_field','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('45','删除字段','42','admin','model_field','delete','','0','0');
INSERT INTO `yzm_menu` VALUES ('46','排序字段','42','admin','model_field','order','','0','0');
INSERT INTO `yzm_menu` VALUES ('47','模块管理','3','admin','module','init','','0','1');
INSERT INTO `yzm_menu` VALUES ('48','模块安装','47','admin','module','install','','0','0');
INSERT INTO `yzm_menu` VALUES ('49','模块卸载','47','admin','module','uninstall','','0','0');
INSERT INTO `yzm_menu` VALUES ('50','角色管理','4','admin','role','init','','0','1');
INSERT INTO `yzm_menu` VALUES ('51','删除角色','50','admin','role','delete','','0','0');
INSERT INTO `yzm_menu` VALUES ('52','添加角色','50','admin','role','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('53','编辑角色','50','admin','role','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('54','模型管理','1','admin','sitemodel','init','','15','1');
INSERT INTO `yzm_menu` VALUES ('55','删除模型','54','admin','sitemodel','delete','','0','0');
INSERT INTO `yzm_menu` VALUES ('56','添加模型','54','admin','sitemodel','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('57','编辑模型','54','admin','sitemodel','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('58','系统设置','6','admin','system_manage','init','','60','1');
INSERT INTO `yzm_menu` VALUES ('59','会员中心设置','2','admin','system_manage','member_set','','26','1');
INSERT INTO `yzm_menu` VALUES ('60','屏蔽词管理','6','admin','system_manage','prohibit_words','','63','1');
INSERT INTO `yzm_menu` VALUES ('61','自定义配置','6','admin','system_manage','user_config_list','','62','1');
INSERT INTO `yzm_menu` VALUES ('62','添加配置','61','admin','system_manage','user_config_add','','0','0');
INSERT INTO `yzm_menu` VALUES ('63','配置编辑','61','admin','system_manage','user_config_edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('64','配置删除','61','admin','system_manage','user_config_del','','0','0');
INSERT INTO `yzm_menu` VALUES ('65','TAG管理','1','admin','tag','init','','16','1');
INSERT INTO `yzm_menu` VALUES ('66','添加TAG','65','admin','tag','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('67','编辑TAG','65','admin','tag','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('68','删除TAG','65','admin','tag','del','','0','0');
INSERT INTO `yzm_menu` VALUES ('69','批量更新URL','1','admin','update_urls','init','','17','1');
INSERT INTO `yzm_menu` VALUES ('70','附件管理','1','attachment','index','init','','14','1');
INSERT INTO `yzm_menu` VALUES ('71','附件搜索','70','attachment','index','search_list','','0','0');
INSERT INTO `yzm_menu` VALUES ('72','附件浏览','70','attachment','index','public_att_view','','0','0');
INSERT INTO `yzm_menu` VALUES ('73','删除单个附件','70','attachment','index','del_one','','0','0');
INSERT INTO `yzm_menu` VALUES ('74','删除多个附件','70','attachment','index','del','','0','0');
INSERT INTO `yzm_menu` VALUES ('75','评论管理','1','comment','comment','init','','12','1');
INSERT INTO `yzm_menu` VALUES ('76','评论搜索','75','comment','comment','search','','0','0');
INSERT INTO `yzm_menu` VALUES ('77','删除评论','75','comment','comment','del','','0','0');
INSERT INTO `yzm_menu` VALUES ('78','评论审核','75','comment','comment','adopt','','0','0');
INSERT INTO `yzm_menu` VALUES ('79','留言管理','3','guestbook','guestbook','init','','1','1');
INSERT INTO `yzm_menu` VALUES ('80','查看及回复留言','79','guestbook','guestbook','read','','0','0');
INSERT INTO `yzm_menu` VALUES ('81','留言审核','79','guestbook','guestbook','toggle','','0','0');
INSERT INTO `yzm_menu` VALUES ('82','删除留言','79','guestbook','guestbook','del','','0','0');
INSERT INTO `yzm_menu` VALUES ('88','会员管理','2','member','member','init','','20','1');
INSERT INTO `yzm_menu` VALUES ('89','会员搜索','88','member','member','search','','0','0');
INSERT INTO `yzm_menu` VALUES ('90','添加会员','88','member','member','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('91','修改会员信息','88','member','member','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('92','修改会员密码','88','member','member','password','','0','0');
INSERT INTO `yzm_menu` VALUES ('93','删除会员','88','member','member','del','','0','0');
INSERT INTO `yzm_menu` VALUES ('94','审核会员','2','member','member','check','','21','1');
INSERT INTO `yzm_menu` VALUES ('95','通过审核','94','member','member','adopt','','0','0');
INSERT INTO `yzm_menu` VALUES ('96','锁定用户','88','member','member','lock','','0','0');
INSERT INTO `yzm_menu` VALUES ('97','解锁用户','88','member','member','unlock','','0','0');
INSERT INTO `yzm_menu` VALUES ('98','账单管理','2','member','member','pay','','22','1');
INSERT INTO `yzm_menu` VALUES ('99','入账记录搜索','98','member','member','pay_search','','0','0');
INSERT INTO `yzm_menu` VALUES ('100','入账记录删除','98','member','member','pay_del','','0','0');
INSERT INTO `yzm_menu` VALUES ('101','消费记录','98','member','member','pay_spend','','0','0');
INSERT INTO `yzm_menu` VALUES ('102','消费记录搜索','98','member','member','pay_spend_search','','0','0');
INSERT INTO `yzm_menu` VALUES ('103','消费记录删除','98','member','member','pay_spend_del','','0','0');
INSERT INTO `yzm_menu` VALUES ('104','会员组管理','2','member','member_group','init','','25','1');
INSERT INTO `yzm_menu` VALUES ('105','添加组别','104','member','member_group','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('106','修改组别','104','member','member_group','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('107','删除组别','104','member','member_group','del','','0','0');
INSERT INTO `yzm_menu` VALUES ('108','消息管理','2','member','member_message','init','','23','1');
INSERT INTO `yzm_menu` VALUES ('109','消息搜索','108','member','member_message','search','','0','0');
INSERT INTO `yzm_menu` VALUES ('110','删除消息','108','member','member_message','del','','0','0');
INSERT INTO `yzm_menu` VALUES ('111','发送单个消息','108','member','member_message','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('112','群发消息','2','member','member_message','messages_list','','23','1');
INSERT INTO `yzm_menu` VALUES ('113','新建群发','112','member','member_message','add_messages','','0','0');
INSERT INTO `yzm_menu` VALUES ('114','删除群发消息','112','member','member_message','del_messages','','0','0');
INSERT INTO `yzm_menu` VALUES ('115','权限管理','50','admin','role','role_priv','','0','0');
INSERT INTO `yzm_menu` VALUES ('116','后台菜单管理','6','admin','menu','init','','64','1');
INSERT INTO `yzm_menu` VALUES ('117','删除菜单','116','admin','menu','delete','','0','0');
INSERT INTO `yzm_menu` VALUES ('118','添加菜单','116','admin','menu','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('119','编辑菜单','116','admin','menu','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('120','菜单排序','116','admin','menu','order','','0','0');
INSERT INTO `yzm_menu` VALUES ('121','邮箱配置','6','admin','system_manage','init','tab=4','61','1');
INSERT INTO `yzm_menu` VALUES ('122','修改资料','5','admin','admin_manage','public_edit_info','','51','1');
INSERT INTO `yzm_menu` VALUES ('123','修改密码','5','admin','admin_manage','public_edit_pwd','','52','1');
INSERT INTO `yzm_menu` VALUES ('134','友情链接管理','3','link','link','init','','6','1');
INSERT INTO `yzm_menu` VALUES ('135','添加友情链接','134','link','link','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('136','修改友情链接','134','link','link','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('137','删除单个友情链接','134','link','link','del_one','','0','0');
INSERT INTO `yzm_menu` VALUES ('138','删除多个友情链接','134','link','link','del','','0','0');
INSERT INTO `yzm_menu` VALUES ('139','URL规则管理','6','admin','urlrule','init','','65','1');
INSERT INTO `yzm_menu` VALUES ('140','添加URL规则','139','admin','urlrule','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('141','删除URL规则','139','admin','urlrule','del','','0','0');
INSERT INTO `yzm_menu` VALUES ('142','编辑URL规则','139','admin','urlrule','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('143','移动分类','30','admin','content','remove','','0','0');
INSERT INTO `yzm_menu` VALUES ('144','SQL命令行','6','admin','sql','init','','63','1');
INSERT INTO `yzm_menu` VALUES ('145','提交SQL命令','144','admin','sql','do_sql','','0','0');
INSERT INTO `yzm_menu` VALUES ('156','轮播图管理','3','banner','banner','init','','1','1');
INSERT INTO `yzm_menu` VALUES ('157','添加轮播','156','banner','banner','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('158','修改轮播','156','banner','banner','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('159','删除轮播','156','banner','banner','del','','0','0');
INSERT INTO `yzm_menu` VALUES ('160','添加轮播分类','156','banner','banner','cat_add','','0','0');
INSERT INTO `yzm_menu` VALUES ('161','管理轮播分类','156','banner','banner','cat_manage','','0','0');
INSERT INTO `yzm_menu` VALUES ('162','会员统计','2','member','member','member_count','','24','1');
INSERT INTO `yzm_menu` VALUES ('165','采集管理','3','collection','collection_content','init','','0','1');
INSERT INTO `yzm_menu` VALUES ('166','添加采集节点','165','collection','collection_content','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('167','编辑采集节点','165','collection','collection_content','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('168','删除采集节点','165','collection','collection_content','del','','0','0');
INSERT INTO `yzm_menu` VALUES ('169','采集测试','165','collection','collection_content','collection_test','','0','0');
INSERT INTO `yzm_menu` VALUES ('170','采集网址','165','collection','collection_content','collection_list_url','','0','0');
INSERT INTO `yzm_menu` VALUES ('171','采集内容','165','collection','collection_content','collection_article_content','','0','0');
INSERT INTO `yzm_menu` VALUES ('172','内容导入','165','collection','collection_content','collection_content_import','','0','0');
INSERT INTO `yzm_menu` VALUES ('173','新建内容发布方案','165','collection','collection_content','create_programme','','0','0');
INSERT INTO `yzm_menu` VALUES ('174','采集列表','165','collection','collection_content','collection_list','','0','0');
INSERT INTO `yzm_menu` VALUES ('175','删除采集列表','165','collection','collection_content','collection_list_del','','0','0');
INSERT INTO `yzm_menu` VALUES ('200','微信管理','0','wechat','wechat','top','yzm-iconweixin','3','1');
INSERT INTO `yzm_menu` VALUES ('201','微信配置','200','wechat','config','init','','0','1');
INSERT INTO `yzm_menu` VALUES ('202','保存配置','201','wechat','config','save','','0','0');
INSERT INTO `yzm_menu` VALUES ('203','微信用户','200','wechat','user','init','','0','1');
INSERT INTO `yzm_menu` VALUES ('204','关注者搜索','203','wechat','user','search','','0','0');
INSERT INTO `yzm_menu` VALUES ('205','获取分组名称','203','wechat','user','get_groupname','','0','0');
INSERT INTO `yzm_menu` VALUES ('206','同步微信服务器用户','203','wechat','user','synchronization','','0','0');
INSERT INTO `yzm_menu` VALUES ('207','批量移动用户分组','203','wechat','user','move_user_group','','0','0');
INSERT INTO `yzm_menu` VALUES ('208','设置用户备注','203','wechat','user','set_userremark','','0','0');
INSERT INTO `yzm_menu` VALUES ('209','查询用户所在组','203','wechat','user','select_user_group','','0','0');
INSERT INTO `yzm_menu` VALUES ('210','分组管理','200','wechat','group','init','','0','1');
INSERT INTO `yzm_menu` VALUES ('211','创建分组','210','wechat','group','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('212','修改分组','210','wechat','group','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('213','删除分组','210','wechat','group','delete','','0','0');
INSERT INTO `yzm_menu` VALUES ('214','查询所有分组','210','wechat','group','select_group','','0','0');
INSERT INTO `yzm_menu` VALUES ('215','微信菜单','200','wechat','menu','init','','0','1');
INSERT INTO `yzm_menu` VALUES ('216','添加菜单','215','wechat','menu','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('217','编辑菜单','215','wechat','menu','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('218','删除菜单','215','wechat','menu','delete','','0','0');
INSERT INTO `yzm_menu` VALUES ('219','菜单排序','215','wechat','menu','order','','0','0');
INSERT INTO `yzm_menu` VALUES ('220','创建菜单提交微信','215','wechat','menu','create_menu','','0','0');
INSERT INTO `yzm_menu` VALUES ('221','查询远程菜单','215','wechat','menu','select_menu','','0','0');
INSERT INTO `yzm_menu` VALUES ('222','删除所有菜单提交微信','215','wechat','menu','delete_menu','','0','0');
INSERT INTO `yzm_menu` VALUES ('223','消息回复','200','wechat','reply','init','','0','1');
INSERT INTO `yzm_menu` VALUES ('224','自动回复/关注回复','223','wechat','reply','reply_list','','0','0');
INSERT INTO `yzm_menu` VALUES ('225','添加关键字回复','223','wechat','reply','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('226','修改关键字回复','223','wechat','reply','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('227','删除关键字回复','223','wechat','reply','del','','0','0');
INSERT INTO `yzm_menu` VALUES ('228','选择文章','223','wechat','reply','select_article','','0','0');
INSERT INTO `yzm_menu` VALUES ('229','消息管理','200','wechat','message','init','','0','1');
INSERT INTO `yzm_menu` VALUES ('230','用户发送信息','229','wechat','message','send_message','','0','0');
INSERT INTO `yzm_menu` VALUES ('231','标识已读','229','wechat','message','read','','0','0');
INSERT INTO `yzm_menu` VALUES ('232','删除消息','229','wechat','message','del','','0','0');
INSERT INTO `yzm_menu` VALUES ('233','微信场景','200','wechat','scan','init','','0','1');
INSERT INTO `yzm_menu` VALUES ('234','添加场景','233','wechat','scan','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('235','编辑场景','233','wechat','scan','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('236','删除场景','233','wechat','scan','del','','0','0');
INSERT INTO `yzm_menu` VALUES ('237','素材管理','200','wechat','material','init','','0','1');
INSERT INTO `yzm_menu` VALUES ('238','素材搜索','237','wechat','material','search','','0','0');
INSERT INTO `yzm_menu` VALUES ('239','添加素材','237','wechat','material','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('240','添加图文素材','237','wechat','material','add_news','','0','0');
INSERT INTO `yzm_menu` VALUES ('241','删除素材','237','wechat','material','delete','','0','0');
INSERT INTO `yzm_menu` VALUES ('242','选择缩略图','237','wechat','material','select_thumb','','0','0');
INSERT INTO `yzm_menu` VALUES ('243','获取永久素材列表','237','wechat','material','get_material_list','','0','0');
INSERT INTO `yzm_menu` VALUES ('244','高级群发','200','wechat','mass','init','','0','1');
INSERT INTO `yzm_menu` VALUES ('245','新建群发','244','wechat','mass','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('246','查询群发状态','244','wechat','mass','select_status','','0','0');
INSERT INTO `yzm_menu` VALUES ('247','删除群发','244','wechat','mass','del','','0','0');
INSERT INTO `yzm_menu` VALUES ('248','选择素材','244','wechat','mass','select_material','','0','0');
INSERT INTO `yzm_menu` VALUES ('249','选择用户','244','wechat','mass','select_user','','0','0');
INSERT INTO `yzm_menu` VALUES ('250','自定义表单','3','diyform','diyform','init','','2','1');
INSERT INTO `yzm_menu` VALUES ('251','添加表单','250','diyform','diyform','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('252','编辑表单','250','diyform','diyform','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('253','删除表单','250','diyform','diyform','del','','0','0');
INSERT INTO `yzm_menu` VALUES ('254','字段列表','250','diyform','diyform_field','init','','0','0');
INSERT INTO `yzm_menu` VALUES ('255','添加字段','254','diyform','diyform_field','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('256','修改字段','254','diyform','diyform_field','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('257','删除字段','254','diyform','diyform_field','delete','','0','0');
INSERT INTO `yzm_menu` VALUES ('258','排序排序','254','diyform','diyform_field','order','','0','0');
INSERT INTO `yzm_menu` VALUES ('259','表单信息列表','250','diyform','diyform_info','init','','0','0');
INSERT INTO `yzm_menu` VALUES ('260','查看表单信息','259','diyform','diyform_info','view','','0','0');
INSERT INTO `yzm_menu` VALUES ('261','删除表单信息','259','diyform','diyform_info','del','','0','0');
INSERT INTO `yzm_menu` VALUES ('262','广告管理','3','adver','adver','init','','0','1');
INSERT INTO `yzm_menu` VALUES ('263','添加广告','262','adver','adver','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('264','修改广告','262','adver','adver','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('265','删除广告','262','adver','adver','del','','0','0');
INSERT INTO `yzm_menu` VALUES ('266','网站地图','1','admin','sitemap','init','','16','1');
INSERT INTO `yzm_menu` VALUES ('267','生成地图','266','admin','sitemap','make_sitemap','','0','0');
INSERT INTO `yzm_menu` VALUES ('268','导出模型','54','admin','sitemodel','import','','0','0');
INSERT INTO `yzm_menu` VALUES ('269','导入模型','54','admin','sitemodel','export','','0','0');
INSERT INTO `yzm_menu` VALUES ('270','导出配置','61','admin','system_manage','user_config_export','','0','0');
INSERT INTO `yzm_menu` VALUES ('271','导入配置','61','admin','system_manage','user_config_import','','0','0');
INSERT INTO `yzm_menu` VALUES ('283','支付模块','3','pay','pay','init','','0','1');
INSERT INTO `yzm_menu` VALUES ('284','支付配置','283','pay','pay','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('285','订单管理','2','member','order','init','','22','1');
INSERT INTO `yzm_menu` VALUES ('286','订单搜索','285','member','order','order_search','','0','0');
INSERT INTO `yzm_menu` VALUES ('287','订单改价','285','member','order','change_price','','0','0');
INSERT INTO `yzm_menu` VALUES ('288','订单删除','285','member','order','del','','0','0');
INSERT INTO `yzm_menu` VALUES ('289','订单详情','285','member','order','order_details','','0','0');
INSERT INTO `yzm_menu` VALUES ('290','推送至百度','30','admin','content','baidu_push','','0','0');
INSERT INTO `yzm_menu` VALUES ('291','内容属性变更','30','admin','content','attribute_operation','','0','0');
INSERT INTO `yzm_menu` VALUES ('292','更改model','69','admin','update_urls','change_model','','0','0');
INSERT INTO `yzm_menu` VALUES ('293','更新栏目URL','69','admin','update_urls','update_category_url','','0','0');
INSERT INTO `yzm_menu` VALUES ('294','更新内容页URL','69','admin','update_urls','update_content_url','','0','0');
INSERT INTO `yzm_menu` VALUES ('295','留言搜索','79','guestbook','guestbook','search','','0','0');
INSERT INTO `yzm_menu` VALUES ('296','内容关键字','3','admin','keyword_link','init','','1','1');
INSERT INTO `yzm_menu` VALUES ('297','添加关键字','296','admin','keyword_link','add','','0','0');
INSERT INTO `yzm_menu` VALUES ('298','编辑关键字','296','admin','keyword_link','edit','','0','0');
INSERT INTO `yzm_menu` VALUES ('299','删除关键字','296','admin','keyword_link','del','','0','0');
INSERT INTO `yzm_menu` VALUES ('300','应用商店','3','admin','store','init','','0','1');
INSERT INTO `yzm_menu` VALUES ('301','批量添加栏目','24','admin','category','adds','','0','0');
INSERT INTO `yzm_menu` VALUES ('302','内容状态变更','30','admin','content','status_operation','','0','0');
INSERT INTO `yzm_menu` VALUES ('303','关联内容','65','admin','tag','content','','0','0');
INSERT INTO `yzm_menu` VALUES ('304','加入/移除Tag','65','admin','tag','content_oper','','0','0');
INSERT INTO `yzm_menu` VALUES ('305','删除地图','266','admin','sitemap','delete','','0','0');
INSERT INTO `yzm_menu` VALUES ('306','保存配置','58','admin','system_manage','save','','0','0');
INSERT INTO `yzm_menu` VALUES ('307','立即备份','35','admin','database','export_list','','0','0');
INSERT INTO `yzm_menu` VALUES ('308','复制内容','30','admin','content','copy','','0','0');
INSERT INTO `yzm_menu` VALUES ('309','在线充值','88','member','member','recharge','','0','0');
INSERT INTO `yzm_menu` VALUES ('310','登录到任意会员中心','88','member','member','login_user','','0','0');
INSERT INTO `yzm_menu` VALUES ('311','友情链接排序','134','link','link','order','','0','0');
INSERT INTO `yzm_menu` VALUES ('312','友情链接审核','134','link','link','adopt','','0','0');
INSERT INTO `yzm_menu` VALUES ('313','标识已读','79','guestbook','guestbook','set_read','','0','0');
INSERT INTO `yzm_menu` VALUES ('314','删除管理员回复','79','guestbook','guestbook','del_reply','','0','0');
INSERT INTO `yzm_menu` VALUES ('315','轮播图排序','156','banner','banner','order','','0','0');
INSERT INTO `yzm_menu` VALUES ('316','管理非自己发布的内容','30','admin','content','all_content','','0','0');
INSERT INTO `yzm_menu` VALUES ('317','蜘蛛来访记录','3','spider','spider','init','','1','1');
INSERT INTO `yzm_menu` VALUES ('318','删除记录','317','spider','spider','del','','1','0');
INSERT INTO `yzm_menu` VALUES ('319','删除全部记录','318','spider','spider','all_del','','1','0');
INSERT INTO `yzm_menu` VALUES ('320','统计报表','319','spider','spider','statistics','','1','0');
INSERT INTO `yzm_menu` VALUES ('321','站点统计','3','vcount','vcount','init','','1','1');
INSERT INTO `yzm_menu` VALUES ('322','重复内容检测','3','content','check_repeat','init','','5','1');
INSERT INTO `yzm_menu` VALUES ('323','重复内容检测','322','content','check_repeat','public_check','','0','0');
INSERT INTO `yzm_menu` VALUES ('324','一键删除','322','content','check_repeat','delete','','0','0');
INSERT INTO `yzm_menu` VALUES ('325','内容重复详情','322','content','check_repeat','show','','0','0');
INSERT INTO `yzm_menu` VALUES ('326','内容重复详情删除','322','content','check_repeat','del','','0','0');

-- -----------------------------
-- Table structure for `yzm_message`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_message`;
CREATE TABLE `yzm_message` (
  `messageid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `send_from` varchar(30) NOT NULL DEFAULT '' COMMENT '发件人',
  `send_to` varchar(30) NOT NULL DEFAULT '' COMMENT '收件人',
  `message_time` int(10) unsigned NOT NULL DEFAULT '0',
  `subject` char(80) NOT NULL DEFAULT '' COMMENT '主题',
  `content` text NOT NULL,
  `replyid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '回复的id',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '1正常0隐藏',
  `isread` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否读过',
  `issystem` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '系统信息',
  PRIMARY KEY (`messageid`),
  KEY `replyid` (`replyid`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_message_data`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_message_data`;
CREATE TABLE `yzm_message_data` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `group_message_id` int(5) unsigned NOT NULL DEFAULT '0' COMMENT '读过的信息ID',
  PRIMARY KEY (`id`),
  KEY `message` (`userid`,`group_message_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_message_group`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_message_group`;
CREATE TABLE `yzm_message_group` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `groupid` varchar(20) NOT NULL DEFAULT '' COMMENT '用户组id',
  `subject` char(80) NOT NULL DEFAULT '',
  `content` text NOT NULL COMMENT '内容',
  `inputtime` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_model`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_model`;
CREATE TABLE `yzm_model` (
  `modelid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `siteid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `name` char(30) NOT NULL DEFAULT '',
  `tablename` varchar(30) NOT NULL DEFAULT '',
  `alias` varchar(30) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  `setting` text,
  `inputtime` int(10) unsigned NOT NULL DEFAULT '0',
  `items` smallint(5) unsigned NOT NULL DEFAULT '0',
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `issystem` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `isdefault` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`modelid`),
  KEY `siteid` (`siteid`,`type`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `yzm_model`
-- -----------------------------
INSERT INTO `yzm_model` VALUES ('1','0','文章模型','article','article','文章模型','','1466393786','0','0','0','0','1','1');
INSERT INTO `yzm_model` VALUES ('2','0','产品模型','product','product','产品模型','','1466393786','0','0','0','0','1','0');
INSERT INTO `yzm_model` VALUES ('3','0','下载模型','download','download','下载模型','','1466393786','0','0','0','0','1','0');
INSERT INTO `yzm_model` VALUES ('4','0','单页模型','page','page','单页模型','','1683775806','0','0','2','0','1','0');

-- -----------------------------
-- Table structure for `yzm_model_field`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_model_field`;
CREATE TABLE `yzm_model_field` (
  `fieldid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `modelid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `field` varchar(30) NOT NULL DEFAULT '',
  `name` varchar(30) NOT NULL DEFAULT '',
  `tips` varchar(100) NOT NULL DEFAULT '',
  `setting_catid` varchar(100) NOT NULL DEFAULT '',
  `minlength` int(10) unsigned NOT NULL DEFAULT '0',
  `maxlength` int(10) unsigned NOT NULL DEFAULT '0',
  `errortips` varchar(100) NOT NULL DEFAULT '',
  `fieldtype` varchar(20) NOT NULL DEFAULT '',
  `defaultvalue` varchar(30) NOT NULL DEFAULT '',
  `setting` text,
  `isrequired` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `issystem` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `isunique` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `isadd` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `listorder` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`fieldid`),
  KEY `modelid` (`modelid`,`disabled`),
  KEY `field` (`field`,`modelid`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `yzm_model_field`
-- -----------------------------
INSERT INTO `yzm_model_field` VALUES ('1','0','title','标题','','','1','100','请输入标题','input','','','1','1','0','1','0','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('2','0','catid','栏目','','','1','10','请选择栏目','select','','','1','1','0','1','0','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('3','0','thumb','缩略图','','','0','100','','image','','','0','1','0','1','0','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('4','0','keywords','关键词','','','0','50','','input','','','0','1','0','1','0','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('5','0','description','摘要','','','0','255','','textarea','','','0','1','0','1','0','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('6','0','inputtime','发布时间','','','1','10','','datetime','','','1','1','0','0','0','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('7','0','updatetime','更新时间','','','1','10','','datetime','','','1','1','0','0','0','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('8','0','copyfrom','来源','','','0','30','','input','','','0','1','0','1','0','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('9','0','url','URL','','','1','100','','input','','','1','1','0','0','0','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('10','0','userid','用户ID','','','1','10','','input','','','1','1','0','0','0','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('11','0','username','用户名','','','1','30','','input','','','1','1','0','0','0','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('12','0','nickname','昵称','','','0','30','','input','','','0','1','0','0','0','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('13','0','template','模板','','','1','50','','select','','','1','1','0','0','0','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('14','0','content','内容','','','1','999999','','editor','','','1','1','0','1','0','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('15','0','click','点击数','','','1','10','','input','0','','0','1','0','0','0','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('16','0','tag','TAG','','','0','50','','checkbox','','','0','1','0','0','0','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('17','0','readpoint','阅读收费','','','1','5','','input','0','','0','1','0','0','0','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('18','0','groupids_view','阅读权限','','','1','10','','checkbox','1','','0','1','0','0','0','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('19','0','status','状态','','','1','2','','checkbox','','','1','1','0','0','0','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('20','0','flag','属性','','','1','16','','checkbox','','','1','1','0','0','0','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('21','0','listorder','排序','','','1','5','','input','1','','1','1','0','0','0','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('22','2','brand','品牌','','','0','30','','input','','','0','0','0','1','1','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('23','2','standard','型号','','','0','30','','input','','','0','0','0','1','1','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('24','2','yieldly','产地','','','0','50','','input','','','0','0','0','1','1','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('25','2','pictures','产品图集','','','0','1000','','images','','','0','0','0','1','1','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('26','2','price','单价','请输入单价','','1','10','单价不能为空','decimal','','','1','0','0','1','1','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('27','2','unit','价格单位','','','1','10','','select','','{\"0\":\"件\",\"1\":\"斤\",\"2\":\"KG\",\"3\":\"吨\",\"4\":\"套\"}','1','0','0','1','1','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('28','2','stock','库存','库存量必须为数字','','1','5','库存不能为空','number','99999','','1','0','0','1','1','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('29','3','down_url','下载地址','','','1','100','下载地址不能为空','attachment','','','1','0','0','1','1','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('30','3','copytype','授权形式','','','0','20','','select','','{\"0\":\"免费版\",\"1\":\"正式版\",\"2\":\"共享版\",\"3\":\"试用版\",\"4\":\"演示版\",\"5\":\"注册版\",\"6\":\"破解版\"}','0','0','0','1','1','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('31','3','systems','平台','','','1','30','','select','','{\"0\":\"Windows\",\"1\":\"Linux\",\"2\":\"MacOS\"}','1','0','0','1','1','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('32','3','language','语言','','','0','20','','select','','{\"0\":\"简体中文\",\"1\":\"繁体中文\",\"2\":\"英文\",\"3\":\"多国语言\",\"4\":\"其他语言\"}','0','0','0','1','1','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('33','3','version','版本','','','1','15','版本号不能为空','input','','','1','0','0','1','1','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('34','3','filesize','文件大小','只输入数字即可，单位是字节','','0','10','','input','','','0','0','0','1','1','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('35','3','classtype','软件类型','','','1','30','','radio','','{\"0\":\"国产软件\",\"1\":\"国外软件\",\"2\":\"汉化补丁\",\"3\":\"程序源码\",\"4\":\"其他\"}','1','0','0','1','1','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('36','3','stars','评分等级','','','0','20','','radio','','{\"0\":\"1:1星\",\"1\":\"2:2星\",\"2\":\"3:3星\",\"3\":\"4:4星\",\"4\":\"5:5星\"}','0','0','0','1','1','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('37','4','introduce','单页介绍','','0','0','100','','textarea','','','0','0','0','0','1','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('38','1','up','顶','','','0','0','','number','',NULL,'0','0','0','0','1','0','0','1');
INSERT INTO `yzm_model_field` VALUES ('39','1','down','踩','','','0','100','','number','',NULL,'0','0','0','0','1','0','0','1');

-- -----------------------------
-- Table structure for `yzm_module`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_module`;
CREATE TABLE `yzm_module` (
  `module` varchar(15) NOT NULL DEFAULT '',
  `name` varchar(30) NOT NULL DEFAULT '',
  `iscore` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `version` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `setting` text,
  `listorder` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `installdate` date NOT NULL DEFAULT '2016-01-01',
  `updatedate` date NOT NULL DEFAULT '2016-01-01',
  PRIMARY KEY (`module`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `yzm_module`
-- -----------------------------
INSERT INTO `yzm_module` VALUES ('admin','后台模块','1','3.0','后台模块','','0','0','2016-08-27','2023-05-12');
INSERT INTO `yzm_module` VALUES ('index','前台模块','1','2.0','前台模块','','0','0','2016-09-21','2023-01-21');
INSERT INTO `yzm_module` VALUES ('api','接口模块','1','2.0','为整个系统提供接口','','0','0','2016-08-28','2022-08-28');
INSERT INTO `yzm_module` VALUES ('install','安装模块','1','2.0','CMS安装模块','','0','0','2016-10-28','2022-10-28');
INSERT INTO `yzm_module` VALUES ('attachment','附件模块','1','2.0','附件模块','','0','0','2016-10-10','2023-05-10');
INSERT INTO `yzm_module` VALUES ('member','会员模块','1','3.0','会员模块','','0','0','2016-09-21','2023-02-21');
INSERT INTO `yzm_module` VALUES ('guestbook','留言模块','1','2.0','留言板模块','','0','0','2016-10-25','2022-10-25');
INSERT INTO `yzm_module` VALUES ('search','搜索模块','1','2.0','搜索模块','','0','0','2016-11-21','2023-01-21');
INSERT INTO `yzm_module` VALUES ('link','友情链接','0','2.0','友情链接模块','','0','0','2016-12-11','2023-02-10');
INSERT INTO `yzm_module` VALUES ('comment','评论模块','1','2.0','全站评论','','0','0','2017-01-05','2022-01-05');
INSERT INTO `yzm_module` VALUES ('mobile','手机模块','1','2.0','手机模块','','0','0','2017-04-05','2022-04-05');
INSERT INTO `yzm_module` VALUES ('banner','轮播图管理','0','2.0','轮播图管理模块','','0','0','2017-05-12','2023-02-10');
INSERT INTO `yzm_module` VALUES ('collection','采集模块','1','1.0','采集模块','','0','0','2017-08-16','2022-08-16');
INSERT INTO `yzm_module` VALUES ('wechat','微信模块','1','2.0','微信模块','','0','0','2017-11-03','2022-11-03');
INSERT INTO `yzm_module` VALUES ('diyform','自定义表单模块','1','2.0','自定义表单模块','','0','0','2018-01-15','2023-05-11');
INSERT INTO `yzm_module` VALUES ('adver','广告管理','0','2.0','广告管理模块','','0','0','2018-01-18','2023-01-18');
INSERT INTO `yzm_module` VALUES ('pay','支付模块','1','1.0','支付模块','','0','0','2018-07-03','2022-07-03');
INSERT INTO `yzm_module` VALUES ('spider','蜘蛛访问记录模块','0','1.0','蜘蛛访问记录模块','','0','0','2023-10-14','2021-05-21');
INSERT INTO `yzm_module` VALUES ('vcount','站点统计模块','0','1.0','站点统计模块','','0','0','2023-10-14','2019-06-21');
INSERT INTO `yzm_module` VALUES ('content','重复内容检测','0','1.0','提供YzmCMS重复内容检测、删除等功能','','0','0','2023-10-20','2022-08-30');

-- -----------------------------
-- Table structure for `yzm_order`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_order`;
CREATE TABLE `yzm_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_sn` char(18) NOT NULL DEFAULT '' COMMENT '订单号',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '订单状态0未付款1已付款',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `username` varchar(30) NOT NULL DEFAULT '' COMMENT '用户名',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '下单时间',
  `paytime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '支付时间',
  `paytype` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '支付方式1支付宝2微信',
  `transaction` varchar(32) NOT NULL DEFAULT '' COMMENT '第三方交易单号',
  `money` decimal(8,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '订单金额',
  `quantity` decimal(8,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '数量',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1积分,2金钱',
  `ip` char(15) NOT NULL DEFAULT '',
  `desc` varchar(250) NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `userid` (`userid`),
  KEY `order_sn` (`order_sn`),
  KEY `status` (`status`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='订单表';


-- -----------------------------
-- Table structure for `yzm_page`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_page`;
CREATE TABLE `yzm_page` (
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `title` varchar(160) NOT NULL DEFAULT '',
  `introduce` varchar(255) NOT NULL DEFAULT '',
  `content` text,
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0',
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_pay`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_pay`;
CREATE TABLE `yzm_pay` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `trade_sn` char(18) NOT NULL DEFAULT '' COMMENT '订单号',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `money` char(8) NOT NULL DEFAULT '' COMMENT '金钱或积分的量',
  `creat_time` int(10) unsigned NOT NULL DEFAULT '0',
  `msg` varchar(30) NOT NULL DEFAULT '' COMMENT '类型说明',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1积分,2金钱',
  `ip` char(15) NOT NULL DEFAULT '',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '1成功,0失败',
  `remarks` varchar(250) NOT NULL DEFAULT '' COMMENT '备注说明',
  `adminnote` varchar(30) NOT NULL DEFAULT '' COMMENT '如是后台操作,管理员姓名',
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`),
  KEY `trade_sn` (`trade_sn`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_pay_mode`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_pay_mode`;
CREATE TABLE `yzm_pay_mode` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL DEFAULT '',
  `logo` varchar(100) NOT NULL DEFAULT '',
  `desc` varchar(250) NOT NULL DEFAULT '',
  `config` text,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `author` varchar(60) NOT NULL DEFAULT '',
  `version` varchar(10) NOT NULL DEFAULT '',
  `action` varchar(30) NOT NULL DEFAULT '' COMMENT '支付调用方法',
  `template` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `yzm_pay_mode`
-- -----------------------------
INSERT INTO `yzm_pay_mode` VALUES ('1','支付宝','alipay.png','支付宝新版在线支付插件，要求PHP版本>=5.5','{\"app_id\":\"\",\"merchant_private_key\":\"\",\"alipay_public_key\":\"\"}','1','袁志蒙','1.0','alipay','alipay');
INSERT INTO `yzm_pay_mode` VALUES ('2','微信','wechat.png','微信支付提供公众号支付、APP支付、扫码支付、刷卡支付等支付方式。','{\\\"app_id\\\":\\\"\\\",\\\"app_secret\\\":\\\"\\\",\\\"mch_id\\\":\\\"\\\",\\\"key\\\":\\\"\\\"}','0','袁志蒙','1.0','wechat','wechat');

-- -----------------------------
-- Table structure for `yzm_pay_spend`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_pay_spend`;
CREATE TABLE `yzm_pay_spend` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `trade_sn` char(18) NOT NULL DEFAULT '' COMMENT '订单号',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `money` char(8) NOT NULL DEFAULT '' COMMENT '金钱或积分的量',
  `creat_time` int(10) unsigned NOT NULL DEFAULT '0',
  `msg` varchar(30) NOT NULL DEFAULT '' COMMENT '类型说明',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1积分,2金钱',
  `ip` char(15) NOT NULL DEFAULT '',
  `remarks` varchar(250) NOT NULL DEFAULT '' COMMENT '备注说明',
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`),
  KEY `trade_sn` (`trade_sn`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_product`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_product`;
CREATE TABLE `yzm_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `nickname` varchar(30) NOT NULL DEFAULT '',
  `title` varchar(180) NOT NULL DEFAULT '',
  `color` char(9) NOT NULL DEFAULT '',
  `inputtime` int(10) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0',
  `keywords` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `click` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  `copyfrom` varchar(50) NOT NULL DEFAULT '',
  `thumb` varchar(150) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL DEFAULT '',
  `flag` varchar(12) NOT NULL DEFAULT '' COMMENT '1置顶,2头条,3特荐,4推荐,5热点,6幻灯,7跳转',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `issystem` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `listorder` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `groupids_view` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '阅读权限',
  `readpoint` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '阅读收费',
  `paytype` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '收费类型',
  `is_push` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否百度推送',
  `brand` varchar(50) NOT NULL DEFAULT '' COMMENT '品牌',
  `standard` varchar(100) NOT NULL DEFAULT '' COMMENT '型号',
  `yieldly` varchar(100) NOT NULL DEFAULT '' COMMENT '产地',
  `pictures` text COMMENT '产品图集',
  `price` decimal(8,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '单价',
  `unit` varchar(30) NOT NULL DEFAULT '' COMMENT '价格单位',
  `stock` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '库存',
  PRIMARY KEY (`id`),
  KEY `status` (`status`,`listorder`),
  KEY `catid` (`status`,`catid`),
  KEY `userid` (`status`,`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_spider_log`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_spider_log`;
CREATE TABLE `yzm_spider_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `spider` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '蜘蛛',
  `ip` varchar(15) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `referer` varchar(255) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `spider` (`spider`),
  KEY `url` (`url`),
  KEY `addtime` (`addtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='蜘蛛访问日志';


-- -----------------------------
-- Table structure for `yzm_tag`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_tag`;
CREATE TABLE `yzm_tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `siteid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `tag` varchar(30) NOT NULL DEFAULT '',
  `total` mediumint(9) unsigned NOT NULL DEFAULT '0',
  `click` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `seo_title` varchar(100) NOT NULL DEFAULT '' COMMENT 'SEO标题',
  `seo_keywords` varchar(200) NOT NULL DEFAULT '' COMMENT 'SEO关键字',
  `seo_description` varchar(255) NOT NULL DEFAULT '' COMMENT 'SEO描述',
  `inputtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `siteid_catid` (`siteid`,`catid`),
  KEY `siteid_tag` (`siteid`,`tag`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `yzm_tag`
-- -----------------------------
INSERT INTO `yzm_tag` VALUES ('1','0','0','抖音','2','4','','','','1697001019');
INSERT INTO `yzm_tag` VALUES ('2','0','0','明星','1','2','','','','1697087108');
INSERT INTO `yzm_tag` VALUES ('3','0','0','程序员','0','1','','','','1697088427');
INSERT INTO `yzm_tag` VALUES ('4','0','0','房产','1','1','','','','1697094601');
INSERT INTO `yzm_tag` VALUES ('5','0','0','数码','2','0','','','','1697095190');
INSERT INTO `yzm_tag` VALUES ('6','0','0','汽车','0','1','','','','1697096265');
INSERT INTO `yzm_tag` VALUES ('7','0','0','教育','2','4','','','','1697099434');

-- -----------------------------
-- Table structure for `yzm_tag_content`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_tag_content`;
CREATE TABLE `yzm_tag_content` (
  `siteid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `modelid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `aid` int(10) unsigned NOT NULL DEFAULT '0',
  `tagid` int(10) unsigned NOT NULL DEFAULT '0',
  KEY `tag_index` (`modelid`,`aid`),
  KEY `tagid_index` (`siteid`,`tagid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `yzm_tag_content`
-- -----------------------------
INSERT INTO `yzm_tag_content` VALUES ('0','1','10','6','2');
INSERT INTO `yzm_tag_content` VALUES ('0','1','9','8','1');
INSERT INTO `yzm_tag_content` VALUES ('0','1','8','15','4');
INSERT INTO `yzm_tag_content` VALUES ('0','1','8','17','5');
INSERT INTO `yzm_tag_content` VALUES ('0','1','9','24','7');
INSERT INTO `yzm_tag_content` VALUES ('0','1','9','27','5');
INSERT INTO `yzm_tag_content` VALUES ('0','1','9','29','7');

-- -----------------------------
-- Table structure for `yzm_urlrule`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_urlrule`;
CREATE TABLE `yzm_urlrule` (
  `urlruleid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '规则名称',
  `urlrule` varchar(100) NOT NULL DEFAULT '' COMMENT 'URL规则',
  `route` varchar(100) NOT NULL DEFAULT '' COMMENT '指向的路由',
  `listorder` tinyint(3) unsigned NOT NULL DEFAULT '50' COMMENT '优先级排序',
  PRIMARY KEY (`urlruleid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_view_count`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_view_count`;
CREATE TABLE `yzm_view_count` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '总访问',
  `today` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '今日访问',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='站点浏览统计';

-- -----------------------------
-- Records of `yzm_view_count`
-- -----------------------------
INSERT INTO `yzm_view_count` VALUES ('1','499','290','1699272527');

-- -----------------------------
-- Table structure for `yzm_view_log`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_view_log`;
CREATE TABLE `yzm_view_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `inputtime` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` char(15) NOT NULL DEFAULT '',
  `referer` varchar(80) NOT NULL DEFAULT '' COMMENT '来源',
  PRIMARY KEY (`id`),
  KEY `ip` (`ip`)
) ENGINE=InnoDB AUTO_INCREMENT=501 DEFAULT CHARSET=utf8 COMMENT='站点浏览记录';

-- -----------------------------
-- Records of `yzm_view_log`
-- -----------------------------
INSERT INTO `yzm_view_log` VALUES ('1','1698160624','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('2','1698160626','127.0.0.1','http://localhost:94/search/index/tag/id/6.html');
INSERT INTO `yzm_view_log` VALUES ('3','1698160628','127.0.0.1','http://localhost:94/search/index/tag/id/7.html');
INSERT INTO `yzm_view_log` VALUES ('4','1699177445','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('5','1699180385','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('6','1699180754','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('7','1699184265','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('8','1699192961','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('9','1699192965','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('10','1699192989','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('11','1699192990','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('12','1699192991','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('13','1699193112','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('14','1699193122','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('15','1699193186','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('16','1699193210','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('17','1699193235','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('18','1699193241','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('19','1699193376','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('20','1699193403','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('21','1699193415','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('22','1699193431','127.0.0.1','http://localhost:94/kaidian/20.html');
INSERT INTO `yzm_view_log` VALUES ('23','1699193464','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('24','1699193515','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('25','1699193516','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('26','1699193587','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('27','1699193594','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('28','1699193704','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('29','1699193708','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('30','1699193739','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('31','1699193752','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('32','1699193780','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('33','1699193789','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('34','1699193910','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('35','1699193957','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('36','1699193959','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('37','1699193967','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('38','1699194015','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('39','1699194043','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('40','1699194069','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('41','1699194069','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('42','1699194070','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('43','1699194085','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('44','1699194320','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('45','1699194363','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('46','1699194396','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('47','1699194413','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('48','1699194524','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('49','1699194587','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('50','1699194713','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('51','1699194780','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('52','1699194847','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('53','1699195105','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('54','1699195124','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('55','1699195132','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('56','1699195193','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('57','1699195247','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('58','1699195275','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('59','1699195342','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('60','1699195406','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('61','1699195408','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('62','1699195518','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('63','1699195574','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('64','1699195579','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('65','1699195627','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('66','1699195630','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('67','1699195650','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('68','1699195661','127.0.0.1','http://localhost:94/kaidian/');
INSERT INTO `yzm_view_log` VALUES ('69','1699195769','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('70','1699195783','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('71','1699195797','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('72','1699195803','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('73','1699195914','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('74','1699195916','127.0.0.1','http://localhost:94/taobaotuiguang/');
INSERT INTO `yzm_view_log` VALUES ('75','1699195918','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('76','1699195918','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('77','1699195919','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('78','1699195920','127.0.0.1','http://localhost:94/taobaotuiguang/');
INSERT INTO `yzm_view_log` VALUES ('79','1699195921','127.0.0.1','http://localhost:94/zhibodianshang/');
INSERT INTO `yzm_view_log` VALUES ('80','1699195921','127.0.0.1','http://localhost:94/dianshangzatan/');
INSERT INTO `yzm_view_log` VALUES ('81','1699195924','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('82','1699195924','127.0.0.1','http://localhost:94/taobaotuiguang/');
INSERT INTO `yzm_view_log` VALUES ('83','1699195925','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('84','1699195959','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('85','1699195961','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('86','1699196036','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('87','1699196038','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('88','1699196039','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('89','1699196049','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('90','1699196051','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('91','1699196053','127.0.0.1','http://localhost:94/taobaotuiguang/');
INSERT INTO `yzm_view_log` VALUES ('92','1699196054','127.0.0.1','http://localhost:94/zhibodianshang/');
INSERT INTO `yzm_view_log` VALUES ('93','1699196056','127.0.0.1','http://localhost:94/dianshangzatan/');
INSERT INTO `yzm_view_log` VALUES ('94','1699196058','127.0.0.1','http://localhost:94/zhibodianshang/');
INSERT INTO `yzm_view_log` VALUES ('95','1699196059','127.0.0.1','http://localhost:94/taobaotuiguang/');
INSERT INTO `yzm_view_log` VALUES ('96','1699196066','127.0.0.1','http://localhost:94/taobaotuiguang/');
INSERT INTO `yzm_view_log` VALUES ('97','1699196067','127.0.0.1','http://localhost:94/taobaotuiguang/');
INSERT INTO `yzm_view_log` VALUES ('98','1699196067','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('99','1699196159','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('100','1699196168','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('101','1699196210','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('102','1699196214','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('103','1699196273','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('104','1699196274','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('105','1699196275','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('106','1699196278','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('107','1699196282','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('108','1699196286','127.0.0.1','http://localhost:94/taobaotuiguang/');
INSERT INTO `yzm_view_log` VALUES ('109','1699196288','127.0.0.1','http://localhost:94/zhibodianshang/');
INSERT INTO `yzm_view_log` VALUES ('110','1699196288','127.0.0.1','http://localhost:94/dianshangzatan/');
INSERT INTO `yzm_view_log` VALUES ('111','1699196290','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('112','1699196291','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('113','1699196334','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('114','1699196417','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('115','1699196423','127.0.0.1','http://localhost:94/yunying/');
INSERT INTO `yzm_view_log` VALUES ('116','1699196453','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('117','1699196496','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('118','1699196513','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('119','1699196514','127.0.0.1','http://localhost:94/kaidian/');
INSERT INTO `yzm_view_log` VALUES ('120','1699196518','127.0.0.1','http://localhost:94/yunying/');
INSERT INTO `yzm_view_log` VALUES ('121','1699196525','127.0.0.1','http://localhost:94/kaidian/');
INSERT INTO `yzm_view_log` VALUES ('122','1699196527','127.0.0.1','http://localhost:94/yunying/');
INSERT INTO `yzm_view_log` VALUES ('123','1699196546','127.0.0.1','http://localhost:94/yunying/');
INSERT INTO `yzm_view_log` VALUES ('124','1699196566','127.0.0.1','http://localhost:94/yunying/');
INSERT INTO `yzm_view_log` VALUES ('125','1699196586','127.0.0.1','http://localhost:94/yunying/');
INSERT INTO `yzm_view_log` VALUES ('126','1699196588','127.0.0.1','http://localhost:94/yunying/13.html');
INSERT INTO `yzm_view_log` VALUES ('127','1699196606','127.0.0.1','http://localhost:94/yunying/');
INSERT INTO `yzm_view_log` VALUES ('128','1699196607','127.0.0.1','http://localhost:94/yunying/13.html');
INSERT INTO `yzm_view_log` VALUES ('129','1699196622','127.0.0.1','http://localhost:94/taobaotuiguang/');
INSERT INTO `yzm_view_log` VALUES ('130','1699196624','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('131','1699196629','127.0.0.1','http://localhost:94/kaidian/26.html');
INSERT INTO `yzm_view_log` VALUES ('132','1699196683','127.0.0.1','http://localhost:94/kaidian/26.html');
INSERT INTO `yzm_view_log` VALUES ('133','1699196726','127.0.0.1','http://localhost:94/kaidian/26.html');
INSERT INTO `yzm_view_log` VALUES ('134','1699196750','127.0.0.1','http://localhost:94/kaidian/26.html');
INSERT INTO `yzm_view_log` VALUES ('135','1699196876','127.0.0.1','http://localhost:94/kaidian/26.html');
INSERT INTO `yzm_view_log` VALUES ('136','1699196879','127.0.0.1','http://localhost:94/kaidian/26.html');
INSERT INTO `yzm_view_log` VALUES ('137','1699196887','127.0.0.1','http://localhost:94/kaidian/26.html');
INSERT INTO `yzm_view_log` VALUES ('138','1699196924','127.0.0.1','http://localhost:94/kaidian/26.html');
INSERT INTO `yzm_view_log` VALUES ('139','1699196930','127.0.0.1','http://localhost:94/kaidian/26.html');
INSERT INTO `yzm_view_log` VALUES ('140','1699196944','127.0.0.1','http://localhost:94/kaidian/26.html');
INSERT INTO `yzm_view_log` VALUES ('141','1699196949','127.0.0.1','http://localhost:94/kaidian/26.html');
INSERT INTO `yzm_view_log` VALUES ('142','1699196957','127.0.0.1','http://localhost:94/kaidian/26.html');
INSERT INTO `yzm_view_log` VALUES ('143','1699197039','127.0.0.1','http://localhost:94/kaidian/26.html');
INSERT INTO `yzm_view_log` VALUES ('144','1699197068','127.0.0.1','http://localhost:94/kaidian/26.html');
INSERT INTO `yzm_view_log` VALUES ('145','1699197074','127.0.0.1','http://localhost:94/kaidian/');
INSERT INTO `yzm_view_log` VALUES ('146','1699197127','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('147','1699197144','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('148','1699197163','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('149','1699197175','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('150','1699197227','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('151','1699197311','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('152','1699197346','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('153','1699197380','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('154','1699197406','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('155','1699197636','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('156','1699197715','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('157','1699197730','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('158','1699197746','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('159','1699197759','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('160','1699197780','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('161','1699197801','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('162','1699197853','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('163','1699197905','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('164','1699197933','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('165','1699197987','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('166','1699198004','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('167','1699198024','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('168','1699198041','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('169','1699198052','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('170','1699198062','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('171','1699198089','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('172','1699198103','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('173','1699198223','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('174','1699198227','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('175','1699198241','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('176','1699198262','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('177','1699198275','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('178','1699198309','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('179','1699198324','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('180','1699198334','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('181','1699198378','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('182','1699198461','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('183','1699198463','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('184','1699198484','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('185','1699198493','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('186','1699198508','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('187','1699198545','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('188','1699198580','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('189','1699198596','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('190','1699198603','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('191','1699198610','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('192','1699198649','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('193','1699198657','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('194','1699198701','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('195','1699198705','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('196','1699198977','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('197','1699199052','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('198','1699199266','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('199','1699199394','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('200','1699199416','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('201','1699199423','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('202','1699199458','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('203','1699199541','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('204','1699199730','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('205','1699199842','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('206','1699199876','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('207','1699199938','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('208','1699199954','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('209','1699199984','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('210','1699200112','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('211','1699200139','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('212','1699200214','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('213','1699200315','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('214','1699200333','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('215','1699200405','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('216','1699200419','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('217','1699200446','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('218','1699200457','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('219','1699200482','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('220','1699200590','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('221','1699200603','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('222','1699200655','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('223','1699200687','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('224','1699200701','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('225','1699200733','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('226','1699200874','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('227','1699200914','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('228','1699200932','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('229','1699200948','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('230','1699200982','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('231','1699200991','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('232','1699201076','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('233','1699201089','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('234','1699201127','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('235','1699201141','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('236','1699201161','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('237','1699201180','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('238','1699201236','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('239','1699201246','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('240','1699201269','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('241','1699201278','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('242','1699201289','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('243','1699201410','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('244','1699201423','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('245','1699201428','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('246','1699201465','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('247','1699201466','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('248','1699201474','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('249','1699201478','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('250','1699201495','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('251','1699201503','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('252','1699201515','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('253','1699201526','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('254','1699201575','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('255','1699201584','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('256','1699201597','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('257','1699201646','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('258','1699201655','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('259','1699201662','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('260','1699201685','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('261','1699201707','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('262','1699201737','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('263','1699201748','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('264','1699201778','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('265','1699201832','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('266','1699201846','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('267','1699201898','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('268','1699201922','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('269','1699201981','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('270','1699202009','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('271','1699202052','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('272','1699202070','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('273','1699202088','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('274','1699202093','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('275','1699202117','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('276','1699202120','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('277','1699202149','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('278','1699202150','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('279','1699202171','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('280','1699202183','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('281','1699202217','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('282','1699202238','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('283','1699202277','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('284','1699202380','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('285','1699202402','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('286','1699202548','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('287','1699202567','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('288','1699202636','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('289','1699202650','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('290','1699202682','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('291','1699202687','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('292','1699202724','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('293','1699202744','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('294','1699202760','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('295','1699202786','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('296','1699202806','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('297','1699202846','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('298','1699202857','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('299','1699202881','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('300','1699202893','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('301','1699202898','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('302','1699202902','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('303','1699202930','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('304','1699202943','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('305','1699202957','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('306','1699202981','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('307','1699203004','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('308','1699203122','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('309','1699203163','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('310','1699203181','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('311','1699203191','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('312','1699203201','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('313','1699203205','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('314','1699203213','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('315','1699203216','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('316','1699203222','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('317','1699203254','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('318','1699203279','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('319','1699203294','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('320','1699203338','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('321','1699203357','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('322','1699203428','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('323','1699203471','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('324','1699203487','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('325','1699203505','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('326','1699203519','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('327','1699203531','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('328','1699203567','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('329','1699203582','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('330','1699203594','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('331','1699203620','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('332','1699203627','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('333','1699203639','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('334','1699203640','127.0.0.1','http://localhost:94/kaidian/');
INSERT INTO `yzm_view_log` VALUES ('335','1699203641','127.0.0.1','http://localhost:94/yunying/');
INSERT INTO `yzm_view_log` VALUES ('336','1699203643','127.0.0.1','http://localhost:94/yunying/13.html');
INSERT INTO `yzm_view_log` VALUES ('337','1699203651','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('338','1699203664','127.0.0.1','http://localhost:94/taobaotuiguang/');
INSERT INTO `yzm_view_log` VALUES ('339','1699203665','127.0.0.1','http://localhost:94/zhibodianshang/');
INSERT INTO `yzm_view_log` VALUES ('340','1699203666','127.0.0.1','http://localhost:94/dianshangzatan/');
INSERT INTO `yzm_view_log` VALUES ('341','1699203667','127.0.0.1','http://localhost:94/taobaotuiguang/');
INSERT INTO `yzm_view_log` VALUES ('342','1699203667','127.0.0.1','http://localhost:94/taobaotuiguang/');
INSERT INTO `yzm_view_log` VALUES ('343','1699203669','127.0.0.1','http://localhost:94/dianshangzatan/');
INSERT INTO `yzm_view_log` VALUES ('344','1699203670','127.0.0.1','http://localhost:94/taobaotuiguang/');
INSERT INTO `yzm_view_log` VALUES ('345','1699203671','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('346','1699203706','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('347','1699203962','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('348','1699203975','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('349','1699203989','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('350','1699204100','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('351','1699204153','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('352','1699204271','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('353','1699204275','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('354','1699204280','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('355','1699204295','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('356','1699204304','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('357','1699204347','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('358','1699204348','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('359','1699204348','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('360','1699204378','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('361','1699204388','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('362','1699204391','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('363','1699204395','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('364','1699251843','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('365','1699251873','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('366','1699251962','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('367','1699251983','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('368','1699252106','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('369','1699252133','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('370','1699252207','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('371','1699252215','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('372','1699252253','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('373','1699252310','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('374','1699252311','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('375','1699252427','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('376','1699252439','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('377','1699252447','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('378','1699252496','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('379','1699252663','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('380','1699252828','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('381','1699266870','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('382','1699267232','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('383','1699267367','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('384','1699267509','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('385','1699267564','127.0.0.1','http://localhost:94/kaidian/22.html');
INSERT INTO `yzm_view_log` VALUES ('386','1699267579','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('387','1699267626','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('388','1699267627','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('389','1699267722','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('390','1699267726','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('391','1699267746','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('392','1699267756','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('393','1699267758','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('394','1699267762','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('395','1699267835','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('396','1699267862','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('397','1699267890','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('398','1699267923','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('399','1699267958','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('400','1699267971','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('401','1699267988','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('402','1699268124','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('403','1699268361','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('404','1699268367','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('405','1699268414','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('406','1699268443','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('407','1699268450','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('408','1699268452','127.0.0.1','http://localhost:94/taobaotuiguang/');
INSERT INTO `yzm_view_log` VALUES ('409','1699268454','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('410','1699268494','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('411','1699268499','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('412','1699268513','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('413','1699268566','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('414','1699268589','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('415','1699268590','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('416','1699268629','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('417','1699268668','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('418','1699268679','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('419','1699268684','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('420','1699268690','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('421','1699268714','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('422','1699268735','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('423','1699268742','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('424','1699268894','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('425','1699268917','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('426','1699268939','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('427','1699268940','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('428','1699268971','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('429','1699269057','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('430','1699269068','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('431','1699269172','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('432','1699269185','127.0.0.1','http://localhost:94/douyin/');
INSERT INTO `yzm_view_log` VALUES ('433','1699269188','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('434','1699269275','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('435','1699269291','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('436','1699269330','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('437','1699269339','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('438','1699269437','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('439','1699269483','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('440','1699269520','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('441','1699269539','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('442','1699269545','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('443','1699269553','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('444','1699269556','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('445','1699269578','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('446','1699269594','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('447','1699269596','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('448','1699269598','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('449','1699269600','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('450','1699269603','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('451','1699269663','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('452','1699269665','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('453','1699269762','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('454','1699269803','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('455','1699269875','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('456','1699269876','127.0.0.1','http://localhost:94/dianshangzatan/');
INSERT INTO `yzm_view_log` VALUES ('457','1699269876','127.0.0.1','http://localhost:94/zhibodianshang/');
INSERT INTO `yzm_view_log` VALUES ('458','1699269877','127.0.0.1','http://localhost:94/taobaotuiguang/');
INSERT INTO `yzm_view_log` VALUES ('459','1699269877','127.0.0.1','http://localhost:94/taobaokaidian/');
INSERT INTO `yzm_view_log` VALUES ('460','1699269878','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('461','1699269878','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('462','1699270131','127.0.0.1','http://localhost:94/dianshangzatan/');
INSERT INTO `yzm_view_log` VALUES ('463','1699270132','127.0.0.1','http://localhost:94/zhibodianshang/');
INSERT INTO `yzm_view_log` VALUES ('464','1699270133','127.0.0.1','http://localhost:94/douyin/');
INSERT INTO `yzm_view_log` VALUES ('465','1699270135','127.0.0.1','http://localhost:94/kuaishou/');
INSERT INTO `yzm_view_log` VALUES ('466','1699270135','127.0.0.1','http://localhost:94/zhibodianshang/');
INSERT INTO `yzm_view_log` VALUES ('467','1699270137','127.0.0.1','http://localhost:94/douyin/');
INSERT INTO `yzm_view_log` VALUES ('468','1699270138','127.0.0.1','http://localhost:94/kuaishou/');
INSERT INTO `yzm_view_log` VALUES ('469','1699270139','127.0.0.1','http://localhost:94/weixin/');
INSERT INTO `yzm_view_log` VALUES ('470','1699270141','127.0.0.1','http://localhost:94/douyin/');
INSERT INTO `yzm_view_log` VALUES ('471','1699270373','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('472','1699270727','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('473','1699270917','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('474','1699270924','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('475','1699270924','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('476','1699270953','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('477','1699270985','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('478','1699271230','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('479','1699271232','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('480','1699271319','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('481','1699271339','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('482','1699271434','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('483','1699271512','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('484','1699271549','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('485','1699271554','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('486','1699271586','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('487','1699271595','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('488','1699271596','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('489','1699271600','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('490','1699271693','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('491','1699271755','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('492','1699271764','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('493','1699271765','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('494','1699271781','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('495','1699271783','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('496','1699271801','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('497','1699271803','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('498','1699271805','127.0.0.1','http://localhost:94/kaidian/29.html');
INSERT INTO `yzm_view_log` VALUES ('499','1699272508','127.0.0.1','http://localhost:94/');
INSERT INTO `yzm_view_log` VALUES ('500','1699272527','127.0.0.1','http://localhost:94/');

-- -----------------------------
-- Table structure for `yzm_wechat_auto_reply`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_wechat_auto_reply`;
CREATE TABLE `yzm_wechat_auto_reply` (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1关键字回复2自动回复3关注回复',
  `keyword` varchar(64) NOT NULL DEFAULT '' COMMENT '关键字回复的关键字',
  `keyword_type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1完全匹配0模糊匹配',
  `relation_id` varchar(15) NOT NULL DEFAULT '' COMMENT '图文回复的关联内容ID',
  `content` text NOT NULL COMMENT '文本回复的内容',
  PRIMARY KEY (`id`),
  KEY `type_index` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_wechat_group`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_wechat_group`;
CREATE TABLE `yzm_wechat_group` (
  `id` mediumint(9) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `count` mediumint(8) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_wechat_mass`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_wechat_mass`;
CREATE TABLE `yzm_wechat_mass` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_type` char(6) NOT NULL DEFAULT '' COMMENT '消息类型',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0通过openid群发1通过分组群发2全部',
  `media_id` varchar(200) NOT NULL DEFAULT '',
  `msg_id` varchar(10) NOT NULL DEFAULT '',
  `msg_data_id` varchar(10) NOT NULL DEFAULT '' COMMENT '图文消息的数据ID',
  `receive` varchar(255) NOT NULL DEFAULT '' COMMENT '按组群发为组id，否则为openid列表',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '1任务提交成功2群发已结束',
  `masstime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发送时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_wechat_media`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_wechat_media`;
CREATE TABLE `yzm_wechat_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `originname` varchar(50) NOT NULL DEFAULT '',
  `filename` varchar(50) NOT NULL DEFAULT '',
  `filepath` char(200) NOT NULL DEFAULT '',
  `type` char(6) NOT NULL DEFAULT '',
  `media_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0临时素材,1永久素材',
  `media_id` varchar(200) NOT NULL DEFAULT '',
  `created_at` int(10) unsigned NOT NULL DEFAULT '0',
  `url` varchar(200) NOT NULL DEFAULT '' COMMENT '永久素材的图片url/图文素材标题',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_wechat_menu`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_wechat_menu`;
CREATE TABLE `yzm_wechat_menu` (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `parentid` mediumint(6) NOT NULL DEFAULT '0',
  `name` varchar(48) NOT NULL DEFAULT '',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1关键字2跳转',
  `keyword` varchar(128) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `event` varchar(64) NOT NULL DEFAULT '',
  `listorder` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parentid` (`parentid`),
  KEY `listorder` (`listorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_wechat_message`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_wechat_message`;
CREATE TABLE `yzm_wechat_message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `openid` char(100) NOT NULL DEFAULT '',
  `issystem` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为系统回复',
  `inputtime` int(10) unsigned NOT NULL DEFAULT '0',
  `msgtype` varchar(32) NOT NULL DEFAULT '' COMMENT '消息类型',
  `isread` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '1已读0未读',
  `content` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `openid` (`openid`),
  KEY `issystem` (`issystem`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_wechat_scan`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_wechat_scan`;
CREATE TABLE `yzm_wechat_scan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scan` varchar(65) NOT NULL DEFAULT '' COMMENT '场景',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0永久,1临时',
  `expire_time` char(7) NOT NULL DEFAULT '0' COMMENT '二维码有效时间',
  `ticket` varchar(150) NOT NULL DEFAULT '',
  `remarks` varchar(255) NOT NULL DEFAULT '' COMMENT '场景备注',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `yzm_wechat_user`
-- -----------------------------
DROP TABLE IF EXISTS `yzm_wechat_user`;
CREATE TABLE `yzm_wechat_user` (
  `wechatid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `openid` char(100) NOT NULL DEFAULT '',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `subscribe` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1关注0取消',
  `nickname` varchar(50) NOT NULL DEFAULT '',
  `sex` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `city` char(50) NOT NULL DEFAULT '',
  `province` char(50) NOT NULL DEFAULT '',
  `country` char(50) NOT NULL DEFAULT '',
  `headimgurl` char(255) NOT NULL DEFAULT '',
  `subscribe_time` int(10) unsigned NOT NULL DEFAULT '0',
  `remark` varchar(50) NOT NULL DEFAULT '',
  `scan` varchar(30) NOT NULL DEFAULT '' COMMENT '来源场景',
  PRIMARY KEY (`wechatid`),
  UNIQUE KEY `openid` (`openid`),
  KEY `groupid` (`groupid`),
  KEY `subscribe` (`subscribe`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

